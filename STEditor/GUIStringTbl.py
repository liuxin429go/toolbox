#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from io import StringIO
import os
import shutil
import sys
#import win32com.client.gencache

import wx
import wx.stc
import wx.html
import wx.grid
from wx.lib.anchors import LayoutAnchors
import imp
#from wx.lib.activexwrapper import MakeActiveXClass
# Workaround, if DD_DIR_MUST_EXIST is not defined in wxPython
try:
    dummy = wx.DD_DIR_MUST_EXIST
except AttributeError:
    wx.DD_DIR_MUST_EXIST = 0x0200

import StringTbl

__package__ = 'STEditor'
__bubble__  = 'H2D2 String Table Editor'
__version__ = '19.02'
__copyright__ = 'Copyright (c) 2007 INRS\nCopyright (c) 2019 Yves Secretan\nLGLP 3.0'


def create(parent):
    return GUIStringTbl(parent)

[wxID_GUISTRINGTBL, wxID_GUISTRINGTBLDUMPWINDOW, wxID_GUISTRINGTBLGRID,
 wxID_GUISTRINGTBLHTMLWINDOW, wxID_GUISTRINGTBLNOTEBOOK,
 wxID_GUISTRINGTBLSPLITTERWINDOW, wxID_GUISTRINGTBLSTATUSBAR,
] = [wx.NewId() for _init_ctrls in range(7)]

[wxID_GUISTRINGTBLMNU_FILEMNU_FILE_CLOSE,
 wxID_GUISTRINGTBLMNU_FILEMNU_FILE_QUIT,
 wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SAVE,
 wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SAVE_SRC,
 wxID_GUISTRINGTBLMNU_FILEMNU_FILE_RESCAN,
 wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SCAN_ERR,
 wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SCAN_MSG,
] = [wx.NewId() for _init_coll_mnu_file_Items in range(7)]

[wxID_GUISTRINGTBLMNU_HELPMNU_HELP_ABOUT,
 wxID_GUISTRINGTBLMNU_HELPMNU_HELP_HELP,
] = [wx.NewId() for _init_coll_mnu_help_Items in range(2)]

[wxID_GUISTRINGTBLMNU_TOOLSMNU_TOOLS_ADD_LOCALE,
 wxID_GUISTRINGTBLMNU_TOOLSMNU_TOOLS_IMPORT,
 wxID_GUISTRINGTBLMNU_TOOLSMNU_TOOLS_RELOAD,
] = [wx.NewId() for _init_coll_mnu_tools_Items in range(3)]

[wxID_GUISTRINGTBLMNU_LOCALESMNU_LOCALS_ADD_COLUMN] = [wx.NewId() for _init_coll_mnu_locales_Items in range(1)]

class GUIStringTbl(wx.Frame):
    def _init_coll_globalBoxSizer_Items(self, parent):
        # generated method, don't edit

        parent.Add(self.splitterWindow, 1, border=0,
              flag=wx.ALIGN_BOTTOM | wx.EXPAND | wx.ALIGN_TOP | wx.ALIGN_RIGHT | wx.ALIGN_LEFT)

    def _init_coll_menuBar_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnu_file, title='File')
        parent.Append(menu=self.mnu_tools, title='Tools')
        parent.Append(menu=self.mnu_help, title='Help')

    def _init_coll_mnu_file_Items(self, parent):
        # generated method, don't edit

        parent.Append(helpString='Scan a directory to extract MSG_ tokens',
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SCAN_MSG,
              kind=wx.ITEM_NORMAL, item='Scan MSG')
        parent.Append(helpString='Scan a directory to extract ERR_ tokens',
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SCAN_ERR,
              kind=wx.ITEM_NORMAL, item='Scan ERR')
        parent.Append(helpString='Rescan the previous directory',
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_RESCAN,
              kind=wx.ITEM_NORMAL, item='Rescan')
        parent.Append(helpString='Save the modifications to the string files',
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SAVE, kind=wx.ITEM_NORMAL,
              item='Save string files')
        parent.Append(helpString='Close the data',
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_CLOSE, kind=wx.ITEM_NORMAL,
              item='Close')
        parent.AppendSeparator()
        parent.Append(helpString='Save the selected source code file',
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SAVE_SRC,
              kind=wx.ITEM_NORMAL, item='Save source code file')
        parent.AppendSeparator()
        parent.Append(helpString='Quit the application',
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_QUIT, kind=wx.ITEM_NORMAL,
              item='Quit')
        self.Bind(wx.EVT_MENU, self.OnMnu_file_scan_msg,
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SCAN_MSG)
        self.Bind(wx.EVT_MENU, self.OnMnu_file_scan_err,
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SCAN_ERR)
        self.Bind(wx.EVT_MENU, self.OnMnu_file_rescan,
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_RESCAN)
        self.Bind(wx.EVT_MENU, self.OnMnu_file_close,
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_CLOSE)
        self.Bind(wx.EVT_MENU, self.OnMnu_file_save,
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SAVE)
        self.Bind(wx.EVT_MENU, self.OnMnu_file_save_src,
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SAVE_SRC)
        self.Bind(wx.EVT_MENU, self.OnMnu_file_quit,
              id=wxID_GUISTRINGTBLMNU_FILEMNU_FILE_QUIT)

    def _init_coll_mnu_help_Items(self, parent):
        # generated method, don't edit

        parent.Append(helpString='Help', id=wxID_GUISTRINGTBLMNU_HELPMNU_HELP_HELP,
              kind=wx.ITEM_NORMAL, item='Help')
        parent.Append(helpString='', id=wxID_GUISTRINGTBLMNU_HELPMNU_HELP_ABOUT,
              kind=wx.ITEM_NORMAL, item='About')
        self.Bind(wx.EVT_MENU, self.OnMnu_help_about,
              id=wxID_GUISTRINGTBLMNU_HELPMNU_HELP_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnMnu_help_help,
              id=wxID_GUISTRINGTBLMNU_HELPMNU_HELP_HELP)

    def _init_coll_mnu_tools_Items(self, parent):
        # generated method, don't edit

        parent.Append(helpString='Import all the inherited strings',
              id=wxID_GUISTRINGTBLMNU_TOOLSMNU_TOOLS_IMPORT,
              kind=wx.ITEM_NORMAL, item='Import all inherited')
        parent.Append(helpString='Add a locale',
              id=wxID_GUISTRINGTBLMNU_TOOLSMNU_TOOLS_ADD_LOCALE,
              kind=wx.ITEM_NORMAL, item='Add locale')
        parent.AppendSeparator()
        parent.Append(helpString='Reload Python modules',
              id=wxID_GUISTRINGTBLMNU_TOOLSMNU_TOOLS_RELOAD,
              kind=wx.ITEM_NORMAL, item='Reload')
        self.Bind(wx.EVT_MENU, self.OnMnu_tools_reload,
              id=wxID_GUISTRINGTBLMNU_TOOLSMNU_TOOLS_RELOAD)
        self.Bind(wx.EVT_MENU, self.OnMnu_tools_add_locale,
              id=wxID_GUISTRINGTBLMNU_TOOLSMNU_TOOLS_ADD_LOCALE)
        self.Bind(wx.EVT_MENU, self.OnMnu_tools_import,
              id=wxID_GUISTRINGTBLMNU_TOOLSMNU_TOOLS_IMPORT)

    def _init_coll_notebook_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.dumpWindow, select=True,
              text='Entry')
        parent.AddPage(imageId=-1, page=self.htmlWindow, select=False,
              text='WebFTN')

    def _init_sizers(self):
        # generated method, don't edit
        self.globalBoxSizer = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_globalBoxSizer_Items(self.globalBoxSizer)

        self.splitterWindow.SetSizer(self.globalBoxSizer)

    def _init_utils(self):
        # generated method, don't edit
        self.menuBar = wx.MenuBar()

        self.mnu_file = wx.Menu(title='')

        self.mnu_tools = wx.Menu(title='')

        self.mnu_help = wx.Menu(title='')

        self._init_coll_menuBar_Menus(self.menuBar)
        self._init_coll_mnu_file_Items(self.mnu_file)
        self._init_coll_mnu_tools_Items(self.mnu_tools)
        self._init_coll_mnu_help_Items(self.mnu_help)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_GUISTRINGTBL, name='StringEdt',
              parent=prnt, pos=wx.Point(111, 80), size=wx.Size(901, 614),
              style=wx.SYSTEM_MENU | wx.DEFAULT_FRAME_STYLE,
              title='H2D2 StringTable Editor')
        self._init_utils()
        self.SetClientSize(wx.Size(893, 587))
        self.SetMenuBar(self.menuBar)
        self.SetToolTip('H2D2 String Table Editor')
        self.Bind(wx.EVT_CLOSE, self.OnClose)

        self.splitterWindow = wx.SplitterWindow(id=wxID_GUISTRINGTBLSPLITTERWINDOW,
              name='splitterWindow', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(893, 548), style=wx.CAPTION | wx.SP_3D)
        self.splitterWindow.SetLabel('splitterWindow')
        self.splitterWindow.SetToolTip('')
        self.splitterWindow.SetAutoLayout(False)

        self.grid = wx.grid.Grid(id=wxID_GUISTRINGTBLGRID, name='grid',
              parent=self.splitterWindow, pos=wx.Point(2, 209),
              size=wx.Size(889, 337), style=0)
        self.grid.SetAutoLayout(False)
        self.grid.SetToolTip('')
        self.grid.SetConstraints(LayoutAnchors(self.grid, True, True, True,
              True))
        self.grid.SetMinSize(wx.Size(-1, -1))
        self.grid.Bind(wx.grid.EVT_GRID_CELL_CHANGED, self.OnGrid_CellChange)
        self.grid.Bind(wx.grid.EVT_GRID_LABEL_LEFT_DCLICK,
              self.OnGrid_LabelLeftDclick)
        self.grid.Bind(wx.grid.EVT_GRID_CELL_LEFT_CLICK,
              self.OnGrid_CellLeftClick)
        self.grid.Bind(wx.grid.EVT_GRID_LABEL_LEFT_CLICK,
              self.OnGrid_LabelLeftClick)

        self.statusBar = wx.StatusBar(id=wxID_GUISTRINGTBLSTATUSBAR,
              name='statusBar', parent=self, style=0)
        self.statusBar.SetFieldsCount(2)
        self.statusBar.SetToolTip('')
        self.statusBar.SetStatusText('')
        self.SetStatusBar(self.statusBar)

        self.notebook = wx.Notebook(id=wxID_GUISTRINGTBLNOTEBOOK,
              name='notebook', parent=self.splitterWindow, pos=wx.Point(2, 2),
              size=wx.Size(889, 200), style=0)
        self.notebook.SetToolTip('')
        self.notebook.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED,
              self.OnNotebook_PageChanged, id=wxID_GUISTRINGTBLNOTEBOOK)
        self.splitterWindow.SplitHorizontally(self.notebook, self.grid)

        self.htmlWindow = wx.html.HtmlWindow(id=wxID_GUISTRINGTBLHTMLWINDOW,
              name='htmlWindow', parent=self.notebook, pos=wx.Point(0, 0),
              size=wx.Size(881, 174), style=wx.html.HW_SCROLLBAR_AUTO)
        self.htmlWindow.SetToolTip('')
        self.htmlWindow.SetConstraints(LayoutAnchors(self.htmlWindow, True,
              True, True, True))

        self.dumpWindow = wx.html.HtmlWindow(id=wxID_GUISTRINGTBLDUMPWINDOW,
              name='dumpWindow', parent=self.notebook, pos=wx.Point(0, 0),
              size=wx.Size(881, 174), style=wx.html.HW_SCROLLBAR_AUTO)
        self.dumpWindow.SetToolTip('Outut window for string tbl entry content')
        self.dumpWindow.SetConstraints(LayoutAnchors(self.dumpWindow, True,
              True, True, True))
        self.dumpWindow.Bind(wx.html.EVT_HTML_LINK_CLICKED,
              self.OnHtml_LinkClicked)

        self._init_coll_notebook_Pages(self.notebook)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        #wxDIALOG_EX_CONTEXTHELP

        self.mnu_tools.Enable(wxID_GUISTRINGTBLMNU_TOOLSMNU_TOOLS_RELOAD, False)
        self.mnu_help.Enable(wxID_GUISTRINGTBLMNU_HELPMNU_HELP_HELP, False)

        self.__enableMnu_file_save(False)
        self.__enableMnu_file_rescan(False)
        self.__enableMnu_file_save_src(False)
        self.__enableMnu_tools_add_column(False)
        self.__enableMnu_tools_import(False)

        self.pages = {}

        self.data = StringTbl.StringTbl()
        self.dataModified = False

        self.grid.CreateGrid(10, 2)
        self.grid.SetRowLabelAlignment(wx.ALIGN_LEFT, wx.ALIGN_CENTER)
        self.grid.SetRowLabelSize(250)
        self.grid.SetDefaultColSize(200, resizeExistingCols = True)
        self.grid.EnableDragColSize(True)
        self.grid.EnableDragRowSize(False)

    def __enableMnu_file_save(self, enable):
        self.mnu_file.Enable(wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SAVE, enable)

    def __enableMnu_file_rescan(self, enable):
        self.mnu_file.Enable(wxID_GUISTRINGTBLMNU_FILEMNU_FILE_RESCAN, enable)

    def __enableMnu_file_save_src(self, enable):
        self.mnu_file.Enable(wxID_GUISTRINGTBLMNU_FILEMNU_FILE_SAVE_SRC, enable)

    def __enableMnu_tools_add_column(self, enable):
        self.mnu_tools.Enable(wxID_GUISTRINGTBLMNU_TOOLSMNU_TOOLS_ADD_LOCALE, enable)

    def __enableMnu_tools_import(self, enable):
        self.mnu_tools.Enable(wxID_GUISTRINGTBLMNU_TOOLSMNU_TOOLS_IMPORT, enable)

    ###---  Events
    def OnMnu_file_scan_msg(self, event):
        if (self.dataModified):
            self.__save('Save modified data')
        self.__scan(StringTbl.Type.Message)
        self.__enableMnu_file_save(self.dataModified)
        self.__enableMnu_tools_add_column(True)
        self.__enableMnu_file_rescan(True)

    def OnMnu_file_scan_err(self, event):
        if (self.dataModified):
            self.__save('Save modified data')
        self.__scan(StringTbl.Type.Error)
        self.__enableMnu_file_save(self.dataModified)
        self.__enableMnu_tools_add_column(True)
        self.__enableMnu_file_rescan(True)

    def OnMnu_file_rescan(self, event):
        if (self.dataModified):
            self.__save('Save modified data')
        self.__rescan()
        self.__enableMnu_file_save(self.dataModified)
        self.__enableMnu_tools_add_column(True)

    def OnMnu_file_close(self, event):
        if (self.dataModified):
            self.__save('Save modified data')
        self.dumpWindow.SetPage('')
        data = StringTbl.StringTbl()
        self.__asgDataToGrid(data)
        self.SetTitle(__bubble__)
        self.data = data
        self.dataModified = False
        self.__enableMnu_file_save(False)
        self.__enableMnu_tools_add_column(False)
        self.__enableMnu_file_rescan(False)

    def OnMnu_file_save(self, event):
        if (self.dataModified):
            self.__save()
        else:
            wx.Bell()

    def OnMnu_file_save_src(self, event):
        i = self.notebook.GetSelection()
        s = self.notebook.GetPageText(i)
        win = self.notebook.GetCurrentPage()
        if s[-2:] != ' *':
            raise ValueError('Invalid source file to save: %s', s)
        if self.pages[win][0] != 'Modified':
            raise ValueError('Invalid source file to save: %s', s)

        url = self.pages[win][1]
        dlg = wx.MessageDialog(self, 'Saving: %s' % url, 'Save source file', wx.OK | wx.CANCEL | wx.ICON_QUESTION)
        if dlg.ShowModal() == wx.ID_OK:
            bck = url + '.~bck'
            shutil.copy2(url, bck)
            with open(url, 'w+', encoding='utf-8') as ofs:
                ofs.write( win.GetText() )

            self.notebook.SetPageText( i, os.path.basename(url) )
            self.__enableMnu_file_save_src(False)

    def OnMnu_file_quit(self, event):
        self.Close()

    def OnClose(self, event):
        if (self.dataModified):
            self.__save('Closing application')
        self.Destroy()

    def OnMnu_tools_import(self, event):
        nchange = 0
        cols = self.grid.GetSelectedCols()
        for c in cols:
            if (c < 0):
                wx.Bell()
            else:
                l = self.grid.GetColLabelValue(c)
                nchange += self.data.importInherited(l)

        if (nchange > 0):
            self.__asgDataToGrid(self.data)
            if (not self.dataModified):
                self.dataModified = True
                self.__enableMnu_file_save(True)

    def OnMnu_tools_add_locale(self, event):
        dlg = wx.TextEntryDialog(self, 'New locale')
        ret = dlg.ShowModal()
        if (ret == wx.ID_OK):
            lcl = dlg.GetValue()
            lcl.strip()
            self.data.addLocale(lcl)
            self.__asgDataToGrid(self.data)
            self.dataModified = True
            self.__enableMnu_file_save(True)

    def OnMnu_tools_reload(self, event):
        mdl = sys.modules['GUIStringTbl']
        imp.reload(mdl)
        mdl = sys.modules['StringTbl']
        imp.reload(mdl)

    def OnMnu_help_about(self, event):
        s = '%s - Version %s\n%s\n\n%s' % (__package__, __version__, __bubble__, __copyright__)
        msg = wx.MessageDialog(self, s, __bubble__, wx.OK)
        ret = msg.ShowModal()

    def OnMnu_help_help(self, event):
        event.Skip()

    def OnGrid_CellChange(self, event):
        if (not self.dataModified):
            self.dataModified = True
            self.__enableMnu_file_save(True)
        r = event.GetRow()
        c = event.GetCol()

        t = self.grid.GetRowLabelValue(r)
        l = self.grid.GetColLabelValue(c)
        v = self.grid.GetCellValue(r, c)
        self.data.updateValue(t, l, v)

        self.__asgDataToCell(r, c, self.data[t][l][-1])

    def OnGrid_LabelLeftClick(self, event):
        enable = (event.GetRow() < 0)
        self.__enableMnu_tools_import(enable)
        event.Skip()

    def OnGrid_LabelLeftDclick(self, event):
        r = event.GetRow()
        l = self.grid.GetRowLabelValue(r)
        file = self.data[l].filePos[0].file
        line = self.data[l].filePos[0].line
        line = max(1, line-3)       # Remonte un peu

        file = os.path.basename(file)
        html = file.replace('.', '_')
        html = '%s.%s#L%s' % (html, 'htm', line)
        site = 'http://www.intranet.gre-ehn.inrs-ete.uquebec.ca/doc_technique/H2D2/webftn'
        url = '%s/%s' % (site, html)
        self.htmlWindow.LoadPage(url)
        self.notebook.SetSelection(1)
        self.statusBar.SetStatusText('%s:%s' % (file, line), 0)

    def OnGrid_CellLeftClick(self, event):
        r = event.GetRow()
        c = event.GetCol()
        t = self.grid.GetRowLabelValue(r)
        l = self.grid.GetColLabelValue(c)
        v = self.grid.GetCellValue(r, c)

        e = self.data[t]    # entry
        s = StringIO()
        e.asHtml(s)
        self.dumpWindow.SetPage(s.getvalue())
        s.close()
        self.notebook.SetSelection(0)

        self.__enableMnu_tools_import(False)
        event.Skip()

    def OnNotebook_PageChanged(self, event):
        i = event.GetSelection()
        if (i < 2):
            self.__enableMnu_file_save_src(False)
        else:
            s = self.notebook.GetPageText(i)
            status = (s[-2:] == ' *')
            self.__enableMnu_file_save_src(status)
        event.Skip()

    def OnEditor_Modified(self, event):
        win = event.GetEventObject()
        if (self.pages[win][0] == 'New'):
            pass
        elif (self.pages[win][0] == 'Unmodified'):
            self.pages[win][0] = 'Modified'
            if (self.notebook.GetCurrentPage() == win):
                i = self.notebook.GetSelection()
                s = self.notebook.GetPageText(i)
                s += ' *'
                self.notebook.SetPageText(i, s)
                self.__enableMnu_file_save_src(True)
        elif (self.pages[win][0] == 'Modified'):
            #if (event.GetModificationType() == wx.stc.STC_PERFORMED_UNDO):
            if (not win.CanUndo()):
                self.pages[win][0] = 'Unmodified'
                if (self.notebook.GetCurrentPage() == win):
                    i = self.notebook.GetSelection()
                    s = self.notebook.GetPageText(i)[:-2]
                    self.notebook.SetPageText(i, s)
                    self.__enableMnu_file_save_src(False)
        else:
            raise RuntimeError("Invalid Editor state: %s", self.pages[win][0])

    def OnHtml_LinkClicked(self, event):
        url = event.GetLinkInfo().GetHref()
        if (url[0:7] == 'http://'):
            self.notebook.SetSelection(1)
            self.htmlWindow.LoadPage(event.GetLinkInfo().GetHref())
        else:
            url, lin = url.split('#')
            lin = int(lin)
            bse = os.path.basename(url)
            nbk = self.notebook
            found = -1
            for i in range(0, nbk.GetPageCount()):
                if (nbk.GetPageText(i)[:-2] == bse):
                    found = i
            if (found < 0):
                win = self.__createEditorWindow(bse)
                self.pages[win] = [ 'New', url ]
                win.SetText( open(url, encoding='utf-8').read() )
                win.GotoLine(lin)
                win.EmptyUndoBuffer()
                self.notebook.AddPage(imageId=-1, page=win, select=True, text=bse)
                self.pages[win][0] = 'Unmodified'
            else:
                win = self.notebook.GetPage(i)
                win.GotoLine(lin)
                self.notebook.SetSelection(i)
        #self.statusBar.SetStatusText('%s:%s' % (file, line), 0)

    ###---  Private section
    def __createEditorWindow(self, label):
        win = wx.stc.StyledTextCtrl(id=wx.NewId(),
              name=label, parent=self.notebook, pos=wx.Point(0, 0),
              size=wx.Size(881, 174), style=0)
        win.SetConstraints(LayoutAnchors(win, True, True, True, True))
        win.SetMarginType (1, wx.stc.STC_MARGIN_NUMBER)
        win.SetMarginWidth(1, 38)
        win.SetLexer(wx.stc.STC_LEX_FORTRAN)
        win.SetTabWidth(3)
        win.SetTabIndents(True)
        win.SetUseTabs(False)
        win.StyleSetFont(wx.stc.STC_STYLE_DEFAULT, wx.Font(10, wx.MODERN, wx.NORMAL, wx.NORMAL))
        win.StyleSetSpec(wx.stc.STC_STYLE_LINENUMBER, "back:#C0C0C0,face:Helvetica,size:9")
        win.Bind(wx.stc.EVT_STC_MODIFIED, self.OnEditor_Modified)
        return win

    def __asgDataToCell(self, r, c, lcl):
        """
        Add data to a cell of the grid widget
        """
        clr = wx.BLACK
        if  (lcl.stat == StringTbl.Status.Empty):     clr = wx.BLACK
        elif(lcl.stat == StringTbl.Status.Ok):        clr = wx.BLACK
        elif(lcl.stat == StringTbl.Status.Inherited): clr = wx.BLUE
        elif(lcl.stat == StringTbl.Status.Conflict):  clr = wx.RED
        self.grid.SetCellTextColour(r, c, clr)
        self.grid.SetCellValue(r, c, lcl.str)

    def __asgDataToGrid(self, data):
        """
        Add all the data to the grid widget
        """
        try:
            self.grid.BeginBatch()
            self.grid.ClearGrid()

            keys = list(data.keys())
            keys.sort()

            nRow = len(keys)
            nCol = data.getNbrLocales()
            if (nRow > self.grid.GetNumberRows()):
                self.grid.AppendRows(nRow - self.grid.GetNumberRows())
            elif (nRow < self.grid.GetNumberRows()):
                self.grid.DeleteRows(0, self.grid.GetNumberRows() - nRow)
            if (nCol > self.grid.GetNumberCols()):
                self.grid.AppendCols(nCol - self.grid.GetNumberCols())
            elif (nCol < self.grid.GetNumberCols()):
                self.grid.DeleteCols(0, self.grid.GetNumberCols() - nCol)

            for c in range(0, nCol):
                self.grid.SetColLabelValue(c, data.getLocale(c))

            for r in range(0, nRow):
                k = keys[r]
                self.grid.SetRowLabelValue(r, k)

                entry = data[k]
                for c in range(0, nCol):
                    l = self.grid.GetColLabelValue(c)
                    self.__asgDataToCell(r, c, entry[l][-1])
        finally:
            self.grid.EndBatch()
            self.grid.ForceRefresh()

    def __save(self, label = ''):
        """
        Save the underlying data to file
        """
        if (self.dataModified):
            mustSave = True
            if (label != ''):
                dlg = wx.MessageDialog(self, 'Module has changed\nDo you want to save the changes?', label, wx.YES_NO | wx.CANCEL | wx.ICON_QUESTION)
                ret = dlg.ShowModal()
                if (ret != wx.ID_YES): mustSave = False
            if (mustSave):
                busy = wx.BusyCursor()
                self.data.updateTbl(self)
                self.data.saveTbl()
                self.data.rescanPath()

        self.dataModified = False
        self.__enableMnu_file_save(False)

    def __scan(self, type):
        """
        Scan a directory to load the data
        """
        homeDir = os.environ['INRS_DEV']
        scanDir = os.path.join(homeDir, 'H2D2')
        dlg = wx.DirDialog(self, 'Scan directory', scanDir, wx.DD_DIR_MUST_EXIST)
        ret = dlg.ShowModal()
        if (ret == wx.ID_OK):
            chemin = dlg.GetPath()
            #try:
            if (True):
                busy = wx.BusyCursor()
                data = StringTbl.StringTbl()
                modf = data.scanPath(chemin, type=type)
                self.__asgDataToGrid(data)
                self.SetTitle(chemin)
                self.data = data
                self.dataModified = modf
                self.__enableMnu_file_save(self.dataModified)
            #except:
            #    msg = wx.MessageDialog(self, 'Error scanning %s' % chemin, 'Error', wx.OK | wx.ICON_ERROR)
            #    msg.ShowModal()

    def __rescan(self):
        """
        Scan a directory to load the data
        """
        try:
            busy = wx.BusyCursor()
            self.data.rescanPath()
            self.__asgDataToGrid(self.data)
            self.dataModified = False
            self.__enableMnu_file_save(False)
        except:
            msg = wx.MessageDialog(self, 'Error rescanning %s' % self.data.path, 'Error', wx.OK | wx.ICON_ERROR)
            msg.ShowModal()

    ###---  Grid iterator
    class GridIterator:
        def __init__(self, grid):
            self.grid = grid
            self.indx = 0
            self.imax = self.grid.GetNumberRows()

        def __iter__(self):
            return self

        def __next__(self):
            if (self.indx >= self.imax):
                raise StopIteration
            k = self.grid.GetRowLabelValue(self.indx)
            v = { }
            for ic in range(0, self.grid.GetNumberCols()):
                c_ = self.grid.GetColLabelValue(ic)
                v_ = self.grid.GetCellValue(self.indx, ic)
                v.setdefault(c_, v_)
            self.indx += 1
            return k, v

        next = __next__         # For Python 2

    def __iter__(self):
        return GUIStringTbl.GridIterator(self.grid)
