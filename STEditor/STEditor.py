#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import sys
homeDir = os.environ['INRS_DEV']
sys.path.append( os.path.join(homeDir, 'toolbox'))

import wx

import GUIStringTbl

modules = {
    'GUIStringTbl': [
                    1, 
                    'H2D2 String Table Editor', 
                    os.path.join(homeDir, 'toolbox/GUIStringTbl.py'),
                    ],
          }

class BoaApp(wx.App):
    def OnInit(self):
        self.main = GUIStringTbl.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        return True

def main():
    application = BoaApp(0)
    application.MainLoop()

if __name__ == '__main__':
    main()
