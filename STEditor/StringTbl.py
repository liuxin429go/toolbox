#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import cgi
from io import StringIO
try:
    import hashlib
    hashlib_md5 = hashlib.md5
except ImportError:
    # for Python << 2.5
    import md5
    hashlib_md5 = md5.new
import os
import re
import shutil
import string
import sys

homeDir = os.environ['INRS_DEV']
sys.path.append( os.path.join(homeDir, 'toolbox'))

import PyFtn
import enum
import logging
logger = logging.getLogger("INRS.IEHSS.H2D2.StringTbl")

Type   = enum.Enum('Type',   ('Undefined', 'Message', 'Error'))
Status = enum.Enum('Status', ('Empty', 'Ok', 'Inherited', 'Conflict'))

class StringTbl(dict):
    """
    Dictionnaire dont la clef est le token et qui fait référence à un
    objet de type Entry.
    """
    typePtrn = { Type.Message : r"[^A-Za-z0-9_]MSG_[A-Z0-9_]+",
                 Type.Error   : r"\'ERR_[A-Z0-9_]+\'"}
    typeExtn = { Type.Message : r".msg.stbl",
                 Type.Error   : r".err.stbl" }

    class Entry(dict):
        """
        Une Entry comprend la liste des occurrences du token (FilePos) et un
        dictionnaire. Ce dictionnaire dont la clef est la langue, fait
        référence à une liste de Locale.
        """
        class FilePos:
            def __init__(self, f = '', l = -1):
                self.file = f.strip()
                self.line = l
            def __str__(self):
                if self.line < 0:
                    return toUnicode('%s' % self.file)
                else:
                    return toUnicode('%s:%i' % (self.file, self.line))
            def asHtml(self):
                if len(self.file) <= 0:
                    ret = ''
                else:
                    file = os.path.basename(self.file)
                    html = file.replace('.', '_')
                    html = '%s.%s' % (html, 'htm')
                    if (self.line >= 0):
                        html = '%s#L%s' % (html, self.line)
                    site = 'http://www.intranet.gre-ehn.ete.inrs.ca/doc_technique/H2D2/webftn'
                    url = '%s/%s' % (site, html)
                    ret = '<a href="%s">%s:%i</a>' % (url, self.file, self.line)
                return toUnicode(ret)

        class Locale:
            def __init__(self, t, l, v = '', s = Status.Empty, p = None):
                self.tok  = t
                self.lcl  = l
                self.str  = v.strip()
                self.stat = s
                self.fPos = p
            def __str__(self):
                st = 'Undefined'
                if self.stat == Status.Empty:
                    st = 'Empty'
                elif self.stat == Status.Ok:
                    st = 'Ok'
                elif self.stat == Status.Inherited:
                    st = 'Inherited'
                elif self.stat == Status.Conflict:
                    st = 'Conflict'
                return toUnicode('%s Stat:%s In:%s' % (self.str, st, self.fPos))
            def asHtml(self):
                st = 'Undefined'
                if self.stat == Status.Empty:
                    st = 'Empty'
                elif self.stat == Status.Ok:
                    st = 'Ok'
                elif self.stat == Status.Inherited:
                    st = 'Inherited'
                elif self.stat == Status.Conflict:
                    st = 'Conflict'
                return self.str, toUnicode(st), self.fPos

        def __init__(self, tk, f, l):
            self.token   = tk
            self.filePos = [ StringTbl.Entry.FilePos(f, l) ]

        def addFile(self, f, l):
            self.filePos.append( StringTbl.Entry.FilePos(f, l) )

        def __str__(self):
            res  = 'Token: %s\n' % self.token
            res += 'In:\n'
            for fp in self.filePos:
                res += '\t%s\n' % (fp)
            res += 'Locale:\n'
            for k,v in self.items():
                res += '\t%s\n' % (k)
                for i in v:
                    res += '\t\t%s\n' % (i)
            return res

        def asHtml(self, os):
            def __writeln(os, s):
                u = toUnicode('%s\n' % s)
                os.write(u) # .encode('utf-8') )

            __writeln(os, '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">')
            __writeln(os, '<html>')
            __writeln(os, '<head>')
            __writeln(os, '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">')
            __writeln(os, '<title>%s</title>' % self.token)
            __writeln(os, '</head>')
            __writeln(os, '<body>')

            __writeln(os, '<h3>%s</h3>' % self.token)
            #__writeln(os, '<br>')
            if (len(self) > 0):
                __writeln(os, '<table border=1 cellspacing=1 cellpadding=2>')
                __writeln(os, '<thead>')
                __writeln(os, '<th width=50><b>Locale</b></th>')
                __writeln(os, '<th width=230><b>String</b></th>')
                __writeln(os, '<th width=60><b>Status</b></th>')
                __writeln(os, '<th><b>Where</b></th>')
                __writeln(os, '</thead>')
                for k,v in list(self.items()):
                    if (len(v) > 0):
                        __writeln(os, '<tr>')
                        __writeln(os, '<td><b>%s:</b></td>' % k)
                        v0 = v[0].asHtml()
                        __writeln(os, '<td>%s</td>' % toHtml(v0[0]))
                        __writeln(os, '<td>%s</td>' % toHtml(v0[1]))
                        __writeln(os, '<td>%s</td>' % v0[2])
                        __writeln(os, '</tr>')
                        for i in v[1:]:
                            __writeln(os, '<tr>')
                            __writeln(os, '<td></td>')
                            i0 = i.asHtml()
                            __writeln(os, '<td>%s</td>' % toHtml(i0[0]))
                            __writeln(os, '<td>%s</td>' % toHtml(i0[1]))
                            __writeln(os, '<td>%s</td>' % i0[2])
                            __writeln(os, '</tr>')
                    else:
                        __writeln(os, '<tr>')
                        __writeln(os, '<td width=67><b>Locale %s:</b></td>' % k)
                        __writeln(os, '<td></td>')
                        __writeln(os, '</tr>')
                __writeln(os, '</table>')
            if (len(self.filePos) > 0):
                __writeln(os, '<p>&nbsp;</p>')
                __writeln(os, '<table border=0 cellspacing=0 cellpadding=1>')
                __writeln(os, '<tr>')
                __writeln(os, '<td width=67><b>In:</b></td>')
                __writeln(os, '<td width=20><a href="%s#%i"><img src="ico.gif"></a></td>' % (self.filePos[0].file, self.filePos[0].line))
                __writeln(os, '<td>%s</td>' % self.filePos[0].asHtml())
                __writeln(os, '</tr>')
                for fp in self.filePos[1:]:
                    __writeln(os, '<tr>')
                    __writeln(os, '<td></td>')
                    __writeln(os, '<td><a href="%s#%i"><img src="ico.gif"></a></td>' % (fp.file, fp.line))
                    __writeln(os, '<td>%s</td>' % fp.asHtml())
                    __writeln(os, '</tr>')
                __writeln(os, '</table>')
            __writeln(os, '</body>')
            __writeln(os, '</html>')

    def __init__(self):
        self.path     = ()      # path
        self.skipDir  = []      # skipDir
        self.modul    = ''
        self.type     = Type.Undefined
        self.locales  = []      # List of locales
        self.modules  = {}      # Dic of loaded H2D2 sub-modules

    def __scanFtn(self, dir, skipDir = []):
        for f in os.listdir(dir):
            if f in skipDir: continue
            fullPath = os.path.join(dir,f)
            fullPath = os.path.normpath(fullPath)
            if os.path.isdir(fullPath):
                self.__scanFtn(fullPath, skipDir)
            elif (os.path.splitext(fullPath)[1].lower() == '.for'):
                self.__scanFtnUnFichier(fullPath)

    def __scanFtnUnFichier(self, dir):
        logger.debug('__scanFtnUnFichier: %s' % dir)
        fic = PyFtn.File(dir)
        fic.setInclude(False)
        fic.setStripCmt(True)
        try:
            ptrn = re.compile( StringTbl.typePtrn[self.type] )
            while (fic):
                st = str(next(fic))
                tks = ptrn.findall(st)
                for tk in tks:
                    #print(tk, st)
                    while (tk[0] < 'A' or tk[0] > 'Z'): tk = tk[1:]
                    if (tk[-1] == "'"): tk = tk[:-1]
                    f = fic.getCurrentFile()
                    l = fic.getCurrentLine() - 1
                    if tk in self:
                        self[tk].addFile(f, l)
                    else:
                        self.setdefault(tk, StringTbl.Entry(tk, f, l))
        except StopIteration:
            pass

    def __scanTbl(self, dir, status, skipDir = []):
        ext = StringTbl.typeExtn[self.type]
        lxt = len(ext)
        for f in os.listdir(dir):
            if f in skipDir: continue
            fullPath = os.path.join(dir,f)
            fullPath = os.path.normpath(fullPath)
            if os.path.isdir(fullPath):
                self.__scanTbl(fullPath, status, skipDir)
            elif (fullPath[-lxt:].lower() == ext):
                self.__scanTblUnFichier(fullPath, status)

    def __scanTblUnFichier(self, fullPath, status):
        logger.debug('__scanTblUnFichier: %s' % fullPath)
        dir = os.path.split(fullPath)[0]
        lng = os.path.basename(dir)

        self.addLocale(lng)

        fic = open(fullPath, 'r', encoding='utf-8')
        i = 0
        for l in fic:
            i += 1
            l = l.strip()
            if (len(l) == 0): continue
            if (l[0] == ';'): continue
            if (l[0] == '#'): continue

            key, val = l.split('=', 1)
            key.strip()
            val.strip()
            if (len(val) == 0): continue

            try:
                lcl = StringTbl.Entry.Locale(key, lng,  val, status, StringTbl.Entry.FilePos(fullPath, i) )
                if (self[key][lng][0].stat == Status.Empty):
                    self[key][lng] = [ lcl ]
                else:
                    self[key][lng].append(lcl)
                    cflict = False
                    for l in self[key][lng]:
                        if (l.str != lcl.str):
                            cflict = True
                    if (cflict):
                        for l in self[key][lng]:
                            l.stat = Status.Conflict
            except KeyError:
                pass

    def __scanSConscript(self, dir, skipDir = []):
        for f in os.listdir(dir):
            if f in skipDir: continue
            fullPath = os.path.join(dir,f)
            fullPath = os.path.normpath(fullPath)
            if os.path.isdir(fullPath):
                self.__scanSConscript(fullPath, skipDir)
            elif (f == 'SConscript'):
                self.__scanSConscriptUnFichier(fullPath)

    def __scanSConscriptUnFichier(self, fullPath):
        logger.debug('__scanSConscriptUnFichier: %s' % fullPath)
        mdl = fullPath
        mdl = os.path.split(mdl)[0]
        bse, mdl = os.path.split(mdl)

        try:
            dummy = globals()['MUC_SCons']
        except:
            m = __import__('MUC_SCons')
            globals()['MUC_SCons'] = m
            globals()['Import'] = getattr(m, 'Import')
            globals()['env']    = getattr(m, 'env')
            globals()['bld']    = getattr(m, 'bld')
            globals()['mpi']    = getattr(m, 'mpi')
            globals()['ctx']    = getattr(m, 'ctx')

        exec(compile(open(fullPath).read(), fullPath, 'exec'))
        try:
            for l in Import('ctx').lpt:
                if (l.find('//') <= 0): continue
                m = l.split('//')[1]
                m = os.path.join(bse, m)
                m = os.path.normpath(m)
                self.__scanMdl(m, status = Status.Inherited)
        except TypeError:
            pass

    def __scanMdl(self, fullPath, status = Status.Ok, skipDir = []):
        if not os.path.isdir(fullPath): return
        if fullPath in self.modules: return

        self.modules[fullPath] = ''
        logger.debug('__scanMdl: %s' % fullPath)
        self.__scanSConscript(fullPath, skipDir)
        self.__scanTbl(fullPath, status, skipDir)

    def checkModified(self):
        for l in self.locales:
            nom  = self.modul + StringTbl.typeExtn[self.type]

            path = os.path.join(self.path, 'locale', l)
            full = os.path.join(path, nom)

            if not os.path.isdir(path):
                return True

            try:
                ori_f = open(full, 'r', encoding='utf-8')
                ori = ori_f.read()
                new = self.asString(l)
                ori_md5 = hashlib_md5( ori.encode('utf-8') )
                new_md5 = hashlib_md5( new.encode('utf-8') )
                ori_f.close()
                if (new_md5.digest() != ori_md5.digest()):
                    return True
            except:
                return (len(self) > 0)
        return False

    def checkFull(self):
        n = 0
        for t, e, in self.items():
            for k,v in e.items():
                for i in v:
                    if (not 1 or i.str == ""):
                        n = n + 1
        return (n == 0)

    def scanPath(self, path, skipDir = [], type = Type.Message):
        self.path   = path
        self.skipDir= skipDir
        self.type   = type
        self.modul  = os.path.basename(path)

        self.clear()
        self.locales = []

        self.isModified = False
        self.__scanFtn(path, skipDir)
        self.__scanMdl(path, skipDir)

        self.isModified = self.checkModified()
        return self.isModified

    def rescanPath(self):
        self.clear()
        self.locales = []
        self.modules = {}
        self.__scanFtn(self.path, skipDir=self.skipDir)
        self.__scanMdl(self.path, skipDir=self.skipDir)

    def asString(self, l):
        os = StringIO()
        keys = list(self.keys())
        keys.sort()
        for k in keys:
            e = self[k][l][-1]
            s = '='.join( [k, e.str] )
            os.write('%s\n' % s)
        return os.getvalue()

    def updateTbl(self, ctnr):
        for t,row in ctnr:
            for l,v in list(row.items()):
               self.updateValue(t, l, v)

    def saveTbl(self):
        keys = list(self.keys())
        keys.sort()
        for l in self.locales:
            nom  = self.modul + StringTbl.typeExtn[self.type]

            path = os.path.join(self.path, 'locale', l)
            full = os.path.join(path, nom)

            if not os.path.isdir(path):
                os.makedirs(path)
            if os.path.isfile(full):
                bck = full + '.~bck'
                shutil.copy2(full, bck)
            with open(full, 'w', encoding='utf-8') as f:
                for k in keys:
                    e = self[k][l][-1]
                    s = '='.join( [k, e.str] )
                    f.write('%s\n' % s)

    def updateValue(self, t, l, v):
        self[t][l][-1].str  = v
        self[t][l][-1].stat = Status.Ok
        cflict = False
        for l in self[t][l]:
            if (l.str != v):
                cflict = True
        if (cflict):
            for l in self[t][l]:
                l.stat = Status.Conflict

    def importInherited(self, lng):
        nchange = 0
        for t, e, in list(self.items()):
            s = e[lng][-1].stat
            if (s == Status.Inherited):
                val = e[lng][0].str
                lcl = StringTbl.Entry.Locale(t, lng,  val, Status.Ok, StringTbl.Entry.FilePos() )
                e[lng].append(lcl)
                nchange += 1
            elif (s == Status.Conflict):
                val = e[lng][0].str
                lcl = StringTbl.Entry.Locale(t, lng,  val, Status.Ok, StringTbl.Entry.FilePos() )
                e[lng][-1] = lcl
                e[lng][0].stat = Status.Inherited
                nchange += 1
        return nchange

    def addLocale(self, l):
        if (l in self.locales): return
        keys = list(self.keys())
        for k in keys:
            self[k].setdefault(l, [ StringTbl.Entry.Locale(k, l) ] )
        self.locales.append(l)

    def getLocale(self, i):
        return self.locales[i]

    def getNbrLocales(self):
        return len(self.locales)

def toUnicode(s):
   return '%s' % s

def toHtml(s):
    r = ''
    for c in s:
        if   c == '&':
            t = '&amp;'
        elif c == '<':
            t = '&lt;'
        elif c == '>':
            t = '&gt;'
        elif c >= '¢':
            t = '&#%d;' % ord(c)
        else:
            t = c;
        r = r + t
    return r


if __name__ == '__main__':
    streamHandler = logging.StreamHandler()
    logger.addHandler(streamHandler)
    logger.setLevel(logging.DEBUG)
    #logger.setLevel(logging.INFO)

    homeDir = os.environ['INRS_DEV']
    scanDir = os.path.join(homeDir, 'H2D2', 'b_mumps')

    s = StringTbl()
    s.scanPath(scanDir, type=Type.Message)

    keys= list(s.keys())
    keys.sort()
    for k in keys:
        #print u'%s' % s[k]
        print(('%s' % k))
