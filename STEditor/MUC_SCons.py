#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

def Import(s):
    return globals()[s]

class Context:
    def __init__(self, bld=[], mpi=[], dir=[], prj=[], grp=[], src=[], inc=[], dfn=[], lpt=[], lib=[], lxt=[]):
        self.lpt = None
        self.lxt = None
        if (len(lxt) > 0):
            Import('ctx').lxt = lxt
            Import('ctx').lpt = lpt

class Environment:
    def __init__(self, *args):
        pass
    def SConscript(self, *args, **kargs):
        return 'Dummy SCons.Environment'
    def MUC_Program(self, *args):
        return 'Dummy MUC_SCons'
    def MUC_SharedLibrary(self, *args):
        return 'Dummy MUC_SCons'
    def MUC_StaticLibrary(self, *args):
        return 'Dummy MUC_SCons'
    def MUC_Run (self, *args):
        return 'Dummy MUC_SCons'
    def MUC_Stage (self, *args):
        return 'Dummy MUC_SCons'
    def MUC_TarBz2(self, *args):
        return 'Dummy MUC_SCons'
    def __getitem__(self, k):
        return 'Dummy MUC_SCons'

env = Environment()
bld = []
mpi = []
ctx = Context()
