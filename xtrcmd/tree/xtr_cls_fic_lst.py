#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Extrait les commandes de H2D2
"""

import enum
import glob
import os
import xml.etree.ElementTree

def if_else(p, n1, n2):
    if (p): return n1
    return n2

class Info:
    type = enum.Enum('type', ('NIL', 'Module', 'Classe', 'Command', 'Method'))

    def __init__(self, name = ''):
        self.file = None
        self.dep  = []
        self.name = name
        self.type = Info.type.NIL
#        if self.classe:  self.grp = self.classe.split('_')[0]
#        if self.command: self.grp = self.command.split('_')[0]

    def __eq__(self, other):
        return (self.name == other.name)

    def isMdl(self):
        return self.type == Info.type.Module

    def isCls(self):
        return self.type == Info.type.Classe

    def isCmd(self):
        return self.type == Info.type.Command

class Writer:
    def __init__(self, fname):
        self.fout = open(fname, 'w', encoding='utf-8')

    def __writeLine(self, l):
        self.fout.write('%s\n' % l)

    def __writeHdr(self):
        hdr = 'H2D2 - Commands menu'
        self.__writeLine(hdr)

    def __writeFtr(self):
        ftr = ''
        self.__writeLine(ftr)

    def __writeOneCmd(self, i):
        f = i.file
        n = i.name
        t = 'command'
        self.__writeLine( '\t\t\t<li><a target="main" href="%s#%s">%s</a></li>' % (f, n, n))

    def __writeOneCls(self, i):
        f = i.file
        n = i.name
        t = 'class'
        self.__writeLine( '\t\t\t<li><a target="main" href="%s#%s">%s</a></li>' % (f, n, n))

    def __writeMdl(self, info):
        self.__writeLine('\t<li class="liClosed">%s' % info.name)
        self.__writeLine('\t\t<ul>')
        for i in info.dep:
            if   (i.isMdl()):
                self.__writeMdl(i)
            elif (i.isCls()):
                self.__writeOneCls(i)
            elif (i.isCmd()):
                self.__writeOneCmd(i)
        self.__writeLine('\t\t</ul>')
        self.__writeLine('\t</li>')

    def __writeMenu(self, infos):
        infos.sort()

        self.__writeLine('<!-- tree -->')
        self.__writeLine('<a href="#" onClick="expandTree(\'tree1\'); return false;"><img src="plus.gif" alt="Expand all"></a>')
        self.__writeLine('<a href="#" onClick="collapseTree(\'tree1\'); return false;"><img src="minus.gif" alt="Collapse all"></a>')
#        self.__writeLine('<A href="#" onClick="expandToItem(\'tree1\',\'login\'); return false;">Expand and find login.html</a>')
        self.__writeLine('')

        self.__writeLine('<ul class="mktree" id="tree1">')
        for k in keys:
            self.__writeLine( '\t<li class="liClosed">%s' % (k))

            cls_infos = dico[k]
            cls_infos.sort()
            self.__writeLine('\t\t<ul>')
            for i in cls_infos:
                f = i.file
                n = if_else(i.classe, i.classe, i.command)
                t = if_else(i.classe, 'class', 'command')
                self.__writeLine( '\t\t\t<li><a target="main" href="%s#%s">%s</a></li>' % (f, n, n))
            self.__writeLine('\t\t</ul>')
            self.__writeLine('\t</li>')

        self.__writeLine('</ul>')

    def write(self, cmds):
        self.__writeHdr()
        self.__writeMenu(cmds)
        self.__writeFtr()

def readInfos(f, infos):
    tree = xml.etree.ElementTree.parse(f)
    root = tree.getroot()
    try:
        name = root.attrib['name']
        assert (name not in infos)
        file = root.attrib['file']
        file = os.path.basename(file)
        infos[name] = [ file ]
    except KeyError:
        pass

    for child in root:
        name = child.attrib['name']
        infos[name] = [ file ]
    return infos

def main():
    infos = {}
    for f in glob.glob('*.tree.xml'):
        readInfos(f, infos)
    for i in infos: print(i)
#    w = Writer("_h2d2_cmd_tree.html")
#    w.write(infos)

main()
