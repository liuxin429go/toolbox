@echo off
set MDL=%1
set FIC=%2
if not [%FIC%] == [] goto xeq_action
if not [%FIC%] == [] goto xeq_action

:prompt_user
set /P MDL=Module:
set /P FIC=File:
goto xeq_action

:xeq_action
set SRC=%INRS_DEV%\H2D2\%MDL%\source\%FIC%.for
set DST=%FIC%.html
set DST=_a.html
set COD=%INRS_DEV%\toolbox\xtrcmd\xtrcmd.py
set ARG=@%INRS_DEV%\H2D2\h2d2.i -f html -k module=%MDL%

@echo on
python %COD% %ARG% -o %DST% %SRC%
pause