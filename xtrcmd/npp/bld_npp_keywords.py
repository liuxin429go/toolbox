#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Extrait les commandes de H2D2
"""

import enum
import glob

Types = enum.Enum('Types', ('module', 'classe', 'command', 'method'))

class Info:
    def __init__(self, fic, typ, name, parent):
        if (typ == 'module'):  type = Types.module
        if (typ == 'class'):   type = Types.classe
        if (typ == 'command'): type = Types.command
        if (typ == 'method'):  type = Types.method
        self.file = fic
        self.type = type
        self.name = name
        self.prnt = parent

class Infos:
    def __init__(self):
        self.infos = []

    def __iter__(self):
        return self.infos.__iter__()

    def __next__(self):
        return self.infos.__next__()

    def __xtrTags(self, l):
        type = None
        name = None
        for t in l.split(' ', 1):
            var, val = t.split('=')
            var = var.strip()
            if (var == 'type'): type = val.strip()[1:-1]
            if (var == 'name'): name = val.strip()[1:-1]
        return type, name

    def readInfos(self, f):
        stk = [None]
        for l in open(f, 'r'):
            l = l.strip()
            if (l[0:10] == '<!--Entry '):
                l = l[10:-3].strip()
                type, name = self.__xtrTags(l)
                info = Info(f, type, name, stk[-1])
                self.infos.append(info)
                if (type == 'module'): stk.append(info)
                if (type == 'class'):  stk.append(info)
            elif (l[0:8] == '<!--End '):
                l = l[8:-3].strip()
                type, name = self.__xtrTags(l)
                if (type == 'module'): stk.pop()
                if (type == 'class'):  stk.pop()

    def tidyInfos(self):
        for i1 in self.infos:
            for i2 in self.infos:
                p2 = i2.prnt
                if p2 and p2.name == i1.name and p2.type == i1.type: i2.prnt = i1

    def getInfosWithParent(self, prnt):
        dico = {}
        for info in self.infos:
            if info.prnt is prnt:
                dico[info.name] = info
        return dico


import os

class Writer:
    def __init__(self, fname):
        self.fout = open(fname, 'w')

    def __writeLine(self, l):
        self.fout.write( '%s\n' % (l) )

    def __writeIndex(self, dico):
        keys = list(dico.keys())
        keys.sort()
        for key in keys:
            self.__writeLine(key)

    def __writeMenu(self, infos):
        dico1 = {}
        for info in infos:
            if info.type == Types.method: continue
            if info.prnt is None and info.type == Types.module:
                mdl_from_file = os.path.splitext(info.file)[0]
                if info.name != mdl_from_file: continue
            dico1[info.name] = None
        self.__writeLine('<Keywords name="Keywords3">')
        self.__writeIndex(dico1)
        self.__writeLine('</Keywords>')

        dico2 = {}
        for info in infos:
            if info.type != Types.method: continue
            if info.name in dico1: continue
            dico2[info.name] = None
        self.__writeLine('<Keywords name="Keywords4">')
        self.__writeIndex(dico2)
        self.__writeLine('</Keywords>')

    def write(self, cmds):
        self.__writeMenu(cmds)


def main():
    infos = Infos()
    for f in glob.glob('*.html'):
        infos.readInfos(f)
    infos.tidyInfos()
    w = Writer("npp_keywords.xml")
    w.write(infos)

main()
