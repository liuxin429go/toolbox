#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import platform
import re
from bs4 import BeautifulSoup

import H2D2Commands

"""
Add indent level to prettify output
https://stackoverflow.com/questions/15509397/custom-indent-width-for-beautifulsoup-prettify
"""
orig_prettify = BeautifulSoup.prettify
r = re.compile(r'^(\s*)', re.MULTILINE)
def prettify(self, encoding=None, formatter="minimal", indent_width=4):
    return r.sub(r'\1' * indent_width, orig_prettify(self, encoding, formatter))
BeautifulSoup.prettify = prettify

DOCTYPE   = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">'
COPYRIGHT = '''
<!-- ************************************************************************ -->
<!--  h2d2 xtrcmd 19.02 -->
<!--  Copyright (c) INRS 2006-2018 -->
<!--  Institut National de la Recherche Scientifique (INRS) -->
<!--  Copyright (c) INRS 2019 Yves Secretan -->
<!--  -->
<!--  Distributed under the GNU Lesser General Public License, Version 3.0. -->
<!--  See accompanying file LICENSE.txt. -->
<!-- ************************************************************************ -->

<!-- File generated automatically, changes will be lost on next update -->
'''

class Writer:
    def __init__(self, fname):
        self.fnam = fname
        self.onam = None
        self.fout = None

    def __openFile(self, n):
        if (not self.fnam or self.fnam == ""):
            if (self.fout): self.__closeFile()
            self.onam = n+'.html'
            self.fout = open(self.onam, 'w', encoding='utf-8')
        else:
            if (not self.fout): 
                self.onam = self.fnam
                self.fout = open(self.onam, 'w',  encoding='utf-8')

    def __closeFile(self):
        if self.fout:
            self.fout.close()
            self.__tidy()
        self.fout = None

    def __tidy(self):
        with open(self.onam, 'r', encoding='utf-8') as fout:
            tree = BeautifulSoup(fout, features="html.parser") # lxml 
        odoc = tree.prettify(indent_width=2)
        with open(self.onam, 'w', encoding='utf-8') as fout:
            fout.write(odoc)
    
    def __writeLine(self, l):
        self.fout.write('%s\n' % l)

    def __writeHdr(self, titleGeneric, titleSpecific, tags):
        self.__writeLine(DOCTYPE)
        self.__writeLine(COPYRIGHT)
        self.__writeLine('<html>')
        self.__writeLine('<head>')
        self.__writeLine('<meta http-equiv="Content-Type" content="text/html; charset=utf-8">')
        self.__writeLine('<meta name="keywords" content="%s">' % (', '.join(tags)) )
        self.__writeLine('<link rel="stylesheet" type="text/css" href="h2d2_cmd.css">')
        self.__writeLine('<title>%s</title>' % titleGeneric)
        self.__writeLine('</head>')
        self.__writeLine('<body>')
        if (self.module != ''):
            self.__writeLine('')
            self.__writeLine( '<!--Entry type="module" name="%s" -->' % self.module)

    def __writeFtr(self):
        if (self.module != ''):
            self.__writeLine('<!--End type="module" name="%s" -->' % self.module)
            self.__writeLine('')
        self.__writeLine('</body>')
        self.__writeLine('</html>')

    def __writeMenu(self):
        pass
        #if (self.module != ''):
        #    self.__writeLine( '<!--Entry type="module" name="%s" -->' % self.module)
        #    self.__writeLine('')

    def __writeOneFncFnc(self, mth):
        l = ''
        for a in mth.args:
            l += ', ' + a.name
        if (l): l = l[2:]

        self.__writeLine('<table>')
        line  = '<tr>'
        if mth.type != '##__unset__##':
            line +=     '<td class="fnc_kind">%s</td>' % mth.type
        line +=     '<td class="fnc_name">'
        if mth.rtrn:
            line +=         '<span class="fnc_type">%s</span> ' % mth.rtrn.type
        line +=         '%s' % mth.name
        line +=         '(<span class="fnc_args">%s</span>)' % l
        line +=     '</td>'
        line += '</tr>'
        self.__writeLine(line)
        self.__writeLine('</table>')
        if mth.desc:
            self.__writeLine('<div class="fnc_desc">')
            for l in mth.desc: self.__writeLine('%s' % l)
            self.__writeLine('</div>')

        if (len(mth.args) > 0 or mth.rtrn):
            self.__writeLine('<div class="args"><table>')
            if (mth.rtrn):
                a = mth.rtrn
                self.__writeLine('<tr>')
                self.__writeLine('   <td class="arg_type">%s</td>' % a.type)
                self.__writeLine('   <td class="arg_name">%s</td>' % a.name)
                self.__writeLine('   <td class="arg_desc">')
                for l in a.desc: self.__writeLine(l)
                self.__writeLine('   </td>')
                self.__writeLine('</tr>')
            for a in mth.args:
                self.__writeLine('<tr>')
                self.__writeLine('   <td class="arg_type">%s</td>' % a.type)
                self.__writeLine('   <td class="arg_name">%s</td>' % a.name)
                self.__writeLine('   <td class="arg_desc">')
                for l in a.desc: self.__writeLine(l)
                self.__writeLine('   </td>')
                self.__writeLine('</tr>')
            self.__writeLine('</table></div>')

    def __writeOneFncOper(self, mth):
        l = ''
        for a in mth.args:
            l += ', ' + a.name
        if (l): l = l[2:]

        self.__writeLine('<table>')
        line  = '<tr>'
        line +=     '<td class="fnc_kind">%s</td>' % mth.type
        line +=     '<td class="fnc_name">%s' % mth.name
        line +=         '(<span class="fnc_args">%s</span>)' % l
        line +=     '</td>'
        line += '</tr>'
        self.__writeLine(line)
        self.__writeLine('</table>')
        if mth.desc:
            self.__writeLine('<div class="fnc_desc">')
            for l in mth.desc: self.__writeLine('%s' % l)
            self.__writeLine('</div>')

        if len(mth.args) > 0:
            self.__writeLine('<div class="args"><table>')
            for a in mth.args:
                self.__writeLine('<tr>')
                self.__writeLine('   <td class="arg_type">%s</td>' % a.type)
                self.__writeLine('   <td class="arg_name">%s</td>' % a.name)
                self.__writeLine('   <td class="arg_desc">')
                for l in a.desc: self.__writeLine('%s' % l)
                self.__writeLine('   </td>')
                self.__writeLine('</tr>')
            self.__writeLine('</table></div>')

    def __writeOneFncProp(self, mth):
        self.__writeLine('<table>')
        line  = '<tr>'
        line +=     '<td class="fnc_kind">%s</td>' % mth.type
        line +=     '<td class="fnc_name">%s</td>' % mth.name
        line += '</tr>'
        self.__writeLine(line)
        self.__writeLine('</table>')
        if mth.desc:
            self.__writeLine('<div class="fnc_desc">')
            for l in mth.desc: self.__writeLine('%s' % l)
            self.__writeLine('</div>')

    def __writeOneFnc(self, f):
        self.__writeLine( '<!--Entry type="method" name="%s" -->' % f.name)
        self.__writeLine('<div class="method">')
        if (f.type == 'property'):
            self.__writeOneFncProp(f)
        elif (f.type == 'getter'):
            self.__writeOneFncProp(f)
        elif (f.type == 'setter'):
            self.__writeOneFncProp(f)
        elif (f.type == 'operator'):
            self.__writeOneFncOper(f)
        elif (f.type == '##__unset__##'):
            self.__writeOneFncFnc(f)
        else:
            self.__writeOneFncFnc(f)
        self.__writeLine('</div>')

    def __writeOneCmd(self, mth):
        self.__writeLine( '<!--Entry type="command" name="%s" -->' % mth.name)
        self.__writeLine('<div class="command">')
        self.__writeOneFncFnc(mth)
        self.__writeLine('</div>')
        self.__writeLine('<!--End type="command" name="%s" -->' % mth.name)

    def __writeOneCls(self, cls):
        self.__writeLine( '<!--Entry type="class" name="%s" -->' % cls.name)
        self.__writeLine('<div class="classe">')
        self.__writeLine('<table>')
        line  = '<tr>'
        line +=     '<td class="bnr_kind">%s</td>' % 'class'
        line +=     '<td class="bnr_name"><a name="%s">%s</a></td>' % (cls.name, cls.name)
        line += '</tr>'
        self.__writeLine(line)
        self.__writeLine('</table>')
        if cls.desc:
            self.__writeLine('<div class="fnc_desc">')
            for l in cls.desc: self.__writeLine('%s' % l)
            self.__writeLine('</div>')

        for c in cls.ctrs:  self.__writeOneFnc(c)
        for m in cls.meths: self.__writeOneFnc(m)
        for m in cls.sttcs: self.__writeOneFnc(m)
        self.__writeLine('</div>')
        self.__writeLine('<!--End type="class" name="%s" -->' % cls.name)

    def __writeOneMdl(self, mdl):
        self.__writeLine( '<!--Entry type="module" name="%s" -->' % mdl.name)
        self.__writeLine('<div class="classe">')

        self.__writeLine('<table>')
        line  = '<tr>'
        line +=     '<td class="bnr_kind">%s</td>' % 'module'
        line +=     '<td class="bnr_name"><a name="%s">%s</a></td>' % (mdl.name, mdl.name)
        line += '</tr>'
        self.__writeLine(line)
        self.__writeLine('</table>')
        if mdl.desc:
            self.__writeLine('<div class="fnc_desc">')
            for l in mdl.desc: self.__writeLine('%s' % l)
            self.__writeLine('</div>')

        for c in mdl.cmps:
            if (isinstance(c, H2D2Commands.Mdl)): self.__writeOneMdl(c)
            if (isinstance(c, H2D2Commands.Cls)): self.__writeOneCls(c)
            if (isinstance(c, H2D2Commands.Fnc)): self.__writeOneFnc(c)

        self.__writeLine('</div>')
        self.__writeLine('<!--End type="module" name="%s" -->' % mdl.name)

    def __writeMdl(self, mdl):
        self.__openFile(mdl.name)
        self.__writeHdr('H2D2 - Commands', mdl.name, self.tags)
        self.__writeMenu()
        self.__writeOneMdl(mdl)
        self.__writeFtr()
        self.__closeFile()

    def __writeCls(self, cls):
        self.__openFile(cls.name)
        self.__writeHdr('H2D2 - Commands', cls.name, self.tags)
        self.__writeMenu()
        self.__writeOneCls(cls)
        self.__writeFtr()
        self.__closeFile()

    def __writeCmd(self, mdl):
        self.__openFile(mdl.name)
        self.__writeHdr('H2D2 - Commands', mdl.name, self.tags)
        self.__writeMenu()
        self.__writeOneCmd(mdl)
        self.__writeFtr()
        self.__closeFile()

    def __scanForTags(self, itm, tags):
        if (isinstance(itm, H2D2Commands.Mdl)): tags.append(itm.name)
        if (isinstance(itm, H2D2Commands.Cls)): tags.append(itm.name)
        try:
            for c in itm.cmps: self.__scanForTags(c, tags)
        except:
            pass
        try:
            for c in itm.meths: self.__scanForTags(c, tags)
        except:
            pass

    def __parseArgs(self, kwargs):
        self.module = ''
        self.xlink  = False
        if 'module' in kwargs: self.module = kwargs['module']
        if 'xlink'  in kwargs: self.xlink = True

    def write(self, cmds, **kwargs):
        self.__parseArgs(kwargs)
        self.cmds = cmds

        for c in cmds:
            self.cmdActu = c
            self.tags = ['H2D2']
            self.__scanForTags(c, self.tags)
            if (isinstance(c, H2D2Commands.Mdl)): self.__writeMdl(c)
            if (isinstance(c, H2D2Commands.Cls)): self.__writeCls(c)
            if (isinstance(c, H2D2Commands.Fnc)): self.__writeCmd(c)
