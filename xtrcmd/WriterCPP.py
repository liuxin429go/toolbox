#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import H2D2Commands
from  H2D2Commands import Cls, Fnc
import sys

modeDebug = False
def printDebug(s):
    if (modeDebug):
        print(s)

class Writer:
    def __init__(self, fname):
        self.fout = open(fname, 'w', encoding='utf-8')
        self.indt = 0

    def writeHdr(self):
        printDebug('In writeHdr:')
        pass

    def writeFtr(self):
        printDebug('In writeFtr:')
        pass

    def write(self, c):
        printDebug('In write:')
        if (isinstance(c, Cls)): self.__writeCls(c)
        if (isinstance(c, Fnc)): self.__writeCmd(c)

    def __writeCls(self, cls):
        self.indt = 0
        title = "class %s" % cls.name
        self._prettyPrint(title)
        self._prettyPrint('=' * len(title))
        self._prettyPrint(' ')
        if (cls.desc):
            self.indt += 1
            self._prettyPrint("%s\n" % cls.desc)
            self.indt -= 1

        self.indt += 1
        self._writeCtr(cls.ctr)
        self._writeMth(cls.meths)

    def __writeCmd(self, cmd):
        printDebug('In writeCmd:')
        self.indt = 0
        self._writeOneFnc(cmd)

    def _writeCtr(self, ctr):
        self._writeMth([ctr])

    def _writeMth(self, mth):
        for m in mth:
            if (m.type == 'property'):
                self._writeOneProp(m)
            elif (m.type == 'parameter'):
                self._writeOneProp(m)
            elif (m.type == 'operator'):
                self._writeOneOper(m)
            else:
                self._writeOneFnc(m)
            self._prettyPrint(' ')

    def _writeOneFnc(self, mth):
        printDebug('In writeOneFnc:')
        l = ''
        for a in mth.args:
            l += ', ' + a.name
        if (l): l = l[2:]

        line = "%s" % mth.type
        if (mth.rtrn): line += " %s" % mth.rtrn.type
        line += " %s" % mth.name
        line += "(%s)" % l
        self._prettyPrint(line)
        if (mth.desc):
            self.indt += 1
            self._prettyPrint("%s" % mth.desc)
            self.indt -= 1

        self.indt += 2
        if (mth.rtrn):
            a = mth.rtrn
            self._prettyPrint("%-10s%-12s%s" %(a.type, a.name, a.desc))
        for a in mth.args:
            self._prettyPrint("%-10s%-12s%s" %(a.type, a.name, a.desc))
        self.indt -= 2

    def _writeOneOper(self, mth):
        l = ''
        for a in mth.args:
            l += ', ' + a.name
        if (l): l = l[2:]

        self._prettyPrint("%s %s" % (mth.type, mth.name))
        if (mth.desc):
            self.indt += 1
            self._prettyPrint("%s" % mth.desc)
            self.indt -= 1

        self.indt += 2
        for a in mth.args:
            self._prettyPrint("%-10s%-12s%s" %(a.type, a.name, a.desc))
        self.indt -= 2


    def _writeOneProp(self, mth):
        self._prettyPrint("%s %s" % (mth.type, mth.name))
        if (mth.desc):
            self.indt += 1
            self._prettyPrint("%s" % mth.desc)
            self.indt -= 1

    def _prettyPrint(self, s, maxlen=75, split=' '):

        # Tack on the splitting character to guarantee a final match
        string = s + split

        lines   = []
        oldeol  = 0
        eol     = 0
        while not (eol == -1 or eol == len(string)-1):
            eol = string.rfind(split, oldeol, oldeol+maxlen+len(split))
            line = string[oldeol:eol]
            line = line.rjust(len(line) + self.indt*3)
            self.fout.write(line + '\n')
            oldeol = eol + len(split)
