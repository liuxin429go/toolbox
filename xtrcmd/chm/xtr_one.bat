set MDL=%1
set FIC=%2
set SRC=%INRS_DEV%\H2D2\%MDL%\source\%FIC%.for
set DST=%FIC%.html

set COD=%INRS_DEV%\toolbox\xtrcmd\xtrcmd.py
set ARG=@%INRS_DEV%\H2D2\h2d2.i -f html -k modul=%MDL%

python %COD% %ARG% -o %DST% %SRC%