set SRC_DIR=%1
if ~%SRC_DIR%~==~~ set SRC_DIR=%INRS_DEV%\H2D2

python xtr_all.py %SRC_DIR%
python bld_content.py
python bld_index.py
python bld_hhp.py

setlocal
set PGM_DIR=%ProgramFiles(x86)%
if "%PGM_DIR%" == "" set PGM_DIR=%ProgramFiles%
"%PGM_DIR%\HTML Help Workshop\hhc" h2d2_chm.hhp
endlocal

del *.html
del *.hh*


if ~%MUC_AUTOMATIC_COMPILATION%~==~~ pause
