#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import H2D2Commands
import re
import sys
import html2text
import unicodedata

htmlWriter = html2text.HTML2Text()
htmlWriter.body_width = 0
htmlWriter.unicode_snob = 1
htmlWriter.single_line_break = True

HTML_SPACES = ['&nbsp;'] * 16

modeDebug = False
def printDebug(s):
    if (modeDebug):
        print(s)

def expandToken(tok, ltok):
    res = tok
    if res is not None:
        if len(res) < ltok: 
            res = res + ''.join( HTML_SPACES[:ltok-len(res)] )
    return res
        
class Writer:
    def __init__(self, fname):
        self.indt = 0
        self.fout = None    # In case there is an exception in file(...)
        self.fout = open(fname, 'w', encoding='utf-8')

    def __del__(self):
        if (self.fout):
            self.fout.close()
            self.fout = None

    def __writeHdr(self):
        printDebug('In writeHdr:')

    def __writeFtr(self):
        printDebug('In writeFtr:')

    def __writeMdl(self, mdl):
        printDebug('In writeMdl: %s' % mdl.name)
        title = "module %s" % mdl.name
        self.__prettyPrint(title)
        self.__prettyPrint('=' * len(title))
        self.__prettyPrint(' ')
        if (mdl.desc):
            self.indt += 1
            self.__prettyPrint(mdl.desc)
            self.__prettyPrint(' ')
            self.indt -= 1

        self.indt += 1
        for c in mdl.cmps:
            if (isinstance(c, H2D2Commands.Mdl)): self.__writeMdl(c)
            if (isinstance(c, H2D2Commands.Cls)): self.__writeCls(c)
            if (isinstance(c, H2D2Commands.Fnc)): self.__writeFnc(c)
        self.indt -= 1

    def __writeCls(self, cls):
        printDebug('In __writeCls:')
        title = "class %s" % cls.name
        self.__prettyPrint(title)
        self.__prettyPrint('=' * len(title))
        self.__prettyPrint(' ')
        if (cls.desc):
            self.indt += 1
            self.__prettyPrint(cls.desc)
            self.__prettyPrint(' ')
            self.indt -= 1

        self.indt += 1
        for c in cls.ctrs:  self.__writeFnc(c)
        for m in cls.meths: self.__writeFnc(m)
        for m in cls.sttcs: self.__writeFnc(m)
        self.indt -= 1

    def __writeFnc(self, f):
        if (f.type == 'property'):
            self.__writeOneProp(f)
        elif (f.type == 'getter'):
            self.__writeOneProp(f)
        elif (f.type == 'setter'):
            self.__writeOneProp(f)
        elif (f.type == 'parameter'):
            self.__writeOneProp(f)
        elif (f.type == 'operator'):
            self.__writeOneOper(f)
        elif (f.type == '##__unset__##'):
            self.__writeOneName(f)
        else:
            self.__writeOneFnc(f)
        self.__prettyPrint(' ')

    def __writeOneFnc(self, mth):
        printDebug('In writeOneFnc: %s' % mth.name)
        l = ''
        for a in mth.args:
            l += ', ' + a.name
        if (l): l = l[2:]

        line = "%s" % mth.type
        if (mth.rtrn): line += " %s" % mth.rtrn.type
        line += " %s" % mth.name
        parIndt = int(0.5 + len(line) / 3.0)
        line += "(%s)" % l
        self.__prettyPrint(line, parIndt = parIndt)
        if (mth.desc):
            self.indt += 1
            self.__prettyPrint(mth.desc)
            self.indt -= 1

        self.indt += 2
        if (mth.rtrn):
            a = mth.rtrn
            t, n = a.type, a.name
            t = expandToken(t, 9)
            n = expandToken(n,12)
            for d in a.desc:
                self.__prettyPrint("%s%s%s" %(t, n, d), parIndt = 6)
                t = expandToken('', 9)
                n = expandToken('',12)
        for a in mth.args:
            t, n = a.type, a.name
            t = expandToken(t, 9)
            n = expandToken(n,12)
            for d in a.desc:
                self.__prettyPrint("%s%s%s" %(t, n, d), parIndt = 6)
                t = expandToken('', 9)
                n = expandToken('',12-1)
                #t, n = '', ''
        self.indt -= 2

    def __writeOneOper(self, mth):
        printDebug('In writeOneOper: %s' % mth.name)
        l = ''
        for a in mth.args:
            l += ', ' + a.name
        if (l): l = l[2:]

        self.__prettyPrint("%s %s" % (mth.type, mth.name))
        if (mth.desc):
            self.indt += 1
            self.__prettyPrint(mth.desc)
            self.indt -= 1

        self.indt += 2
        for a in mth.args:
            t, n = a.type, a.name
            t = expandToken(t, 9)
            n = expandToken(n,12)
            for d in a.desc:
                self.__prettyPrint("%s%s%s" %(t, n, d), parIndt = 6)
                t = expandToken('', 9)
                n = expandToken('',12-1)
        self.indt -= 2

    def __writeOneProp(self, mth):
        printDebug('In writeOneProp: %s' % mth.name)
        self.__prettyPrint("%s %s" % (mth.type, mth.name))
        if (mth.desc):
            self.indt += 1
            self.__prettyPrint(mth.desc)
            self.indt -= 1

    def __writeOneName(self, mth):
        printDebug('In writeOneName: %s' % mth.name)
        self.__prettyPrint("%s" % (mth.name))
        if (mth.desc):
            self.indt += 1
            self.__prettyPrint(mth.desc)
            self.indt -= 1

    def __prettyPrint(self, s, maxlen=78, split=' ', parIndt = 0):
        if isinstance(s, str):
            return self.__prettyPrintOneBlock([s], maxlen, split, parIndt)
        else:
            return self.__prettyPrintOneBlock(s, maxlen, split, parIndt)

    def __prettyPrintOneBlock(self, block, maxlen=78, split=' ', parIndt = 0):
        txt = htmlWriter.handle(' '.join(block) )
        if isinstance(txt, str):
            txt = txt.split('\n')
        if len(txt) > 2 and not txt[-1].strip() and not txt[-2].strip() and not txt[-3].strip():
            txt.pop()
            txt.pop()
        if len(txt) > 0 and not txt[-1].strip():
            txt.pop()
        for l in txt:
            if not l: l = ' '
            self.__prettyPrintOneLine(l, maxlen, split, parIndt)

    def __prettyPrintOneLine(self, s, maxlen=78, split=' ', parIndt = 0):
        txt = unicodedata.normalize('NFKC', s + split)

        oldeol  = 0
        eol     = 0
        indt = self.indt
        while not (eol == -1 or eol == len(txt)-1):
            eol = txt.rfind(split, oldeol, oldeol+maxlen+len(split)-indt*3)
            line = txt[oldeol:eol]
            line = line.rjust(len(line) + indt*3)
            self.fout.write(line + '\n')
            oldeol = eol + len(split)
            if (indt == self.indt): indt += parIndt

    def write(self, cmds, **kwargs):
        printDebug('In write:')
        self.indt = 0
        for c in cmds:
            if (isinstance(c, H2D2Commands.Mdl)): self.__writeMdl(c)
            if (isinstance(c, H2D2Commands.Cls)): self.__writeCls(c)
            if (isinstance(c, H2D2Commands.Fnc)): self.__writeFnc(c)

