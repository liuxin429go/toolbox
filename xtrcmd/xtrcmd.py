#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

"""
Extrait les commandes de H2D2
"""

import os
import sys

import logging
LOGGER = logging.getLogger("INRS.IEHSS.Fortran.xtrcmd")

try:
    devDir = os.environ['INRS_DEV']
    if os.path.isdir(devDir):
         xtrDir = os.path.join(devDir, 'toolbox')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)
    else:
        raise RuntimeError('INRS_DEV is not a valid directory: %s', os.environ['INRS_DEV'])
except:
    raise RuntimeError('Environment variable INRS_DEV must be defined')


import H2D2Commands
import WriterHtml
import WriterChm
import WriterTree
import WriterTxt
#import WriterPython

import glob
import optparse
rpdb2_imported = False
try:
    import rpdb2
    rpdb2_imported = True
except:
    pass

__package__ = 'xtrcmd'
__version__ = '19.02'

formats = ['text', 'tree', 'html', 'chm', 'npp']

def xeq(cmds, args):
    for arg in args:
        arg = arg.strip()
        if (len(arg) > 0):
            a = os.path.normpath(arg)
            for f in glob.glob(a):
                LOGGER.info('Parsing: %s', f)
                s = H2D2Commands.File(f)
                c = s.parse()
                cmds += c

def main(opt_args = None):
    print('%s %s' % (__package__, __version__))

    def opt_kw(option, opt_str, value, parser):
        k,v = value.split('=')
        parser.values.kwd[k] = v

    # ---  Parse les options
    usage  = '%s [options]' % __package__
    parser = optparse.OptionParser(usage)
    parser.add_option("-I", "--include", dest="inc", default=[], action='append',
                  help="include PATH", metavar="INC")
    parser.add_option("-X", "--exclude", dest="exc", default=[], action='append',
                  help="exclude included FILE", metavar="EXC")
    parser.add_option("-f", "--format", dest="fmt", type='choice', choices=formats, default="html",
                  help="output FORMAT", metavar="FORMAT")
    parser.add_option("-d", "--debug", dest="dbg", default=False, action='store_true',
                  help="debug the run", metavar="DEBUG")
    parser.add_option("-o", "--output", dest="out", default=None,
                  help="output FILE", metavar="FILE")
    parser.add_option("-k", "--kword", dest="kwd", type='string', default={}, action="callback", callback=opt_kw,
                  help="keyword arguments passed to the writer")

    # ---  Inclus les fichiers réponse
    if (not opt_args): opt_args = sys.argv[1:]
    rmv = []
    rsp = []
    for a in opt_args:
        if (len(a) > 0 and a[0] == '@'):
            f = open(a[1:])
            l = f.readlines()
            f.close()
            rsp.extend( [ i.strip() for i in l] )
            rmv.append(a)
    for a in rmv: opt_args.remove(a)
    opt_args.extend(rsp)

    # --- Parse les arguments de la ligne de commande
    (options, args) = parser.parse_args(opt_args)

    H2D2Commands.File.addIncPath(options.inc)
    H2D2Commands.File.addExcFile(options.exc)

    #LOGGER.setLevel(logging.DEBUG)
    if (options.dbg):
        if (not rpdb2_imported):
            raise RuntimeError('rpdb2 must be installed to debug')
        else:
            rpdb2.start_embedded_debugger("inrs_iehss", fAllowUnencrypted = True)

    cmds = []
    xeq(cmds, args)
    cmds.sort(key=lambda x: x.name)

    w = None
    if len(cmds) > 0:
        if (options.fmt == 'text'): w = WriterTxt.Writer (options.out)
        if (options.fmt == 'tree'): w = WriterTree.Writer(options.out)
        if (options.fmt == 'html'): w = WriterHtml.Writer(options.out)
        if (options.fmt == 'chm'):  w = WriterChm.Writer(options.out)
        if (options.fmt == 'npp'):  w = WriterNpp.Writer(options.out)
        #if (options.fmt == 'python'): w = WriterPython.Writer(options.out)
        if w:
            try:
                w.write(cmds, **options.kwd)
                del(w)
            except Exception as e:
                del(w)
                print(str(e))
                raise

if __name__ == '__main__':
    #inc = [
    #      "-Ie:/dev/H2D2/h2d2_algo/source",
    #      "-Ie:/dev/H2D2/h2d2_post/source",
    #      ]
    #args = '@e:/dev/H2D2/h2d2.i' + r' -f text -o a.hlp E:\dev\H2D2\h2d2_umef\source\sphdro_ic.for'
    args = '@e:/bld-1804/H2D2/h2d2.i' + r' -f text -o a.txt E:/bld-1710/H2D2/cd2d_age/source/cd2d_age_ic.for'
    args.replace('\\', '/')
    args = args.split(' ')
    #args += ['--debug']

    streamHandler = logging.StreamHandler()
    LOGGER.addHandler(streamHandler)
    LOGGER.setLevel(logging.INFO)

    #main(args)

    main()
