#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import PyFtn

import datetime
import os
import sys

class Writer:
    def __init__(self, fname):
        self.mustRemoveFile = False
        self.fname = fname
        if (fname):
            self.fout = open(fname, 'w', encoding='utf-8')
        else:
            self.fout = sys.stdout

    def __del__(self):
        try:
            self.fout.close()
            if (self.mustRemoveFile and self.fname):
                os.remove(self.fname)
        except:
            pass

    def __writeHdr(self):
        self.__writeLine('//************************************************************************')
        self.__writeLine('// H2D2 - External declaration of public symbols')
        self.__writeLine('// Module: %s' % self.modul)
        self.__writeLine('// Entry point: extern "C" void fake_dll_%s()' % self.modul)
        self.__writeLine('//')
        self.__writeLine('// This file is generated automatically, any change will be lost.')
        self.__writeLine('// Generated %s' % datetime.datetime.now().isoformat(' '))
        self.__writeLine('//************************************************************************')
        self.__writeLine(' ')
        self.__writeLine('#include "cconfig.h"')
        self.__writeLine(' ')
        self.__writeLine(' ')
        self.__writeLine('#ifdef FAKE_DLL')
        self.__writeLine(' ')
        self.__writeLine(' ')
        self.__writeLine('#undef STRINGIF2')
        self.__writeLine('#undef STRINGIFY')
        self.__writeLine('#undef F_SMBL')
        self.__writeLine('#undef F_NAME')
        self.__writeLine('#undef F_PROT')
        self.__writeLine('#undef F_RGST')
        self.__writeLine('#undef M_SMBL')
        self.__writeLine('#undef M_NAME')
        self.__writeLine('#undef M_PROT')
        self.__writeLine('#undef M_RGST')
        self.__writeLine(' ')
        self.__writeLine('#define STRINGIF2(f) # f')
        self.__writeLine('#define STRINGIFY(f) STRINGIF2( f )')
        self.__writeLine(' ')
        self.__writeLine('#define F_SMBL(F, f) F2C_CONF_DECOR_FNC(F, f)')
        self.__writeLine('#define F_NAME(F, f) STRINGIFY( F_SMBL(F, f) )')
        self.__writeLine('#define F_PROT(F, f) void F_SMBL(F, f)()')
        self.__writeLine('#define F_RGST(F, f, l) fake_dll_lib_reg(&F_SMBL(F, f), F_NAME(F, f), l)')
        self.__writeLine(' ')
        self.__writeLine('#ifdef F2C_CONF_DECOR_MDL')
        self.__writeLine('#  define M_SMBL(M, m, F, f) F2C_CONF_DECOR_MDL(M, m, F, f)')
        self.__writeLine('#  define M_NAME(M, m, F, f) STRINGIFY( M_SMBL(M, m, F, f) )')
        self.__writeLine('#  define M_PROT(M, m, F, f) void M_SMBL(M, m, F, f)()')
        self.__writeLine('#  define M_RGST(M, m, F, f, l) fake_dll_lib_reg(&M_SMBL(M, m, F, f), M_NAME(M, m, F, f), l)')
        self.__writeLine('#else')
        self.__writeLine('#  define M_PROT(M, m, F, f)')
        self.__writeLine('#  define M_RGST(M, m, F, f, l)')
        self.__writeLine('#endif')
        self.__writeLine(' ')
        self.__writeLine('#ifdef __cplusplus')
        self.__writeLine('extern "C"')
        self.__writeLine('{')
        self.__writeLine('#endif')
        self.__writeLine(' ')

    def __writeHdrExtern(self):
        pass

    def __writeHdrDlopen(self, size = 0):
        self.__writeLine(' ')
        self.__writeLine('void fake_dll_lib_reg(void (*)(), const char*, const char*);')
        self.__writeLine(' ')
        self.__writeLine('void fake_dll_%s()' % self.modul)
        self.__writeLine('{')
        if (size != 0):
            self.__writeLine('   static char libname[] = "%s";' % self.modul)

    def __writeFtrDlopen(self):
        self.__writeLine('}')
        self.__writeLine(' ')

    def __writeFtrExtern(self):
        pass

    def __writeFtr(self):
        self.__writeLine('#ifdef __cplusplus')
        self.__writeLine('}')
        self.__writeLine('#endif')
        self.__writeLine(' ')
        self.__writeLine('#endif    // FAKE_DLL')
        self.__writeLine(' ')

    def __writeLine(self, s, maxlen=1024, split=' '):
        # Append the splitting character to guarantee a final match
        string = s + split

        lines   = []
        oldeol  = 0
        eol     = 0
        while not (eol == -1 or eol == len(string)-1):
            eol = string.rfind(split, oldeol, oldeol+maxlen+len(split))
            line = string[oldeol:eol]
            if isinstance(line, bytes): line = line.decode('latin-1')
            #if isinstance(line, str):   line = line.encode('cp850')
            self.fout.write( '%s\n' % (line) )
            oldeol = eol + len(split)

    def __writeFtnmodExtern(self, m):
        mMaj = m.getName().upper()
        mMin = m.getName().lower()
        for f in m.getFunctions():
            if (f.visibility == PyFtn.Visibility.Private and not self.xtr_private): continue
            if (f.visibility == PyFtn.Visibility.Public  and not self.xtr_public): continue

            fMaj = f.getName().upper()
            fMin = f.getName().lower()
            line = 'M_PROT(%s, %s, %s, %s);' % (mMaj, mMin, fMaj, fMin)
            self.__writeLine(line)

    def __writeMethodExtern(self, m):
        if (m.visibility == PyFtn.Visibility.Private and not self.xtr_private): return
        if (m.visibility == PyFtn.Visibility.Public  and not self.xtr_public): return

        fMaj = m.getName().upper()
        fMin = m.getName().lower()
        line = 'F_PROT(%s, %s);' % (fMaj, fMin)
        self.__writeLine(line)

    def __writeFtnmodDlopen(self, m):
        mMaj = m.getName().upper()
        mMin = m.getName().lower()
        for f in m.getFunctions():
            if (f.visibility == PyFtn.Visibility.Private and not self.xtr_private): continue
            if (f.visibility == PyFtn.Visibility.Public  and not self.xtr_public): continue
            
            fMaj = f.getName().upper()
            fMin = f.getName().lower()
            line = '   M_RGST(%s, %s, %s, %s, libname);' % (mMaj, mMin, fMaj, fMin)
            self.__writeLine(line)

    def __writeMethodDlopen(self, m):
        if (m.visibility == PyFtn.Visibility.Private and not self.xtr_private): return
        if (m.visibility == PyFtn.Visibility.Public  and not self.xtr_public): return

        fMaj = m.getName().upper()
        fMin = m.getName().lower()
        line = '   F_RGST(%s, %s, libname);' % (fMaj, fMin)
        self.__writeLine(line)

    def __writeClassExtern(self, cls):
        if (self.__size(cls) <= 0): return
        self.__writeLine(' ')
        self.__writeLine('// ---  class %s' % cls.name)
        for m in cls.ftnModules:
            self.__writeFtnmodExtern(m)
        for m in cls.methodes:
            self.__writeMethodExtern(m)

    def __writeClassDlopen(self, cls):
        if (self.__size(cls) <= 0): return
        self.__writeLine(' ')
        self.__writeLine('   // ---  class %s' % cls.name)
        for m in cls.ftnModules:
            self.__writeFtnmodDlopen(m)
        for m in cls.methodes:
            self.__writeMethodDlopen(m)

    def __writeModuleExtern(self, mdl):
        for c in sorted(mdl.values(), key=lambda x : x.name):
            self.__writeClassExtern(c)

    def __writeModuleDlopen(self, mdl):
        for c in sorted(mdl.values(), key=lambda x : x.name):
            self.__writeClassDlopen(c)

    def __size(self, obj):
        size = 0
        try:
            for mth in obj.methodes:
                if (mth.visibility == PyFtn.Visibility.Private and not self.xtr_private): continue
                if (mth.visibility == PyFtn.Visibility.Public  and not self.xtr_public): continue
                size += 1
            for mdl in obj.ftnModules:
                for mth in mdl.getFunctions():
                    if (mth.visibility == PyFtn.Visibility.Private and not self.xtr_private): continue
                    if (mth.visibility == PyFtn.Visibility.Public  and not self.xtr_public): continue
                    size += 1
        except AttributeError as e:
            for o in obj.values():
                size += self.__size(o)
        return size

    def __parseArgs(self, kwargs):
        self.modul = ''
        if 'modul' in kwargs: self.modul = kwargs['modul']

    def write(self, mdls, xtr_public = True, xtr_private = False, **kwargs):
        self.xtr_public  = xtr_public
        self.xtr_private = xtr_private
        self.__parseArgs(kwargs)

        #if (self.__size(mdls) == 0):
        #    self.mustRemoveFile = True
        #    return

        self.__writeHdr()

        self.__writeHdrExtern()
        for mdl in sorted(mdls.values(), key=lambda x: x.name):
            self.__writeModuleExtern(mdl)
        self.__writeFtrExtern()

        self.__writeHdrDlopen( self.__size(mdls) )
        for mdl in sorted(mdls.values(), key=lambda x: x.name):
            self.__writeModuleDlopen(mdl)
        self.__writeFtrDlopen()

        self.__writeFtr()

    def writeGlobal(self, modules):
        set = {}
        mdls = [ set.setdefault(e,e) for e in modules if e not in set ]

        self.__writeLine('//************************************************************************')
        self.__writeLine('// H2D2 - External declaration of public symbols')
        self.__writeLine('// List of modules - registration of faked Dll/SO')
        self.__writeLine('// Entry point: void fake_dlls()')
        self.__writeLine('//')
        self.__writeLine('// This file is generated automatically, any change will be lost.')
        self.__writeLine('// Generated %s' % datetime.datetime.now().isoformat(' '))
        self.__writeLine('//************************************************************************')
        self.__writeLine('#include "cconfig.h"')
        self.__writeLine(' ')
        self.__writeLine('#ifdef __cplusplus')
        self.__writeLine('extern "C"')
        self.__writeLine('{')
        self.__writeLine('#endif')
        self.__writeLine(' ')
        for m in mdls:
            self.__writeLine('void fake_dll_%s();' % m)
        self.__writeLine(' ')
        self.__writeLine('#ifdef FAKE_DLL')
        self.__writeLine('void fake_dlls()')
        self.__writeLine('{')
        for m in mdls:
            self.__writeLine('  fake_dll_%s();' % m)
        self.__writeLine('}')
        self.__writeLine('#endif    //  FAKE_DLL')
        self.__writeLine(' ')
        self.__writeLine('#ifdef __cplusplus')
        self.__writeLine('}')
        self.__writeLine('#endif')
        self.__writeLine(' ')
