#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import PyFtn

import html.entities as htmlentitydefs
import random
import re
import string
import sys

class HTMLWriter:
    ### Adapted from
    ###     http://effbot.org/librarybook/htmlentitydefs.htm
    ###
    def __init__(self):
        # this pattern matches substrings of reserved and non-ASCII characters
#        self.pattern = re.compile(r"[&<>\"\x80-\xff]+")
        self.pattern = re.compile(r"[&<>\"]+")

        # create character map
        self.entity_map = {}

        for i in range(256):
            self.entity_map[ chr(i) ] = "&#%d;" % i

        for entity, char in list(htmlentitydefs.entitydefs.items()):
            if (len(char) == 1):
                uchar = chr( ord(char) )
            else:
                uchar = str(char)
            if uchar in self.entity_map:
                self.entity_map[uchar] = "&%s;" % entity

    def escape_entity(self, m):
        return string.join( list(map(self.entity_map.get, m.group())), "")

    def escape(self, string):
        return self.pattern.sub(self.escape_entity, string)

    def escapeTags(self, string):
        s = string
        s = s.replace('<p>',  '<p/>')
        s = s.replace('<br>', '<br/>')
        return s

    def escapeLF(self, string):
        return string.replace('\n', self.entity_map['\n'])

class Writer:
    def __init__(self, fname):
        self.id = 0
        self.id_base = random.randint(0x0, 0xffffff) * 100
        self.indent = 0
        self.indents = '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
        self.classes = {}
        self.links = {}
        self.htmlWriter = HTMLWriter()
        if (fname):
            self.fout = open(fname, 'w', encoding='utf-8')
        else:
            self.fout = sys.stdout

    def __getNextId(self):
        self.id += 1
        return self.id + self.id_base

    def __writeHdr(self):
        self.__writeLine('<?xml version="1.0" encoding="UTF-8"?>')
        self.__writeLine('<Project name="%s">' % self.modul)
        self.indent = 1
        self.__writeLine('<Models>')
        self.indent += 1

    def __writeFtr(self):
        self.indent = 1
        self.__writeLine('</Models>')
        self.indent -= 1
        self.__writeLine('</Project>')

    def __writeLine(self, l):
        if (self.indent > 0):
            self.fout.write( '%s%s\n' % (self.indents[:self.indent], l) )
        else:
            self.fout.write('%s\n' % (l))

    def __writeHtmlTxt(self, ttl, txt = ''):
        t  = '<html>\n'
        t += '  <head>\n'
        t += '    \n'
        t += '  </head>\n'
        t += '  <body>\n'
        if (ttl != ''):
            t += '    <b>\n'
            t += '      %s\n' % ttl
            t += '    </b>\n'
#        t += '<p></p>'
        if (txt != ''):
            t += '    <p>\n'
            t += '      %s\n' % txt
            t += '    </p>\n'
        t += '  </body>\n'
        t += '</html>\n'
        html = t
        html = self.htmlWriter.escapeTags(html)
        html = self.htmlWriter.escape(html)
        html = self.htmlWriter.escapeLF(html)
        plain = ' '.join( (ttl, txt) )
        plain = plain.replace('\n', ' ')
        plain = self.htmlWriter.escape(plain)
        return plain, html


    def __writeRelRef(self, from_class, to_class):
        self.__writeLine('<RelationshipRef id="%08X" from="%08X" to="%08X"/>' %
            (self.links[(from_class,to_class)], self.classes[from_class], self.classes[to_class]))

    def __writeRelation(self, from_class, to_class):
        self.__writeLine('<Model id="%08X" composite="false" displayModelType="Generalization" modelType="Generalization" name="">' %(self.links[(from_class,to_class)]))
        self.indent += 1
        self.__writeLine('<ModelProperties>')
        self.indent += 1
        self.__writeLine('<StringProperty displayName="Model Type" name="modelType" value="Generalization"/>')
        self.__writeLine('<ModelRefProperty displayName="From" name="from">')
        self.indent += 1
        self.__writeLine('<ModelRef id="%08X"/>' % self.classes[from_class])
        self.indent -= 1
        self.__writeLine('</ModelRefProperty>')
        self.indent += 1
        self.__writeLine('<ModelRefProperty displayName="To" name="to">')
        self.__writeLine('<ModelRef id="%08X"/>' % self.classes[to_class])
        self.indent -= 1
        self.__writeLine('</ModelRefProperty>')
        self.indent -= 1
        self.__writeLine('</ModelProperties>')
        self.indent -= 1
        self.__writeLine('</Model>')

    def __writeVar(self, attr, kind = 'Attribute'):
        if (attr.visibility == PyFtn.Visibility.Private and not self.xtr_private): return
        if (attr.visibility == PyFtn.Visibility.Public  and not self.xtr_public): return

        self.__writeLine('<Model id="%08X" displayModelType="%s" modelType="%s" name="%s">' % (self.__getNextId(), kind, kind, attr.getName()))
        self.indent += 1
        self.__writeLine('<ModelProperties>')
        self.indent += 1
        self.__writeLine('<StringProperty displayName="Model Type" name="modelType" value="%s"/>' % kind)
        self.__writeLine('<StringProperty displayName="Name" name="name" value="%s"/>' % attr.getName())
        self.__writeLine('<StringProperty displayName="Visibility" name="visibility" value="%s"/>' % 'private')
        desc = attr.getDescription()
        self.__writeLine('<HTMLProperty displayName="Documentation" name="documentation" plainTextValue="%s"/>' % desc)
        self.__writeLine('<TextModelProperty displayName="Type" name="type">')
        self.indent += 1
        self.__writeLine('<StringValue value="%s"/>'% attr.getType())
        self.indent -= 1
        self.__writeLine('</TextModelProperty>')
        self.indent -= 1
        self.__writeLine('</ModelProperties>')
        self.indent -= 1
        self.__writeLine('</Model>')

    def __writeMethod(self, meth):
        if (meth.visibility == PyFtn.Visibility.Private and not self.xtr_private): return
        if (meth.visibility == PyFtn.Visibility.Public  and not self.xtr_public): return

        self.__writeLine('<Model id="%08X" displayModelType="Operation" modelType="Operation" name="%s">' % (self.__getNextId(), meth.getName()))
        self.indent += 1
        self.__writeLine('<ModelProperties>')
        self.indent += 1
        self.__writeLine('<StringProperty displayName="Model Type" name="modelType" value="Operation"/>')
        self.__writeLine('<StringProperty displayName="Name" name="name" value="%s"/>' % meth.getName())
        self.__writeLine('<StringProperty displayName="Visibility" name="visibility" value="%s"/>' % str(meth.getVisibility()).lower())
        if (meth.getHeader() is not None and meth.getHeader().description):
            plain, html = self.__writeHtmlTxt(meth.getDecl(), meth.getHeader().description)
            self.__writeLine('<HTMLProperty displayName="Documentation" name="documentation" plainTextValue="%s" value="%s"/>' % (plain, html))
        self.__writeLine('<TextModelProperty displayName="Return Type" name="returnType">')
        self.indent += 1
        self.__writeLine('<StringValue value="%s"/>'% meth.getType())
        self.indent -= 1
        self.__writeLine('</TextModelProperty>')

        self.indent -= 1
        self.__writeLine('</ModelProperties>')

        self.__writeLine('<ChildModels>')
        self.indent += 1
        for a in meth.args:
            self.__writeVar(a, kind = 'Parameter')
        self.indent -= 1
        self.__writeLine('</ChildModels>')

        self.indent -= 1
        self.__writeLine('</Model>')

    def __writeClass(self, cls):
        self.__writeLine('<Model id="%08X" displayModelType="Class" modelType="Class" name="%s">' % (self.classes[cls], cls.getName()))
        self.indent += 1
        self.__writeLine('<ModelProperties>')
        self.indent += 1
        self.__writeLine('<StringProperty displayName="Model Type" name="modelType" value="Class"/>')
        self.__writeLine('<StringProperty displayName="Name" name="name" value="%s"/>' % cls.getName())
        if cls.pure:
            self.__writeLine('<BooleanProperty displayName="Abstract" name="abstract" value="true"/>')
        if (cls.getHdr() is not None and cls.getHdr().description):
            plain, html = self.__writeHtmlTxt(cls.getName(), cls.getHdr().description)
            self.__writeLine('<HTMLProperty displayName="Documentation" name="documentation" plainTextValue="%s" value="%s"/>' % (plain, html))
        self.indent -= 1
        self.__writeLine('</ModelProperties>')

        self.__writeLine('<ChildModels>')
        self.indent += 1
        for a in cls.attributs: self.__writeVar(a, kind = 'Attribute')
        for m in cls.methodes:  self.__writeMethod(m)
        self.indent -= 1
        self.__writeLine('</ChildModels>')

        if (len(cls.enfants) > 0):
            self.__writeLine('<FromSimpleRelationships>')
            self.indent += 1
            for to_class in cls.enfants: self.__writeRelRef(cls, to_class)
            self.indent -= 1
            self.__writeLine('</FromSimpleRelationships>')
        if (len(cls.parents) > 0):
            self.__writeLine('<ToSimpleRelationships>')
            self.indent += 1
            for from_class in cls.parents: self.__writeRelRef(from_class, cls)
            self.indent -= 1
            self.__writeLine('</ToSimpleRelationships>')

        self.indent -= 1
        self.__writeLine('</Model>')

    def __writeModule(self, mdl):
        # ---  Remplis un dico avec les id des classes
        for c in mdl.values():
            self.classes[c] = self.__getNextId()

        # ---  Remplis un dico des liens
        for c in mdl.values():
            if (len(c.enfants) > 0):
                for to_class in c.enfants:
                    self.links[(c,to_class)] = self.__getNextId()

        # ---  Ecris les liens
        for from_class, to_class in self.links:
            self.__writeRelation(from_class, to_class)

        # ---  Ecris le module
        self.__writeLine('<Model id="%08X" composite="false" displayModelType="Package" modelType="Package" name="%s">' % (self.__getNextId(), mdl.getName()))
        self.indent += 1
        self.__writeLine('<ModelProperties>')
        self.indent += 1
        self.__writeLine('<StringProperty displayName="Model Type" name="modelType" value="Package"/>')
        self.__writeLine('<StringProperty displayName="Name" name="name" value="%s"/>' % mdl.getName())
        self.indent -= 1
        self.__writeLine('</ModelProperties>')

        self.__writeLine('<ChildModels>')
        self.indent += 1
        for c in mdl.values(): self.__writeClass(c)
        self.indent -= 1
        self.__writeLine('</ChildModels>')

        self.indent -= 1
        self.__writeLine('</Model>')

    def __parseArgs(self, kwargs):
        self.modul = ''
        if 'modul' in kwargs: self.modul = kwargs['modul']

    def write(self, mdls, xtr_public = True, xtr_private = False, **kwargs):
        self.__parseArgs(kwargs)
        self.xtr_public  = xtr_public
        self.xtr_private = xtr_private

        self.__writeHdr()
        for mdl in mdls.values():
            self.__writeModule(mdl)
        self.__writeFtr()

