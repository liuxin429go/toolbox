#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import hashlib
import os
import sys

import logging
logger = logging.getLogger("INRS.IEHSS.Fortran")

try:
    devDir = os.environ['INRS_DEV']
    if os.path.isdir(devDir):
         xtrDir = os.path.join(devDir, 'toolbox')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)

         xtrDir = os.path.join(devDir, 'toolbox/xtrapi')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)

         xtrDir = os.environ['INRS_BLD']
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)

         xtrDir = os.path.join(xtrDir, 'MUC_SCons')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)

         xtrDir = os.path.join(sys.prefix)
         if os.path.isdir(xtrDir):
            for f in os.listdir(xtrDir):
                if (f[:5] == 'scons'):
                    sys.path.append( os.path.join(xtrDir, f) )

         xtrDir = os.path.join(sys.prefix, 'lib')
         if os.path.isdir(xtrDir):
            for f in os.listdir(xtrDir):
                if (f[:5] == 'scons'):
                    sys.path.append( os.path.join(xtrDir, f) )

         xtrDir = os.path.join(sys.prefix, 'Lib', 'site-packages')
         if os.path.isdir(xtrDir):
            for f in os.listdir(xtrDir):
                if (f[:5] == 'scons'):
                    sys.path.append( os.path.join(xtrDir, f) )

    else:
        raise RuntimeError('INRS_DEV is not a valid directory: %s', os.environ['INRS_DEV'])
except:
    raise RuntimeError('Environment variable INRS_DEV must be defined')

import xtrapi
import WriterCmc

import MUC_SCons.Context
import SCons_Env

modules = []

def Import(v):
    SCons_Env.Import(v, globals())
Import('env')

def xeqRecursionOnSConscript(dir, mdl = None):
    if (os.path.isdir( os.path.join(dir, 'build') ) and
        os.path.isdir( os.path.join(dir, 'prjVisual') ) and
        os.path.isdir( os.path.join(dir, 'source') )):
        mdl = os.path.basename(dir)
        mdl = os.path.splitext(mdl)[0]

    for f in os.listdir(dir):
        fullPath = os.path.join(dir,f)
        fullPath = os.path.normpath(fullPath)
        if os.path.isdir(fullPath):
            if f == 'source':
                script = os.path.join(fullPath, '../SConscript')
                script = os.path.normpath(script)
                if os.path.isfile(script):
                    exec(compile(open(script).read(), script, 'exec'), globals())
                    if (env.ctx):
                        for s in env.ctx.src:
                            if (os.path.splitext(s)[1] == '.for'):
                                s = os.path.join(dir, s)
                                s = os.path.normpath(s)
                                xeqAction(s, mdl)
            elif f != 'test':
                xeqRecursionOnSConscript(fullPath, mdl)

def xeqRecursion(dir, mdl = None):
    if (os.path.isdir( os.path.join(dir, 'build') ) and
        os.path.isdir( os.path.join(dir, 'prjVisual') ) and
        os.path.isdir( os.path.join(dir, 'source') )):
        mdl = os.path.basename(dir)
        mdl = os.path.splitext(mdl)[0]

    for f in os.listdir(dir):
        fullPath = os.path.join(dir,f)
        fullPath = os.path.normpath(fullPath)
        if os.path.isdir(fullPath):
            if f != 'test':
                xeqRecursion(fullPath, mdl)
        else:
            if os.path.splitext(f)[1] == '.for':
                xeqAction(fullPath, mdl)

def xeqAction(src, mdl):
    inp = '.'.join( [os.path.splitext(src)[0], '*'] )
    out = src
    bck = '.'.join( [out, 'bak'] )          # Backup
    tmp = out + '.new'

    inc = os.path.join(devDir, 'H2D2/h2d2.i')

    args = '@%s -f pkl -k modul=%s -o %s %s' % (inc, mdl, tmp, inp)
    args = args.split()
    xtrapi.main(args)

    if (os.path.isfile(tmp)):
        if (os.path.isfile(out)):
            tmp_fic = open(tmp, 'rb')
            out_fic = open(out, 'rb')
            tmp_str = tmp_fic.read()
            out_str = out_fic.read()
            tmp_md5 = hashlib.md5(tmp_str)
            out_md5 = hashlib.md5(out_str)
            tmp_fic.close()
            out_fic.close()
            if (tmp_md5.digest() != out_md5.digest()):
                if (os.path.isfile(bck)): os.remove(bck)
                os.renames(out, bck)
                os.renames(tmp, out)
            else:
                os.remove(tmp)
        else:
            os.renames(tmp, out)
    if (os.path.isfile(out)):
        modules.append(mdl)

def main(argv = None):
    streamHandler = logging.StreamHandler()
    logger.addHandler(streamHandler)
    logger.setLevel(logging.INFO)

    if argv == None: argv = sys.argv[1:]
    if len(argv) == 0: argv.append('.')
    for a in argv:
        xeqRecursionOnSConscript(a)

main()
