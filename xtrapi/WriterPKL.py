#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import PyFtn

import enum
import os
import string
import sys

Action = enum.Enum('Action', ('Undefined', 'Pickle', 'Unpickle'))

def if_else(p, n1, n2):
    if (p): return n1
    return n2

class Writer:
    def __init__(self, fname):
        self.action = Action.Undefined
        self.fname = fname
        self.fout = None

    def __openFile(self):
        if (self.fout): return
        if (self.fname):
            self.fout = open(self.fname, 'w', encoding='utf-8')
        else:
            self.fout = sys.stdout

    def isPickle(self):
        return self.action == Action.Pickle

    def isUnpickle(self):
        return self.action == Action.Unpickle

    def __writeLine(self, l):
        self.fout.write('%s\n' % (l))

    def __writeHdr(self, cls):
        cname = cls.getName()
        action = if_else(self.action == Action.Pickle, 'PKL', 'UPK')
        mname = "%s_%s" % (cname, action)
        fname = os.path.basename(cls.getFileName())
        fname = os.path.splitext(fname)[0]
        self.__writeLine("""\
C************************************************************************
C Sommaire: (Un)-Pickle la classe
C
C Description:
C       La méthode <code>%s</code> sauve-restaure l'état de l'objet.
C
C Entrée:
C       HOBJ        Hande sur l'objet à sauver/restaurer
C       HXML        Handle sur le fichier d'écriture-lecture
C
C Sortie:
C
C Notes:
C************************************************************************""" % mname)
        if self.isUnpickle():
            self.__writeLine("      FUNCTION %s(HOBJ, LNEW, HXML)" % mname)
        else:
            self.__writeLine("      FUNCTION %s(HOBJ, HXML)" % mname)
        self.__writeLine("C$pragma aux %s export" % mname)
        self.__writeLine("CDEC$ATTRIBUTES DLLEXPORT :: %s" % mname)
        self.__writeLine("")
        self.__writeLine("      IMPLICIT NONE")
        self.__writeLine("")
        self.__writeLine("      INTEGER HOBJ")
        if (self.isUnpickle()): self.__writeLine("      LOGICAL LNEW")
        self.__writeLine("      INTEGER HXML")
        self.__writeLine("")
        self.__writeLine("      INCLUDE '%s.fi'" % fname)
        self.__writeLine("      INCLUDE 'err.fi'")
        self.__writeLine("      INCLUDE 'ioxml.fi'")
        if (self.isUnpickle()): self.__writeLine("      INCLUDE 'obobjc.fi'")
        self.__writeLine("      INCLUDE 'soallc.fi'")
        self.__writeLine("      INCLUDE '%s.fc'" % fname)
        self.__writeLine("")
        self.__writeLine("      INTEGER IERR")
        self.__writeLine("      INTEGER IOB")

    def __writePre(self, cls):
        cname = cls.getName()
        self.__writeLine("C------------------------------------------------------------------------")
        self.__writeLine("D     CALL ERR_PRE(%s_HVALIDE(HOBJ))" % cname)
        self.__writeLine("C------------------------------------------------------------------------")
        self.__writeLine("")

    def __writeFtr(self, cls):
        cname = cls.getName()
        action = if_else(self.action == Action.Pickle, 'PKL', 'UPK')
        mname = "%s_%s" % (cname, action)
        self.__writeLine("      %s = ERR_TYP()" % mname)
        self.__writeLine("      RETURN")
        self.__writeLine("      END")
        self.__writeLine("")

    def __writeDecl(self, cls):
        cname = cls.getName()
        l =len(cname)
        for a in cls.attributs:
            v = str(a)
            t = a.getType()
            self.__writeLine("      %s %s" % (t, v[l+1:]))

    def __writeInit(self, cls):
        if (self.action != Action.Unpickle): return
        self.__writeLine("C---     Initialise l'objet")
        self.__writeLine("      IF (LNEW) IERR = DT_COND_RAZ(HOBJ)")
        self.__writeLine("      IERR = DT_COND_RST(HOBJ)")
        self.__writeLine("")

    def __writeGetAttribs(self, cls):
        cname = cls.getName()
        l =len(cname)
        self.__writeLine("C---     Récupère les attributs")
        self.__writeLine("      IOB = HOBJ - %s_HBASE" % cname)
        for a in cls.attributs:
            v = str(a)
            self.__writeLine("      %s = %s(IOB)" % (v[l+1:], v))
        self.__writeLine("")

    def __writeSetAttribs(self, cls):
        if (self.action != Action.Unpickle): return
        cname = cls.getName()
        l =len(cname)
        self.__writeLine("C---     Assigne les attributs")
        self.__writeLine("      IF (ERR_GOOD()) THEN")
        for a in cls.attributs:
            v = str(a)
            self.__writeLine("         %s(IOB) = %s" % (v, v[l+1:]))
        self.__writeLine("      ENDIF")
        self.__writeLine("")

    def __writePKLAllc(self, cls, attr):
        k = if_else(self.action == Action.Pickle, 'W', 'R')
        v = attr[2][len(cls.getName())+1:]
        if   (attr[0] == "SO_ALLC_ALLINT"):
            self.__writeLine("      IF (ERR_GOOD()) IERR = IO_XML_%sI_V(HXML, %s)" % (k, v))
        elif (attr[0] == "SO_ALLC_ALLIN8"):
            self.__writeLine("      IF (ERR_GOOD()) IERR = IO_XML_%sX_V(HXML, %s)" % (k, v))
        elif (attr[0] == "SO_ALLC_ALLRE8"):
            self.__writeLine('      IF (ERR_GOOD()) IERR = IO_XML_%sD_V(HXML, %s)' % (k, v))
        else:
            print(attr.getType(), attr.getName())
            raise InvalideType

    def __writePKLHndl(self, cls, attr):
        k = if_else(self.action == Action.Pickle, 'W', 'R')
        v = attr[2][len(cls.getName())+1:]
        self.__writeLine("      IF (ERR_GOOD()) IERR = IO_XML_%sH_A(HXML, %s)" % (k, v))

    def __writePKLVar(self, cls, attr):
        k = if_else(self.action == Action.Pickle, 'W', 'R')
        v = str(attr)[len(cls.getName())+1:]
        if   (attr.getType()[0:7] == 'INTEGER'):
            if   (v[0] == 'H'):
                self.__writeLine("      IF (ERR_GOOD()) IERR = IO_XML_%sH_R(HXML, %s)" % (k, v))
            elif (v[0] == 'L'):
                self.__writeLine("      IF (ERR_GOOD()) IERR = IO_XML_%sI_V(HXML, %s)" % (k, v))
            else:
                self.__writeLine("      IF (ERR_GOOD()) IERR = IO_XML_%sI_1(HXML, %s)" % (k, v))
        elif (attr.getType() == 'LOGICAL'):
            self.__writeLine("      IF (ERR_GOOD()) IERR = IO_XML_%sL_1(HXML, %s)" % (k, v))
        elif (attr.getType() == 'REAL*8'):
            self.__writeLine("      IF (ERR_GOOD()) IERR = IO_XML_%sD_1(HXML, %s)" % (k, v))
        elif (attr.getType() == 'CHARACTER*'):
            self.__writeLine("      IF (ERR_GOOD()) IERR = IO_XML_%sS(HXML, %s(1:SP_STRN_LEN(%s)))" % (k, v, v))
        else:
            print(attr.getType(), attr.getName())
            raise InvalideType

    def __writePKLPickle(self, cls, fnc):
        pass2 = []
        for a in fnc.constructors: pass2.append(a[2])
        for a in fnc.allocations: pass2.append(a[2])

        self.__writeLine("C---     (Un)-Pickle")
        for a in cls.attributs:
            if (str(a) in pass2): continue
            self.__writePKLVar(cls, a)
        for a in fnc.constructors:
            self.__writePKLHndl(cls, a)
        for a in fnc.allocations:
            self.__writePKLAllc(cls, a)
        self.__writeLine("")

    def __writePKLClass(self, cls, fnc):
        self.__writeHdr(cls)
        self.__writeDecl(cls)
        self.__writePre(cls)
        self.__writeInit(cls)
        self.__writeGetAttribs(cls)
        self.__writePKLPickle(cls, fnc)
        self.__writeSetAttribs(cls)
        self.__writeFtr(cls)

    def __copyUptoFunction(self, fnc):
        fic = open(fnc.getFileName(), 'r', encoding='utf-8')
        for i in range(fnc.getLineFirst()-1):
            l = next(fic)
            self.__writeLine(l[:-1])
        fic.close()

    def __copyIncludingFunction(self, fnc):
        fic = open(fnc.getFileName(), 'r', encoding='utf-8')
        for i in range(fnc.getLineLast()-1):
            l = next(fic)
            self.__writeLine(l[:-1])
        fic.close()

    def __copyFromFunction(self, fnc):
        fic = open(fnc.getFileName(), 'r', encoding='utf-8')
        for i in range(fnc.getLineLast()-1):
            l = next(fic)
        for l in fic:
            self.__writeLine(l[:-1])
        fic.close()

    def __writeClass(self, cls):
        def filterMethod(meths, k):
            l = [f for f in meths if f.getKind() == k]
            return if_else(len(l) == 0, None, l[0])
        fPkl = filterMethod(cls.methodes, PyFtn.MethodKind.PKL)
        f999 = filterMethod(cls.methodes, PyFtn.MethodKind[2])
        fRst = filterMethod(cls.methodes, PyFtn.MethodKind.RST)
        if f999 and fRst:
            self.__openFile()
            if (fPkl):
                self.__copyUptoFunction(fPkl)
            else:
                self.__copyIncludingFunction(f999)
            self.action = Action.Pickle
            self.__writePKLClass(cls, fRst)
            self.action = Action.Unpickle
            self.__writePKLClass(cls, fRst)
            if (fPkl):
                self.__copyFromFunction(fPkl)
            else:
                self.__copyFromFunction(f999)

    def __writeModule(self, mdl):
        for c in mdl.values():
            self.__writeClass(c)

    def write(self, mdls, xtr_public = True, xtr_private = False, **kwargs):
        for mdl in mdls.values():
            self.__writeModule(mdl)
