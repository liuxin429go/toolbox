#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import ctypes
import os

def LoadLibrary(name):
    if os.name == "nt":
        return ctypes.CDLL('%s.dll' % name)
    elif os.name == "posix":
        return ctypes.CDLL('%s.so' % name)
    else:
        raise NotImplementedError ("Unsupported os: %s" % os.name)

if __name__ == '__main__':
    LoadLibrary('msvcrt')