#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import PyFtn

import sys

class Writer:
    def __init__(self, fname):
        self.indent = 0
        if (fname):
            self.fout = open(fname, 'w', encoding='utf-8')
        else:
            self.fout = sys.stdout

    def __writeHdr(self):
        pass

    def __writeFtr(self):
        pass

    def __writeLine(self, s, maxlen=75, split=' '):
        # Tack on the splitting character to guarantee a final match
        string = s + split

        lines   = []
        oldeol  = 0
        eol     = 0
        while not (eol == -1 or eol == len(string)-1):
            eol = string.rfind(split, oldeol, oldeol+maxlen+len(split))
            line = string[oldeol:eol]
            line = line.rjust(len(line) + self.indent*3)
            if isinstance(line, bytes): line = line.decode('latin-1')
            self.fout.write('%s\n' % line)
            oldeol = eol + len(split)

    def __writeOneProperty(self, mth):
        self.__writeLine("%s %s" % (mth.getType(), mth.getName()))
        if (mth.desc):
            self.indent += 1
            self.__writeLine("%s" % mth.desc)
            self.indent -= 1

    def __writeOneFnc(self, mth):
        l = mth.getArgs()
        if (l): l = l[2:]

        self.__writeLine(' ')
        line = ""
        if (mth.getType()): line += " %s" % mth.getType()
        line += " %s" % mth.getName()
        line += "(%s)" % l
        self.__writeLine(line)
        if (mth.hdr):
            self.indent += 1
            self.__writeLine("%s" % mth.hdr)
            self.indent -= 1

        self.indent += 2
        if (mth.getType()):
            self.__writeLine("%-10s%-12s" %(mth.getType(), mth.getName()))
        for a in mth.args:
            self.__writeLine("%-10s%-12s" %(a.getType(), a.getName()))
        self.indent -= 2

    def _writeOneOper(self, mth):
        l = ''
        for a in mth.args:
            l += ', ' + a.name
        if (l): l = l[2:]

        self._writeLine("%s %s" % (mth.getType(), mth.getName()))
        if (mth.desc):
            self.indent += 1
            self.__writeLine("%s" % mth.desc)
            self.indent -= 1

        self.indent += 2
        for a in mth.args:
            self.__writeLine("%-10s%-12s" % (a.getType(), a.getName()))
        self.indent -= 2

    def __writeAttr(self, attr):
        if (attr.visibility == PyFtn.Visibility.Private and not self.xtr_private): return
        if (attr.visibility == PyFtn.Visibility.Public  and not self.xtr_public): return

        self.__writeLine('name=%s' % attr.getName())
        self.__writeLine('visibility=%s' % 'private')
        self.__writeLine('type=%s'% attr.getType())

    def __writeMethod(self, m):
        if (m.visibility == PyFtn.Visibility.Private and not self.xtr_private): return
        if (m.visibility == PyFtn.Visibility.Public  and not self.xtr_public): return
        print(m.name, m.visibility)

        if (m.getType() == 'property'):
            self._writeOneProp(m)
        elif (m.getType() == 'parameter'):
            self._writeOneProp(m)
        elif (m.getType() == 'operator'):
            self.__writeOneOper(m)
        else:
            self.__writeOneFnc(m)

    def __writeClass(self, cls):
        self.indent = 0
        self.__writeLine(' ')
        title = "class %s" % cls.name
        self.__writeLine(title)
        self.__writeLine('=' * len(title))
        if (cls.getHdr()):
            self.indent += 1
            self._writeLine("%s\n" % cls.getHdr())
            self.indent -= 1

        self.indent += 1
        for a in cls.attributs: self.__writeAttr(a)
        for m in cls.methodes:  self.__writeMethod(m)
        self.indent -= 1

    def __writeModule(self, mdl):
        for c in mdl.values():
            self.__writeClass(c)

    def write(self, mdls, xtr_public = True, xtr_private = False):
        self.xtr_public  = xtr_public
        self.xtr_private = xtr_private

        self.__writeHdr()
        for mdl in mdls.values():
            self.__writeModule(mdl)
        self.__writeFtr()

