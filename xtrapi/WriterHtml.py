#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import PyFtn

#import Version
import sys

class Writer:
    def __init__(self, fname):
        pass

    def write(self, mdls, xtr_public = True, xtr_private = False):
        writer = WriterOneClass()
        for mdl in mdls.values():
            for cls in mdl.values():
                writer.write(cls, xtr_public, xtr_private)


class WriterOneClass:
    def __init__(self):
        self.os = None

    def __openFile(self, clss):
        self.__closeFile()
        nom = clss.getName() + '.html'
        self.os = open(nom, 'w', encoding='utf-8')

    def __closeFile(self):
        if (self.os is not None):
            self.os.close()
            self.os = None

    def __parseArgs(self, kwargs):
        self.modul = ''
        if 'modul' in kwargs: self.modul = kwargs['modul']

    def write(self, mdls, xtr_public = True, xtr_private = False, **kwargs):
        self.__parseArgs(kwargs)
        self.xtr_public  = xtr_public
        self.xtr_private = xtr_private

        self.__openFile(clss)
        self.__writeHdr()

##        print
##        print "<!-- API -->"
##        print "<div class=\"api\">"
##        print "<table>"
##        for l in api.lst:
##            if (isinstance(l, PyFtn.Function)):
##                print "       %s %s" % (l.getType(), l.getName())
##            elif (isinstance(l, PyFtn.Subroutine)):
##                print "C      SUBROUTINE %s" % l.getName()
##        print "</table>"
##        print "<!-- end api -->"

        self.__writeFtr()
        self.__closeFile()

    def __writeHdr(self):
        hdr = \
'''
<!-- ************************************************************************ -->
<!-- WebFtn v0.1.0 -->
<!-- Copyright (c) 2006, Institut National de la Recherche Scientifique (INRS) -->
<!-- TOUS DROITS RÉSERVÉS -->
<!-- ************************************************************************ -->

<!-- Ce fichier est généré automatiquement -->
<!-- Tout changement sera donc perdu       -->
<html>
<head>
<link rel="stylesheet" type="text/css" href="h2d2_cmd.css" />
</head>
<body>
'''
        self.os.write(hdr)

    def __writeFtr(self):
        ftr = \
'''
</body>
</html>
'''
        self.os.write(ftr)
