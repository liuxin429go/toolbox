#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import sys
from xml.dom import minidom

import logging
logger = logging.getLogger("INRS.IEHSS.Fortran")

def getModels(doc):
    tag_models = doc.getElementsByTagName('Models')
    all_models = tag_models[0].getElementsByTagName('Model')
    models = {}
    for m in all_models:
        typ = m.getAttribute("modelType")
        if (typ == "Class"):
            name = m.getAttribute("name")
            models[name] = m
    return models

def update(mdlPrj, mdlCls):
    clsChildModels = mdlCls.getElementsByTagName('ChildModels')
    prjChildModels = mdlPrj.getElementsByTagName('ChildModels')
    mdlPrj.replaceChild(clsChildModels[0], prjChildModels[0])

def compareOn(prjMethods, clsMethods, compareTag):
    compareFields = {
        'StringProperty'    : [ 'displayName', 'name', 'value'],
        'StringValue      ' : [ 'value'],
        'HTMLProperty'      : [ 'displayName', 'name', 'plainTextValue'],
    }
    print('   Comparing on %s' % compareTag)

    sameClass = False
    nbrSameMethod = 0
    for clsMethod in clsMethods:
        clsMethodName    = clsMethod.getAttribute('name')
        clsMethodType    = clsMethod.getAttribute('modelType')
        clsMethodProps   = clsMethod.getElementsByTagName('ModelProperties')[0]
        clsMethodStrings = clsMethodProps.getElementsByTagName(compareTag)
#        print '   Looking for: %s' % clsMethodName

        sameMethod = False
        for prjMethod in prjMethods:
            prjMethodName    = prjMethod.getAttribute('name')
            prjMethodType    = prjMethod.getAttribute('modelType')
            prjMethodProps   = prjMethod.getElementsByTagName('ModelProperties')[0]
            prjMethodStrings = prjMethodProps.getElementsByTagName(compareTag)

            if (clsMethodType != prjMethodType): continue
            if (clsMethodName != prjMethodName): continue
#            print '   Found: %s' % prjMethodName

            nbrSameString = 0
            for clsString in clsMethodStrings:
#                print '      Controling: %s %s' % (clsMethodName, clsString.getAttribute('name'))
                sameString = False
                for prjString in prjMethodStrings:
#                    print '      With %s' % prjString.getAttribute('name')
                    nHit = 0
                    for field in compareFields[compareTag]:
#                        print '      ', clsString.getAttribute(field), ' =? ', prjString.getAttribute(field)
                        if (clsString.getAttribute(field) == prjString.getAttribute(field)):
                            nHit += 1
                    sameString = (nHit == len(compareFields[compareTag]))
                    if (sameString): break

                if sameString:
                    nbrSameString += 1
#                    print '      Match found for: %s %s' % (clsMethodName, clsString.getAttribute('name'))
                else:
                    print('      No match found for: %s %s' % (clsMethodName, clsString.getAttribute('name')))

            if (nbrSameString == len(clsMethodStrings)):
                sameMethod = True

        if (sameMethod):
            nbrSameMethod += 1
#            print '      Match found for: %s' % clsMethodName
        else:
            print('      No match found for: %s' % clsMethodName)

    if (nbrSameMethod == len(clsMethods)):
        sameClass = True

    return sameClass

def areSame(mdlPrj, mdlCls):
    print('   Comparing')

    # ---  Get the methods, attributes, etc...
    clsChildModels = mdlCls.getElementsByTagName('ChildModels')
    prjChildModels = mdlPrj.getElementsByTagName('ChildModels')
    clsMethods = clsChildModels[0].getElementsByTagName('Model')
    prjMethods = prjChildModels[0].getElementsByTagName('Model')

    # ---  First order check
    if (len(clsMethods) != len(prjMethods)):
        print('      Classes differ by number of methods: %i / %i' % (len(clsMethods), len(prjMethods)))
        return False

    # ---  Filter out the childrens of childrens
    clsMethodsFiltered = [c for c in clsMethods if c.parentNode == clsChildModels[0]]
    prjMethodsFiltered = [c for c in prjMethods if c.parentNode == prjChildModels[0]]

    # ---  Deep compare
    sameClass = True
    sameClass = sameClass and compareOn(prjMethodsFiltered, clsMethodsFiltered, 'StringProperty')
#    sameClass = sameClass and compareOn(prjMethodsFiltered, clsMethodsFiltered, 'StringValue')
    sameClass = sameClass and compareOn(prjMethodsFiltered, clsMethodsFiltered, 'HTMLProperty')

    return sameClass

def updateProjectFromOneFile(mdlsPrj, mdlsCls):
    # ---  Look for changes and new classes
    modified = False
    for k, mdlCls in list(mdlsCls.items()):
        print('Parsing %s' % k)
        try:
            mdlPrj = mdlsPrj[k]
            same = areSame(mdlPrj, mdlCls)
            if (same):
                print('   Keeping   %s' % k)
            else:
                print('   Updating  %s' % k)
                update(mdlPrj, mdlCls)
                modified = True
        except KeyError:
            print('   Appending %s' % k)
            list(mdlsPrj.values())[0].parentNode.appendChild(mdlCls)
            modified = True

    # ---  Remove classes that are only in the project file
    for k,mdlPrj in list(mdlsPrj.items()):
        try:
            mdlCls = mdlsPrj[k]
        except:
#            mdlPrj.parentNode.removeChild(mdlPrj)
            print('Could be removed: %s' % k)
#            modified = True

    return modified

def updateProject(upd, prj):
    if not os.path.isfile(upd): raise ArgumentError
    prjExist = os.path.isfile(prj)

    docCls = minidom.parse(upd)
    mdlsCls = getModels(docCls)
    if prjExist:
        docPrj = minidom.parse(prj)
        mdlsPrj = getModels(docPrj)
        modified = updateProjectFromOneFile(mdlsPrj, mdlsCls)
    else:
        docPrj = docCls
        modified = True

    if (modified):
        bck = '.'.join( [prj, 'bak'] )          # Backup
        new,ext = os.path.splitext(prj)         # Split into base and extension
        ext = ext[1:]                           # Cut .
        new = '.'.join( [new, 'new', ext] )     # New
        fo = open(new, 'w', encoding='utf-8')
        docPrj.writexml(fo)
        fo.close()

        if os.path.isfile(bck): os.remove(bck)
        if os.path.isfile(prj): os.renames(prj, bck)
        os.renames(new, prj)

def main(argv = None):
    def usage():
        print('Usage:')
        print('   updateXML file_with_changes file_to_update')

    streamHandler = logging.StreamHandler()
    logger.addHandler(streamHandler)
    logger.setLevel(logging.INFO)

    if (argv == None): argv = sys.argv[1:]
    if (len(argv) != 2):
        usage()
        return

    updateProject(argv[0], argv[1])

main()
