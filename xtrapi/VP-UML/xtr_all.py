#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os
import sys

import logging
logger = logging.getLogger("INRS.IEHSS.Fortran")

try:
    devDir = os.environ['INRS_DEV']
    if os.path.isdir(devDir):
         xtrDir = os.path.join(devDir, 'toolbox')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)

         xtrDir = os.path.join(devDir, 'toolbox/xtrapi')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)

         xtrDir = os.environ['INRS_BLD']
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)

    else:
        raise RuntimeError('INRS_DEV is not a valid directory: %s', os.environ['INRS_DEV'])
except:
    raise RuntimeError('Environment variable INRS_DEV must be defined')

import xtrapi

def xeqRecursion(dir, mdl = None):
    asgMdl = False
    if (os.path.basename(dir) == 'H2D2'): asgMdl = True

    if os.path.isdir(dir):
        for f in os.listdir(dir):
            if (asgMdl): mdl = f

            fullPath = os.path.join(dir,f)
            fullPath = os.path.normpath(fullPath)
            if os.path.isdir(fullPath):
                xeqRecursion(fullPath, mdl)
            elif (os.path.splitext(fullPath)[1].lower() == '.for'):
                xeqAction(fullPath, mdl)
    else:
        xeqAction(dir, mdl)

def xeqAction(dir, mdl):
    inp = dir

    out_d = os.path.dirname(dir)            # Cut file name
    out_d = os.path.dirname(out_d)          # Cut last dir
    out_f = os.path.basename(dir)           # File name
    out_f = os.path.splitext(out_f)[0]      # Cut extension
    out_f = '.'.join( [out_f, 'xml'] )      # Add new extension
#    out = os.path.join(out_d, 'doc', out_f) # Full output filename
    out = os.path.join('.', 'tmp', out_f)   # Full output filename
    bck = '.'.join( [out, 'bak'] )          # Backup

    if (not os.path.isdir( os.path.dirname(out)) ):
        os.makedirs( os.path.dirname(out) )

    inc = os.path.join(devDir, 'H2D2/h2d2.i')
    tmp = out + '.new'
    args = '@%s -f xml -k modul=%s -o %s %s' % (inc, mdl, out, inp)
    args = args.split(' ')
    xtrapi.main(args)

def main(argv = None):
    streamHandler = logging.StreamHandler()
    logger.addHandler(streamHandler)
    logger.setLevel(logging.INFO)

    if (argv == None): argv = sys.argv[1:]
    if (len(argv) == 0): argv.append('.')
    for a in argv:
        xeqRecursion(a)

main()
