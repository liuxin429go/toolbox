#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from io import StringIO
import sys

import PyFtn

try:
    import win32clipboard as w
    HAS_WIN32_CLIPBOARD = True
except ImportError:
    w = None
    HAS_WIN32_CLIPBOARD = False

class Writer:
    def __init__(self, fname):
        if fname:
            self.fout = open(fname, 'w', encoding='utf-8')
        else:
            self.fout = StringIO()
        self.indent = 0
        self.xtr_public  = True
        self.xtr_private = True

    def __writeHdr(self):
        pass

    def __writeFtr(self):
        pass

    def __writeLine(self, s, maxlen=75, split=' '):
        # Tack on the splitting character to guarantee a final match
        glbStr = '!   %s%s%s' % ('   '*self.indent, s, split)

        oldeol = 0
        eol    = 0
        while not (eol == -1 or eol == len(glbStr)-1):
            eol = glbStr.rfind(split, oldeol, oldeol+maxlen+len(split))
            line = glbStr[oldeol:eol]
            if isinstance(line, bytes): line = line.decode('latin-1')
            self.fout.write('%s\n' % line)
            oldeol = eol + len(split)

    def __writeOneProp(self, mth):
        self.__writeLine("%s %s" % (mth.getType(), mth.getName()))

    def __writeOneFnc(self, mth):
        if mth.getType():
            line = "%s %s" % (mth.getType(), mth.getName())
        else:
            line = "%s %s" % ('SUBROUTINE', mth.getName())
        self.__writeLine(line)

    def __writeOneOper(self, mth):
        l = ''
        for a in mth.args:
            l += ', ' + a.name
        if l: l = l[2:]
        self.__writeLine("%s %s" % (mth.getType(), mth.getName()))

    def __writeMethod(self, m):
        if m.visibility == PyFtn.Visibility.Private and not self.xtr_private: return
        if m.visibility == PyFtn.Visibility.Public  and not self.xtr_public: return

        if m.getType() == 'property':
            self.__writeOneProp(m)
        elif m.getType() == 'parameter':
            self.__writeOneProp(m)
        elif m.getType() == 'operator':
            self.__writeOneOper(m)
        else:
            self.__writeOneFnc(m)

    def __writeClass(self, cls):
        self.__writeLine('H2D2 Class: %s' % cls.name)
        self.indent += 1
        for m in cls.methodes:
            self.__writeMethod(m)
        for m in cls.ftnModules:
            self.__writeLine('FTN (Sub)Module: %s' % m.name)
            self.indent += 1
            if self.xtr_public:
                self.__writeLine('Public:')
                old_val = self.xtr_private
                self.xtr_private = False
                self.indent += 1
                for f in m.funcs:
                    self.__writeMethod(f)
                self.indent -= 1
                self.xtr_private = old_val
            if self.xtr_private:
                self.__writeLine('Private:')
                old_val = self.xtr_public
                self.xtr_public = False
                self.indent += 1
                for f in m.funcs:
                    self.__writeMethod(f)
                self.indent -= 1
                self.xtr_public = old_val

            for i in m.ifaces:
                self.__writeLine('')
                self.__writeLine('FTN Interface: %s' % i.name)
                self.indent += 1
                if self.xtr_public:
                    self.__writeLine('Public:')
                    old_val = self.xtr_private
                    self.xtr_private = False
                    self.indent += 1
                    for f in i.funcs:
                        self.__writeMethod(f)
                    self.indent -= 1
                    self.xtr_private = old_val
                if self.xtr_private:
                    self.__writeLine('Private:')
                    old_val = self.xtr_public
                    self.xtr_public = False
                    self.indent += 1
                    for f in i.funcs:
                        self.__writeMethod(f)
                    self.indent -= 1
                    self.xtr_public = old_val
                self.indent -= 1

            for t in m.types:
                self.__writeLine('')
                self.__writeLine('FTN Type: %s' % t.name)
                self.indent += 1
                if self.xtr_public:
                    self.__writeLine('Public:')
                    old_val = self.xtr_private
                    self.xtr_private = False
                    self.indent += 1
                    for f in t.methods:
                        self.__writeMethod(f)
                    self.indent -= 1
                    self.xtr_private = old_val
                if self.xtr_private:
                    self.__writeLine('Private:')
                    old_val = self.xtr_public
                    self.xtr_public = False
                    self.indent += 1
                    for f in t.methods:
                        self.__writeMethod(f)
                    self.indent -= 1
                    self.xtr_public = old_val
                self.indent -= 1
            self.indent -= 1
        self.indent -= 1

    def __writeModule(self, mdl):
        self.__writeLine('H2D2 Module: %s' % mdl.name)
        self.indent += 1
        for c in mdl.values():
            self.__writeClass(c)
        self.indent -= 1

    def write(self, mdls, xtr_public=True, xtr_private=True):
        self.xtr_public  = xtr_public
        self.xtr_private = xtr_private

        self.__writeHdr()
        for mdl in mdls.values():
            self.__writeModule(mdl)
        self.__writeFtr()

        if isinstance(self.fout, StringIO):
            if HAS_WIN32_CLIPBOARD:
                w.OpenClipboard()
                w.EmptyClipboard()
                w.SetClipboardText(self.fout.getvalue().replace('\n', '\r\n') )
                w.CloseClipboard()
                sys.stdout.write('--- Content in clipboard ---\n')
                sys.stdout.write(self.fout.getvalue())
                sys.stdout.write('--- Content in clipboard ---\n')
            else:
                sys.stdout.write(self.fout.getvalue())
