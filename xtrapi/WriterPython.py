#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import PyFtn

import string
import sys

class Writer:
    indents = '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'

    def __init__(self, fname):
        self.indent = 0
        if (fname):
            self.fout = open(fname, 'w', encoding='utf-8')
        else:
            self.fout = sys.stdout

    def __writeLine(self, l):
        if (len(l) <= 0):
            self.fout.write( '\n' )
        elif (self.indent <= 0):
            self.fout.write( '%s\n' % (l) )
        else:
            self.fout.write( '%s%s\n' % (Writer.indents[:self.indent], l) )

    def __writeLineSplit(self, s, maxlen=75, split=' '):
        # Tack on the splitting character to guarantee a final match
        maxlen -= self.indent*4
        if (len(s) < maxlen):
            self.__writeLine(s)
        else:
            string = s + split
            oldeol  = 0
            eol     = 0
            while not (eol == -1 or eol == len(string)-1):
                eol = string.rfind(split, oldeol, oldeol+maxlen+len(split))
                line = string[oldeol:eol]
    #            line = line.rjust(len(line) + self.indent*3)
                self.__writeLine(line)
                oldeol = eol + len(split)

    def __writeHdr(self):
        hdr = \
'''\
# -*- coding: utf-8 -*-
###************************************************************************
### h2d2 xtrapi v0.1.0
### Copyright (c) INRS 2006-2017
### Institut National de la Recherche Scientifique (INRS)
###
### Distributed under the GNU Lesser General Public License, Version 3.0.
### See accompanying file LICENSE.txt.
###************************************************************************

### File generated automatically, changes will be lost on next update

import ctypes
import dllutil
'''
        self.__writeLine(hdr)

    def __writeFtr(self):
        pass

    def __trdArg(self, arg):
        n = arg.getName().lower()
        if (arg.getType() == 'INTEGER' and n == 'hobj'):
            n = 'self'
        return n

    def __trdMeth(self, meth):
        c = meth.getClass()
        n = meth.getName()
        if c and n[0:len(c)] == c:
            n = n[len(c):]
        if n and n[0] == '_':
            n = n[1:]

        n = n.lower()
        if n[0:3] in ['asg', 'req', 'xeq', 'est']:
            n = '%s_%s' % (n[:3], n[3:])

        l = n[0]
        for i in range(1, len(n)):
            if (n[i-1] == '_'):
                l += n[i].upper()
            else:
                l += n[i]
        n = l.replace('_', '')
        return n

    def __writeAttr(self, attr):
        if (attr.visibility == PyFtn.Visibility.Private and not self.xtr_private): return
        if (attr.visibility == PyFtn.Visibility.Public  and not self.xtr_public): return
        self.__writeLine("self.%s = 0" % attr.getName())

    def __writeMethodHeader(self, meth):
#        if (meth.hdr is not None and meth.hdr.description):
#            self.__writeLine(r"'''")
##            self.__writeLine('Proxy for the FORTRAN function:')
#            self.__writeLine('%s' % meth.getDecl())
#            self.__writeLine('')
#            self.__writeLineSplit('%s' % meth.hdr.description)
#            self.__writeLine(r"'''")
        pass

    def __writeMethodCTR(self, meth):
        methName = '__init__'

        args = []
        for a in meth.args:
            args.append(self.__trdArg(a))
        self.__writeLine('def %s(%s):' % (methName, ', '.join(args)))

        self.indent += 1
        self.__writeMethodHeader(meth)
        self.__writeLine('self.hobj = 0')
        self.__writeLine('self.clss = dllutil.LoadLibrary("%s")' % 'h2d2_krnl')
        if (len(args) > 0): args[0] = 'self.hobj'
        self.__writeLine('return self.clss.%s(%s)' % (meth.getName(), ', '.join(args)))
        self.indent -= 1
        self.__writeLine('')

    def __writeMethodDTR(self, meth):
        methName = '__del__'

        args = []
        for a in meth.args:
            args.append(self.__trdArg(a))
        self.__writeLine('def %s(%s):' % (methName, ', '.join(args)))

        self.indent += 1
        self.__writeMethodHeader(meth)
        self.__writeLine('assert(self.hobj != 0)')
        self.__writeLine('assert(self.clss)')
        if (len(args) > 0): args[0] = 'self.hobj'
        self.__writeLine('self.clss.%s(%s)' % (meth.getName(), ', '.join(args)))
        self.__writeLine('self.hobj = 0')
        self.indent -= 1
        self.__writeLine('')

    def __writeMethodXXX(self, meth):
        if (meth.pythonName is not None):
            methName = meth.pythonName
        else:
            methName = self.__trdMeth(meth)

        args = []
        for a in meth.args:
            args.append(self.__trdArg(a))
        self.__writeLine('def %s(%s):' % (methName, ', '.join(args)))

        self.indent += 1
        self.__writeMethodHeader(meth)
        self.__writeLine('assert(self.hobj != 0)')
        self.__writeLine('assert(self.clss)')
        if (len(args) > 0): args[0] = 'self.hobj'
        self.__writeLine('return self.clss.%s(%s)' % (meth.getName(), ', '.join(args)))
        self.indent -= 1
        self.__writeLine('')

    def __writeMethod(self, meth):
        if (meth.visibility == PyFtn.Visibility.Private and not self.xtr_private): return
        if (meth.visibility == PyFtn.Visibility.Public  and not self.xtr_public): return
        if (meth.getName()[-4:] == '_000'): return
        if (meth.getName()[-4:] == '_999'): return
        if (meth.getName()[-4:] == '_CTR'): return self.__writeMethodCTR(meth)
        if (meth.getName()[-4:] == '_DTR'): return self.__writeMethodDTR(meth)
        return self.__writeMethodXXX(meth)

    def __writeClass(self, cls):
        isRealClass = cls.getName()
        if isRealClass:
            self.__writeLine('class %s:' % cls.getName())
            self.indent += 1
            if (cls.getHdr()):
                self.indent += 1
                self.__writeLine('"""')
                self.__writeLine('%s' % cls.getHdr())
                self.__writeLine('"""')
                self.__writeLine('')
                self.indent -= 1

        if isRealClass: self.indent += 1
#        for a in cls.attributs: self.__writeAttr(a)
#        self.__writeLine('')
        for m in cls.methodes:  self.__writeMethod(m)
        if isRealClass: self.indent -= 1

    def __writeModule(self, mdl):
        mdlName = mdl.getName()
        self.__writeLine('###===============================')
        self.__writeLine('### Module: %s' % (mdlName if mdlName else ''))
        self.__writeLine('###===============================')
        self.__writeLine('')
        for c in mdl.values():
            self.__writeClass(c)

    def write(self, mdls, xtr_public = True, xtr_private = False):
        self.xtr_public  = xtr_public
        self.xtr_private = xtr_private

        self.__writeHdr()
        for mdl in mdls.values():
            self.__writeModule(mdl)
        self.__writeFtr()

