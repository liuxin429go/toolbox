#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os

class Environment:
    def __init__(self, tool, version, *args, **kargs):
        self.__reset()

    def __reset(self):
        self.ctx = None
        self.doc = None
        self.etc = None

    def SConscript(self, *args, **kargs):
        pass

    def MUC_Object(self, ctx):
        self.__reset()
        return ctx.prj

    def MUC_StaticLibrary(self, ctx):
        self.__reset()
        return ctx.prj

    def MUC_SharedLibrary(self, ctx):
        self.__reset()
        self.ctx = ctx
        return ctx.prj

    def MUC_Program(self, ctx):
        self.__reset()
        self.ctx = ctx
        return ctx.prj

    def MUC_Stage(self, ctx, f, subdir = ''):
        try:
            grp = os.path.dirname( f[0] )
            if (grp == 'doc'): self.doc = f
            if (grp == 'etc'): self.etc = f
        except:
            pass
        return ctx.prj

    def MUC_TarBz2(self, ctx, f = None):
        return None

    def MUC_Run(self, ctx, f, subdir = ''):
        return ctx.prj

    def __getitem__(self, i):
        if i == 'MUC_TOOL': return 'toolbox'
        if i == 'MUC_TOOL_MAIN': return 'toolbox-xtrapi'
        if i == 'MUC_VERSION_MAIN': return '1.0'
        if i == 'MUC_TARGETPLATEFORM': return 'win64'
        raise ValueError('In SCons_Env.Environment.__getitem__[%s]' % i)

static_env = Environment('fake_dll', '0.1a')

def Import(v, glbls):
    if (v == 'env'):
        glbls[v] = static_env
    elif (v == 'bld'):
        glbls[v] = 'debug'
    elif (v == 'mpi'):
        glbls[v] = 'mpich2'
    else:
        raise ValueError("Import of non-existent variable '%s'" % v)

