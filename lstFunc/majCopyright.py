#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import hashlib
import os
import sys

import logging
logger = logging.getLogger("INRS.IEHSS.Fortran")

try:
    home = os.environ['INRS_DEV']
    if os.path.isdir(home):
        xtrDir = os.path.join(home, 'toolbox/xtrapi')
        if os.path.isdir(xtrDir):
            sys.path.append(xtrDir)
        else:
            raise RuntimeError('Not a valid directory: %s', xtrDir)
    else:
        raise RuntimeError('INRS_DEV is not a valid directory: %s', os.environ['INRS_DEV'])
except:
    raise RuntimeError('Environment variable INRS_DEV must be defined')


def xeqRecursion__(dir, mdl = None):
    asgMdl = False
    if os.path.basename(dir) == 'H2D2': asgMdl = True

def xeqRecursion(dir):
    if os.path.isdir(dir):
        for f in os.listdir(dir):
            fullPath = os.path.join(dir,f)
            fullPath = os.path.normpath(fullPath)
            xeqRecursion(fullPath)
    else:
        fullPath = dir
        if os.path.splitext(fullPath)[1].lower() in ['.for', '.fi', '.fc']:
            xeqAction(fullPath, 'C')
        elif os.path.splitext(fullPath)[1].lower() in ['.cpp', '.h', '.hf', '.hpp']:
            xeqAction(fullPath, '//')
        elif os.path.splitext(fullPath)[1].lower() in ['.py']:
            xeqAction(fullPath, '#')

def xeqAction(dir, cmt):
    inp = dir
    tmp = '.'.join( [inp, 'new'] )          # Add new extension
    bck = '.'.join( [inp, 'bak'] )          # Backup

    logger.info(' Reading  %s' % inp)
    xeqOneFile(inp, tmp, cmt)

    if os.path.isfile(tmp):
        inp_fic = open(inp, 'rb')
        tmp_fic = open(tmp, 'rb')
        inp_md5 = hashlib.md5( inp_fic.read() )
        tmp_md5 = hashlib.md5( tmp_fic.read() )
        inp_fic.close()
        tmp_fic.close()
        if (tmp_md5.digest() != inp_md5.digest()):
            if (os.path.isfile(bck)): os.remove(bck)
            os.renames(inp, bck)
            os.renames(tmp, inp)
            logger.info(' --> Updating %s' % inp)
        else:
            os.remove(tmp)


#import win32clipboard as w
import xtrapi

def writeCprt(fout, y0 = 2011, y1 = 2017):
    txt = ["C --- Institut National de la Recherche Scientifique (INRS)",
           "C ---",
           "C --- Distributed under the GNU Lesser General Public License, Version 3.0.",
           "C --- See accompanying file LICENSE.txt."]

    if (y0 == y1):
        fout.write(u'C --- Copyright (c) INRS %i\n' % y0)
    else:
        fout.write(u'C --- Copyright (c) INRS %i-%i\n' % (y0, y1))
    for l in txt: fout.write("%s\n" % l)

def xeqOneFile(inp, out, cmt):
    finp = open(inp, mode = 'r', encoding= 'utf-8')
    fout = open(out, mode = 'w', encoding= 'utf-8')

    lCmt = len(cmt)
    inBloc = False
    inCprt = False
    doWrite = True
    doSkip  = False
    for l in finp.readlines():
        if (l[0:lCmt] != cmt):
            fout.write(l)
            continue

        if (l[0:25] == 'C************************'):
            if (inBloc and inCprt):
                writeCprt(fout, y0 = y0, y1 = y1)
                inCprt = False
            inBloc = not inBloc
            doSkip = False
            doWrite= True
        if (doSkip): continue

        if (l[0:19] == 'C --- Copyright (c)'):
            l = l[19:].strip()
            try:
                l = l.split(' ')[-1]
            except ValueError:
                pass
            try:
                y0, y1 = l.split('-')
            except ValueError:
                y0 = l
            y0 = int(y0)
            y1 = 2017
            assert(inBloc)
            inCprt = True
            doWrite = False
            doSkip  = True

        if (doWrite): fout.write(l)


def main(argv = None):
    streamHandler = logging.StreamHandler()
    logger.addHandler(streamHandler)
    logger.setLevel(logging.INFO)

    if (argv == None): argv = sys.argv[1:]
    if (len(argv) == 0): argv.append('.')
    for a in argv:
        xeqRecursion(a)

if __name__ == '__main__':
#    args = [ os.path.join(home, 'H2D2\h2d2_svc\source\sofunc.for') ]

#    main(args)
    main()
