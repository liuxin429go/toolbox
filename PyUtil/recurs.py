#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#************************************************************************
# --- Copyright (c) 2007-2018 INRS
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import hashlib
import os
import sys

import logging
logger = logging.getLogger("INRS.IEHSS.Fortran")

try:
    devDir = os.environ['INRS_DEV']
    if os.path.isdir(devDir):
         xtrDir = os.path.join(devDir, 'toolbox')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)

         xtrDir = os.path.join(devDir, 'toolbox/xtrapi')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)

         xtrDir = os.environ['INRS_BLD']
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)

         xtrDir = os.path.join(xtrDir, 'MUC_SCons')
         if os.path.isdir(xtrDir):
             sys.path.append(xtrDir)
         else:
             raise RuntimeError('Not a valid directory: %s', xtrDir)

         xtrDir = os.path.join(sys.prefix)
         if os.path.isdir(xtrDir):
            for f in os.listdir(xtrDir):
                if (f[:5] == 'scons'):
                    sys.path.append( os.path.join(xtrDir, f) )

         xtrDir = os.path.join(sys.prefix, 'lib')
         if os.path.isdir(xtrDir):
            for f in os.listdir(xtrDir):
                if (f[:5] == 'scons'):
                    sys.path.append( os.path.join(xtrDir, f) )

         xtrDir = os.path.join(sys.prefix, 'Lib', 'site-packages')
         if os.path.isdir(xtrDir):
            for f in os.listdir(xtrDir):
                if (f[:5] == 'scons'):
                    sys.path.append( os.path.join(xtrDir, f) )

    else:
        raise RuntimeError('INRS_DEV is not a valid directory: %s', os.environ['INRS_DEV'])
except:
    raise RuntimeError('Environment variable INRS_DEV must be defined')

import MUC_SCons.Context
import SCons_Env

def Import(v):
    SCons_Env.Import(v, globals())
Import('env')

class H2D2Recursion:
    def __init__(self):
        self.skipDir = [ 'test', 'proto']

    def xeqAction(self, fullPath, src, mdl):
        raise UnimplementedError

    def xeqRecursionOnSConscript(self, dir, mdl = None):
        if (os.path.isdir( os.path.join(dir, 'build') ) and
            os.path.isdir( os.path.join(dir, 'prjVisual') ) and
            os.path.isdir( os.path.join(dir, 'source') )):
            mdl = os.path.basename(dir)
            mdl = os.path.splitext(mdl)[0]

        for f in os.listdir(dir):
            fullPath = os.path.join(dir,f)
            fullPath = os.path.normpath(fullPath)
            if os.path.isdir(fullPath):
                if (f == 'source'):
                    script = os.path.join(fullPath, '../SConscript')
                    script = os.path.normpath(script)
                    if os.path.isfile(script):
                        MUC_SCons.Context.env = env
                        exec(compile(open(script).read(), script, 'exec'), globals())
                        if (env.ctx):
                            src = []
                            for s in env.ctx.src:
                                if (os.path.splitext(s)[1] == '.for'):
                                    s = os.path.join(dir, s)
                                    s = os.path.normpath(s)
                                    src.append(s)
                            self.xeqAction(fullPath, src, mdl)
                elif (f not in self.skipDir):
                    self.xeqRecursionOnSConscript(fullPath, mdl)

    def xeqRecursion(self, dir, mdl = None):
        if (os.path.isdir( os.path.join(dir, 'build') ) and
            os.path.isdir( os.path.join(dir, 'prjVisual') ) and
            os.path.isdir( os.path.join(dir, 'source') )):
            mdl = os.path.basename(dir)
            mdl = os.path.splitext(mdl)[0]

        for f in os.listdir(dir):
            fullPath = os.path.join(dir,f)
            fullPath = os.path.normpath(fullPath)
            if os.path.isdir(fullPath):
                if (f == 'source'):
                    self.xeqAction(fullPath, '*.for', mdl)
                elif (f != 'test'):
                    self.xeqRecursion(fullPath, mdl)

    def areSame(self, old, new, ptrn = None):
        new_fic = open(new, 'rb')
        old_fic = open(old, 'rb')
        new_str = new_fic.read()
        old_str = old_fic.read()
        new_md5 = hashlib.md5( new_str[new_str.index(ptrn):] )
        old_md5 = hashlib.md5( old_str[old_str.index(ptrn):] )
        new_fic.close()
        old_fic.close()
        return new_md5.digest() == old_md5.digest()
