pushd STEditor
call STUpdater.bat
popd

pushd xtrapi
call update_all.bat
popd

pushd xtrcmd
call update_all.bat
popd

pushd xtrlog
call xtr_all.bat
popd

pushd xtrtmr
call xtr_all.bat
popd
