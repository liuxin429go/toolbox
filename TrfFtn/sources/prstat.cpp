//************************************************************************
// --- Copyright (c) 2000 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS R╔SERV╔S
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette 
// --- permission apparaissent dans toutes les copies ainsi que dans la 
// --- documentation. 
// --- L'INRS ne prétend en aucune faþon que ce logiciel convient Ó un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//*****************************************************************************
// Fichier:  prstat.cpp
// Classe:   PRStatement
//*****************************************************************************
// 24-08-93  Yves Secretan  Version initiale
// 25-09-97  Yves Roy       Préparation d'une version simple pour étudiants
// 07-02-98  Yves Secretan  Corrige readToString()
// 07-02-98  Yves Secretan  Corrige isMethod() et readStatement()
// 20-03-98  Yves Secretan  Corrige readStatement() (parse des :)
// 30-03-98  Yves Secretan  Recorrige readStatement() (parse des :)
// 20-08-99  Yves Secretan  Version Web++ 2.0b
//*****************************************************************************
#include "prstat.h"
#include "prifstr.h"

#include <stdlib.h>
#include <fstream>

ConstCarP PRStatement::tokenEnteteDebut = "***********************";
ConstCarP PRStatement::tokenEnteteFin   = "***********************";

//*****************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//    Leak !!!
//*****************************************************************************
void PRStatement::asgTokenEnteteDebut(const PRStatement& t)
{
   tokenEnteteDebut = t.dupAsCarP();
}
void PRStatement::asgTokenEnteteFin(const PRStatement& t)
{
   tokenEnteteFin = t.dupAsCarP();
}

//*****************************************************************************
// Sommaire:   Compare les cha¯nes pour l'égalité aux espaces près
//
// Description:
//    La méthode statique privée <code>contiensUniquement(...)</code> retourne VRAI
//    si <code>s1P</code> ne contient que <code>s2P</code> Ó des espaces près.
//
// Entrée:
//    const char* s1P;     Les deux cha¯nes Ó comparer
//    const char* s2P;
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
Booleen PRStatement::contiensUniquement(const char* s1P, const char* s2P)
{
#ifdef MODE_DEBUG
   PRECONDITION(s1P != NUL);
   PRECONDITION(s2P != NUL);
#endif   // ifdef MODE_DEBUG

   const size_t l1 = strlen(s1P);
   const size_t l2 = strlen(s2P);
   if (l1 < l2)
   {
      return FAUX;
   }
   else if (l1 == l2)
   {
      return strcmp(s1P, s2P) == 0;
   }
   else if (*s1P == ' ')
   {
      return strcmp(s1P+1, s2P) == 0;
   }
   else if (s1P[l2] == ' ')
   {
      return strncmp(s1P, s2P, l2) == 0;
   }
   else
   {
      return FAUX;
   }
}

//*****************************************************************************
// Sommaire:   Retourne VRAI si l'objet est une classe.
//
// Description:
//    La méthode publique <code>estClass()</code> retoure VRAI si l'objet est la
//    déclaration d'une <code>classe</code>.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
Booleen PRStatement::estClass () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (*this == "class") return VRAI;
   if (::left(*this, 7) == "friend ") return FAUX;

   if (::left(*this, 13) == "typedef class")
   {
      PRStatement tmp = ::ltrim(::mid(*this, 13));
      int nblk = tmp.len() - ::delChar(tmp, ' ').len();
      return (nblk > 0) ? FAUX : VRAI;
   }

   PRStatement compact = ::delChar(*this, ' ');
   if (::right(compact, 1) == ";") compact = ::left(compact, compact.len()-1);
   if (::right(compact, 1) == ")") return FAUX;
   if (instr("class ") < 0) return FAUX;

   if (::right(compact, 6) == ")const") return FAUX;
   if (::right(compact, 2) == "=0" )    return FAUX;
   if (::left(compact, 9) == "template<" && compact.instr(">class" ) < 0) return FAUX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return VRAI;
}

//*****************************************************************************
// Sommaire:   Retourne VRAI si l'objet est une struct.
//
// Description:
//    La méthode publique <code>estStruct()</code> retoure VRAI si l'objet est la
//    déclaration d'une <code>struct</code>.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
Booleen PRStatement::estStruct () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (*this == "struct") return VRAI;
   if (::left(*this, 7) == "friend ") return FAUX;

   if (::left(*this, 14) == "typedef struct")
   {
      PRStatement tmp = ::ltrim(::mid(*this, 14));
      int nblk = tmp.len() - ::delChar(tmp, ' ').len();
      return (nblk > 0) ? FAUX : VRAI;
   }

   PRStatement compact = ::delChar(*this, ' ');
   if (::right(compact, 1) == ";") compact = ::left(compact, compact.len()-1);
   if (::right(compact, 1) == ")") return FAUX;
   if (instr("struct ") < 0) return FAUX;

   if (::right(compact, 6) == ")const") return FAUX;
   if (::right(compact, 2) == "=0" )    return FAUX;
   if (::left(compact, 9) == "template<" && compact.instr(">struct" ) < 0) return FAUX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return VRAI;
}

//*****************************************************************************
// Sommaire:   Retourne VRAI si l'objet est une enum.
//
// Description:
//    La méthode publique <code>estEnum()</code> retoure VRAI si l'objet est la
//    déclaration d'une <code>enum</code>.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
Booleen PRStatement::estEnum () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (*this == "enum") return VRAI;
   if (::left(*this, 5) == "enum ") return VRAI;
   if (::left(*this, 12) == "typedef enum")
   {
      PRStatement tmp = ::ltrim(::mid(*this, 12));
      int nblk = tmp.len() - ::delChar(tmp, ' ').len();
      return (nblk > 0) ? FAUX : VRAI;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return FAUX;
}

//*****************************************************************************
// Sommaire:   Retourne VRAI si l'objet est une union.
//
// Description:
//    La méthode publique <code>estUnion()</code> retoure VRAI si l'objet est la
//    déclaration d'une <code>union</code>.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
Booleen PRStatement::estUnion () const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (*this == "union") return VRAI;
   if (::left(*this, 7) == "friend ") return FAUX;

   if (::left(*this, 13) == "typedef union")
   {
      PRStatement tmp = ::ltrim(::mid(*this, 13));
      int nblk = tmp.len() - ::delChar(tmp, ' ').len();
      return (nblk > 0) ? FAUX : VRAI;
   }

   PRStatement compact = ::delChar(*this, ' ');
   if (::right(compact, 1) == ";") compact = ::left(compact, compact.len()-1);
   if (::right(compact, 1) == ")") return FAUX;
   if (instr("union ") < 0) return FAUX;

   if (::right(compact, 6) == ")const") return FAUX;
   if (::right(compact, 2) == "=0" )    return FAUX;
   if (::left(compact, 9) == "template<" && compact.instr(">union") < 0) return FAUX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return VRAI;
}

//*****************************************************************************
// Sommaire:   Retourne VRAI si l'objet est une méthode.
//
// Description:
//    La méthode publique <code>estMethode()</code> retoure VRAI si l'objet est la
//    déclaration d'une methode.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
Booleen PRStatement::estMethode() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   PRStatement compact = ::delChar(*this, ' ');
   if (::right(compact,1) == ";")
      compact.left(compact.len()-1);
   if (compact.rinstr("):") > 0)
      compact.left(compact.rinstr("):")+1);
   if (compact.rinstr(")throw") > 0)
      compact.left(compact.rinstr(")throw")+1);
   if (::right(compact,5) == "const")
      compact.left(compact.len()-5);

   if (instr("(" ) < 0) return FAUX;
   if (::left(compact,3) == "if(") return FAUX;
   if (::left(compact,4) == "for(") return FAUX;
   if (::left(compact,6) == "while(") return FAUX;
   if (::left(compact,7) == "switch(") return FAUX;
   if (::right(compact, 2) == "=0" ) return VRAI;
   if (::right(compact, 1) != ")"  ) return FAUX;
   if (::right(compact, 2) == "))" ) return VRAI;

   PRStatement args = ::rbloc(compact, "(", ")");
   compact.left(compact.len() - args.len() - 2);

   if (::right(compact, 10) == "operator()"  ) return VRAI;
   if (::right(compact, 1) == ")"  ) return FAUX;
   if (compact.instr("(" ) >= 0) return FAUX;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return VRAI;
}

//*****************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
void PRStatement::getTypeName (PRStatement& type, PRStatement& name) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Type and name
   if (instr("(") < 0)
   {
      PRStatement stat = *this;
      while (stat.instr("< ") >= 0) stat.subst("< ", "<");
      while (stat.instr(" <") >= 0) stat.subst(" <", "<");
//   while (stat.instr("> ") >= 0) stat.subst("> ", ">");
      while (stat.instr(" >") >= 0) stat.subst(" >", ">");
      while (stat.instr(", ") >= 0) stat.subst(", ", ",");
      while (stat.instr(" ,") >= 0) stat.subst(" ,", ",");
      while (stat.instr(" ::") >= 0) stat.subst(" ::", "::");
      while (stat.instr(":: ") >= 0) stat.subst(":: ", "::");
      while (stat.instr(" *") >= 0) stat.subst(" *", "* ");
      while (stat.instr(" &") >= 0) stat.subst(" &", "& ");
      while (stat.instr("  ") >= 0) stat.subst("  ", " ");
      Entier pos = 0;
      PRStatement tok;
      if		  (::right(stat, 6) == "*const")  				// type *const
         pos = stat.len();
      else if ((tok = ::rtoken(stat, "*const ")) != "")  // type *const name
         pos = stat.len() - tok.len();
      else if (::right(stat, 7) == "* const") 				// type * const
         pos = stat.len();
      else if ((tok = ::rtoken(stat, "* const ")) != "") // type * const name
         pos = stat.len() - tok.len();
      else if (::right(stat, 1) == "*") 						// type *
         pos = stat.len();
      else if ((tok = ::rtoken(stat, "*")) != "" &&      // type * name
               tok.len() < ::rtoken(stat, "&").len())
         pos = stat.len() - tok.len();
      else if (::right(stat, 6) == "&const") 				// type &const
         pos = stat.len();
      else if ((tok = ::rtoken(stat, "&const ")) != "")  // type &const name
         pos = stat.len() - tok.len();
      else if (::right(stat, 7) == "& const") 				// type & const
         pos = stat.len();
      else if ((tok = ::rtoken(stat, "& const ")) != "") // type & const name
         pos = stat.len() - tok.len();
      else if (::right(stat, 1) == "&") 						// type &
         pos = stat.len();
      else if ((tok = ::rtoken(stat, "&")) != "")        // type & name
         pos = stat.len() - tok.len();
      else if (::left(stat, 6) == "const ")              // const type name
      {
         PRStatement tmp = ::mid(stat, 6);
         pos = 6;
         if (::left(tmp, 11) == "signed char")
            pos += 11;
         else if (::left(tmp, 13) == "unsigned char")
            pos += 13;
         else if (::left(tmp, 16) == "signed short int")
            pos += 16;
         else if (::left(tmp, 18) == "unsigned short int")
            pos += 18;
         else if (::left(tmp, 12) == "signed short")
            pos += 12;
         else if (::left(tmp, 14) == "unsigned short")
            pos += 14;
         else if (::left(tmp,  9) == "short int")
            pos += 9;
         else if (::left(tmp, 10) == "signed int")
            pos += 10;
         else if (::left(tmp, 12) == "unsigned int")
            pos += 12;
         else if (::left(tmp, 15) == "signed long int")
            pos += 15;
         else if (::left(tmp, 17) == "unsigned long int")
            pos += 17;
         else if (::left(tmp, 11) == "signed long")
            pos += 11;
         else if (::left(tmp, 13) == "unsigned long")
            pos += 13;
         else if (::left(tmp,  8) == "long int")
            pos += 8;
         else if (::left(tmp, 11) == "long double")
            pos += 11;
         else if (::instr(tmp, " ") > 0)
            pos += ::rinstr(tmp, " ");
         else
            pos += tmp.len();
      }
      else if (::instr(stat, " const ") > 0)             // type const name
      {
         pos = ::instr(stat, " const ") + 1;
      }
      else if (::instr(stat, " ") > 0)                   // type name
      {
         pos = 0;
         if (::left(stat, 11) == "signed char")
            pos += 11;
         else if (::left(stat, 13) == "unsigned char")
            pos += 13;
         else if (::left(stat, 16) == "signed short int")
            pos += 16;
         else if (::left(stat, 18) == "unsigned short int")
            pos += 18;
         else if (::left(stat, 12) == "signed short")
            pos += 12;
         else if (::left(stat, 14) == "unsigned short")
            pos += 14;
         else if (::left(stat,  9) == "short int")
            pos += 9;
         else if (::left(stat, 10) == "signed int")
            pos += 10;
         else if (::left(stat, 12) == "unsigned int")
            pos += 12;
         else if (::left(stat, 15) == "signed long int")
            pos += 15;
         else if (::left(stat, 17) == "unsigned long int")
            pos += 17;
         else if (::left(stat, 11) == "signed long")
            pos += 11;
         else if (::left(stat, 13) == "unsigned long")
            pos += 13;
         else if (::left(stat,  8) == "long int")
            pos += 8;
         else if (::left(stat, 11) == "long double")
            pos += 11;
         else if (::instr(stat, " ") > 0)
            pos += ::rinstr(stat, " ");
      }

      if (pos > 0 && pos < len())
      {
         name = ::mid(stat, pos);
         name.left("[");
         type = ::left(stat, pos) + ::mid(stat, pos+name.len());
      }
      else
         type = stat;
   }
   else
   {
      PRStatement stat = *this;
      while (stat.instr(" (") >= 0) stat.subst(" (", "(");
      while (stat.instr("( ") >= 0) stat.subst("( ", "(");
      while (stat.instr(" )") >= 0) stat.subst(" )", ")");
      while (stat.instr(") ") >= 0) stat.subst(") ", ")");
      while (stat.instr(" [") >= 0) stat.subst(" [", "[");
      while (stat.instr("[ ") >= 0) stat.subst("[ ", "[");
      while (stat.instr(" ]") >= 0) stat.subst(" ]", "]");
      while (stat.instr("] ") >= 0) stat.subst("] ", "]");
      while (stat.instr(" *") >= 0) stat.subst(" *", "*");
      while (stat.instr("* ") >= 0) stat.subst("* ", "*");
      while (stat.instr(" :") >= 0) stat.subst(" :", ":");
      while (stat.instr(": ") >= 0) stat.subst(": ", ":");
      while (stat.instr(" ,") >= 0) stat.subst(" ,", ",");
      while (stat.instr(", ") >= 0) stat.subst(", ", ",");
      while (stat.instr("  ") >= 0) stat.subst("  ", " ");
      if (::right(stat, 5) == "const")
         stat.left(stat.len()-5);
      stat.trim();

      Entier pos;
      if ((pos = stat.instr("(*const ")) > 0)        // type (*const name[])
      {
         name = ::bloc(stat, "(*const ", ")");
         pos += 8;
      }
      else if ((pos = stat.instr("(*")) > 0)         // type (*name[])
      {
         name = ::bloc(stat, "(*", ")");
         pos += 2;
      }
      else if ((pos = stat.instr("*const(")) > 0)    // type *const(name[])
      {
         name = ::bloc(stat, "*const(", ")");
         pos += 7;
      }
      else if ((pos = stat.instr("*(")) > 0)         // type *(name[])
      {
         name = ::bloc(stat, "*(", ")");
         pos += 2;
      }
      else if ((pos = stat.instr("::*")) > 0)         // type (T::*name[])
      {
         name = ::bloc(stat, "::*", ")");
         pos += 3;
      }
      else
      {
         PRStatement bloc = ::rbloc(*this, "(", ")");
         name = ::left(*this, len() - bloc.len() - 2);
         name.delChar(' ');
      }

      if (::right(name, 1) == "]")
      {
         PRStatement bloc = ::rbloc(stat, "[", "]");
         name.left(name.len() - bloc.len() - 2);
      }
      type = ::left(stat, pos) + ::mid(stat, pos+name.len());
//      while (type.instr(",") >= 0) type.subst(",", ", ");
//      type.subst(",", ", ");
   }
   type.trim();
   name.trim();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return;
}

//*****************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//    3 cas:
//       // start          // detecté par cppHeader
//       //
//       // stop
//
//       /* start */       // detecté par cHeader
//       /*       */
//       /* stop  */
//
//       /* start          // marqué par estEnCommentaireC()
//
//          stop*/
//
//*****************************************************************************
ERMsg PRStatement::readHeaderLine (PRistream& fichier)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG


   ERMsg msg = ERMsg::OK;

   static char line[MAXSIZE];
   memset(line, 0x0, MAXSIZE);

   if (fichier.estEnCommentaireC())
   {
      fichier.getline(line, MAXSIZE);
      *this = line;
      rtrim();
      bool tokPresent = (strncmp(::ltrim(*this), tokenEnteteFin, strlen(tokenEnteteFin)) == 0);
      bool finComment = (::right(*this, 2) == "*/");
      if (finComment)
      {
         if (tokPresent)
            *this = "</header>";
         else
            msg = ERMsg(ERMsg::ERREUR, "ERR_FIN_DE_COMMENTAIRE_INATTENDUE");
         fichier.asgEnCommentaireC(false);
      }
      else
      {
         if (tokPresent)
            msg = ERMsg(ERMsg::ERREUR, "ERR_FIN_DE_COMMENTAIRE_ATTENDUE");
      }
   }
   else
   {
      bool cHeader   = false;
      bool cppHeader = false;

      char inputChar = static_cast<char>(fichier.peek());
      if (inputChar == '/')
      {
         fichier.get(inputChar);
         inputChar = static_cast<char>(fichier.peek());
         if (inputChar == '/')
         {
            fichier.get(inputChar);
            cppHeader = true;
         }
         else if (inputChar == '*')
         {
            fichier.get(inputChar);
            cHeader = true;
         }
         else
         {
            fichier.putback(inputChar);
            msg = ERMsg(ERMsg::ERREUR, "ERR_CHAR_INATTENDU");
         }
      }
      else
      {
         msg = ERMsg(ERMsg::ERREUR, "ERR_CHAR_INATTENDU");
      }

      if (msg && cppHeader)
      {
         fichier.getline(line, MAXSIZE);
         *this = line;
         rtrim();
         bool tokPresent = (strncmp(::ltrim(*this), tokenEnteteFin, strlen(tokenEnteteFin)) == 0);
         if (tokPresent)
            *this = "</header>";
      }
      if (msg && cHeader)
      {
         fichier.getline(line, MAXSIZE);
         *this = line;
         rtrim();
         bool tokPresent = (strncmp(::ltrim(*this), tokenEnteteFin, strlen(tokenEnteteFin)) == 0);
         bool finComment = (::right(*this, 2) == "*/");
         if (finComment)
         {
            if (tokPresent)
               *this = "</header>";
         }
         else
         {
            msg = ERMsg(ERMsg::ERREUR, "ERR_FIN_DE_COMMENTAIRE_ATTENDUE");
            fichier.asgEnCommentaireC(true);
         }
      }
   }

   if (msg && fichier.eof())
      msg = ERMsg(ERMsg::ERREUR, "ERR_FIN_DE_FICHIER");
   else if (msg && !fichier)
      msg = ERMsg(ERMsg::ERREUR, "ERR_ERREUR_LECTURE");

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//*****************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERMsg PRStatement::readLine (PRistream& fichier)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   static char line[MAXSIZE];
   memset(line, 0x0, MAXSIZE);

   fichier.getline(line, MAXSIZE);
   *this = line;

   ERMsg msg = ERMsg::OK;
   if (! fichier)
      msg = ERMsg(ERMsg::ERREUR, "ERR_LECTURE_FICHIER");
   if (fichier.eof())
      msg = ERMsg(ERMsg::ERREUR, "ERR_FIN_DE_FICHIER");

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//*****************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERMsg PRStatement::readStatement (PRistream& fichier)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Entier nbrPar = 0;

   static char charbuf[MAXSIZE];
   static char* line = charbuf+1;
   static const char* LINEPTRMAX = line + MAXSIZE - 2;

   memset(charbuf, 0x0, MAXSIZE);

   char inputLast = ' ';
   char inputChar = ' ';
   char inputNext = ' ';
   char *linePtr  = line;
   char *crlfPos  = line;
   char *prepPos  = line;

   Booleen escapeCarOn         = FAUX;
   Booleen preprocOn           = FAUX;
   Booleen singleQuotesOn      = FAUX;
   Booleen doubleQuotesOn      = FAUX;
   Booleen addCharToString    /* = FAUX*/;
   Booleen delLastFromString  /* = FAUX*/;
   Booleen getNextCharFromFile/* = VRAI*/;
   Booleen overFlow = FAUX;
   Booleen done = FAUX;

   if (fichier.estEnCommentaireC())
   {
      skipToString(fichier, "*/");
      fichier.asgEnCommentaireC(false);
   }

   fichier.get(inputChar);
   while ( ! (done || fichier.eof() ) )
   {
      inputNext = static_cast<char>(fichier.peek());

      addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = VRAI;
      switch (inputChar)
      {
         case (9)  :   // \t
            escapeCarOn = FAUX;
            if (preprocOn) inputChar = ' ';
            if (singleQuotesOn || doubleQuotesOn || preprocOn) goto AddToLine;
         break;
         case (10)  :   // LF
            if (preprocOn && !escapeCarOn)
            {
               if (*prepPos == '#') // #line directive
               {
                  char* cP = prepPos+1;
                  while (*cP && *cP == ' ') ++cP;
                  if (*cP && strncmp(cP, "line", 4) == 0)
                     fichier.asgNoLigne(atoi(cP+4));
               }
               if (prepPos == line)
               {
                  addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
                  done = VRAI;
               }
               else
               {
                  addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = VRAI;
                  memset(prepPos, 0x0, linePtr-prepPos+1);
                  linePtr = prepPos;
                  prepPos = line;
                  crlfPos = linePtr;
                  escapeCarOn = FAUX;
               }
               preprocOn = FAUX;
               goto AddToLine;
            }
            else
            {
               crlfPos = linePtr;
               escapeCarOn = FAUX;
               if (singleQuotesOn || doubleQuotesOn || preprocOn) goto AddToLine;
            }
         break;
         case (34)  :   // "
            if (!escapeCarOn && !singleQuotesOn) doubleQuotesOn = !doubleQuotesOn;
            escapeCarOn = FAUX;
            goto AddToLine;
//            break;
         case (35)  :   // #
            prepPos = linePtr;
            if (!escapeCarOn && !singleQuotesOn && !doubleQuotesOn && crlfPos == linePtr) preprocOn = VRAI;
            escapeCarOn = FAUX;
            goto AddToLine;
//            break;
         case (39)  :   // '
            if (!escapeCarOn && !doubleQuotesOn) singleQuotesOn = !singleQuotesOn;
            escapeCarOn = FAUX;
            goto AddToLine;
//            break;
         case (92)  :   /* \\ */
            escapeCarOn = !escapeCarOn;
            if (singleQuotesOn || doubleQuotesOn || preprocOn) goto AddToLine;
         break;
         default:
            escapeCarOn = FAUX;
            if (singleQuotesOn || doubleQuotesOn || preprocOn) goto AddToLine;
         break;
      }

      switch (inputChar)
      {
         case (0)  :
            addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = VRAI;
         break;
         case ('\n')  ://LF
            if (inputLast == '\\')
            {
               addCharToString = FAUX, delLastFromString = VRAI, getNextCharFromFile = VRAI;
               done = FAUX;
            }
            else if (strncmp(line, "#", 1) == 0)
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
               done = VRAI;
            }
            else
            {
               inputChar = ' ';    // !!! continue as space  !!!
            }
         case '\t' :
            if (inputChar == '\t') // skip LF fallthrough
               inputChar = ' ';    // !!! continue as space  !!!
         case ' ' :
            if (inputChar == ' ')  // skip LF fallthrough
            {
               if (inputLast == ' ')
               {
                  addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = VRAI;
                  done = FAUX;
               }
               else if (strcmp(line, "else") == 0)
               {
                  addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = FAUX;
                  done = VRAI;
               }
               else if (strcmp(line, "do")  == 0)
               {
                  addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = FAUX;
                  done = VRAI;
               }
               else if (strcmp(line, "try")  == 0)
               {
                  addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = FAUX;
                  done = VRAI;
               }
               else
               {
                  addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = VRAI;
                  done = FAUX;
               }
            }
         break;
         case (26) ://EOF
         case EOF  ://EOF
            addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            done = VRAI;
         break;
         case '(' :
            nbrPar ++;
            addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = VRAI;
         break;
         case ')' :
            nbrPar --;
            addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = VRAI;
            if (nbrPar == 0)
            {
               if ((strstr(line, " catch ") != 0) || (strncmp(line, "catch ",  6) == 0) ||
                   (strstr(line, " catch(") != 0) || (strncmp(line, "catch(",  6) == 0) ||
                   (strstr(line, " do ")    != 0) || (strncmp(line, "do ",     3) == 0) ||
                   (strstr(line, " do(")    != 0) || (strncmp(line, "do(",     3) == 0) ||
                   (strstr(line, " for ")   != 0) || (strncmp(line, "for ",    4) == 0) ||
                   (strstr(line, " for(")   != 0) || (strncmp(line, "for(",    4) == 0) ||
                   (strstr(line, " if ")    != 0) || (strncmp(line, "if ",     3) == 0) ||
                   (strstr(line, " if(")    != 0) || (strncmp(line, "if(",     3) == 0) ||
                   (strstr(line, " switch ")!= 0) || (strncmp(line, "switch ", 7) == 0) ||
                   (strstr(line, " switch(")!= 0) || (strncmp(line, "switch(", 7) == 0) ||
                   (strstr(line, " while ") != 0) || (strncmp(line, "while ",  6) == 0) ||
                   (strstr(line, " while(") != 0) || (strncmp(line, "while(",  6) == 0))
               {
                  done = VRAI;
                  getNextCharFromFile = FAUX;
               }
            }
         break;
         case '{' :
            done = VRAI;
            if (nbrPar > 0)
            {
               addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = VRAI;
               done = FAUX;
            }
            else if (((linePtr-line) > 0 && strncmp(linePtr-1, ")", 1) == 0) ||
                     ((linePtr-line) > 1 && strncmp(linePtr-2, ")", 1) == 0))
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
            else if (((linePtr-line) > 4 && strncmp(linePtr-5, "const", 5) == 0) ||
                     ((linePtr-line) > 5 && strncmp(linePtr-6, "const", 5) == 0))
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
            else if (strncmp(line, "$base_class$", 12) == 0)
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
            else if (strncmp(line, "class", 5) == 0)
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
            else if (strncmp(line, "catch", 5) == 0)
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
            else if (strncmp(line, "enum", 4) == 0)
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
            else if (strncmp(line, "extern", 6) == 0)
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
            else if (strncmp(line, "namespace", 9) == 0)
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
            else if (strncmp(line, "union", 5) == 0)
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
            else if (strncmp(line, "struct", 6) == 0)
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
            else if (strncmp(line, "typedef", 7) == 0)
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
            else if (strncmp(line, "template", 8) == 0)
            {
               addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
            else
            {
               addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = FAUX;
            }
         break;
         case '}' :
//            nbrAcc --;
            if (nbrPar > 0)
            {
               addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = VRAI;
               done = FAUX;
            }
            else
            {
               addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = FAUX;
               done = VRAI;
            }
         break;
         case ';' :
            addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = VRAI;
            if (nbrPar == 0)
            {
               getNextCharFromFile = FAUX;
               done = VRAI;
            }
         break;
         case ':' :
            addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = VRAI;
            if (inputLast != ':' && inputNext != ':' && nbrPar == 0)
            {
               if (contiensUniquement(line, "case") ||
                   contiensUniquement(line, "default") ||
                   contiensUniquement(line, "private") ||
                   contiensUniquement(line, "protected") ||
                   contiensUniquement(line, "public") /* ||
                   inputLast == ' ' && strstr(line, " ") == (linePtr-1) ||
                   inputLast != ' ' && strstr(line, " ") == NUL*/)
               {
                  done = VRAI;
                  getNextCharFromFile = FAUX;
               }
            }
         break;
         case '/' :
            if (inputLast == '/')
            {
               char* startP = linePtr-1;
               readToChar(fichier, '\n', startP, static_cast<int>(LINEPTRMAX-startP));
               if (strncmp(linePtr, tokenEnteteDebut, strlen(tokenEnteteDebut)) == 0)
               {
                  strcpy(line, "<header>");
                  linePtr = line + strlen(line);
                  addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
                  done = VRAI;
               }
               else
               {
                  memset(startP, 0x0, LINEPTRMAX-startP);
                  inputChar = ' ';
                  addCharToString = FAUX, delLastFromString = VRAI, getNextCharFromFile = VRAI;
               }
            }
            else
            {
               addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = VRAI;
            }
         break;
         case '*' :
            if (inputLast == '/')
            {
               char* startP = linePtr-1;
               int   iStr   = -1;
               readToString(fichier, "\n", "*/", startP, static_cast<int>(LINEPTRMAX-startP), iStr);
               if (iStr == 0)
               {
                  if (strncmp(linePtr, tokenEnteteDebut, strlen(tokenEnteteDebut)) == 0)
                  {
                     strcpy(line, "<header>");
                     linePtr = line + strlen(line);
                     addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
                     done = VRAI;
                     fichier.asgEnCommentaireC(true);
                  }
                  else
                  {
                     readToString(fichier, "*/", startP, static_cast<int>(LINEPTRMAX-startP));
                     memset(startP, 0x0, LINEPTRMAX-startP);
                     inputChar = ' ';
                     addCharToString = FAUX, delLastFromString = VRAI, getNextCharFromFile = VRAI;
                  }
               }
               else
               {
                  if (strncmp(startP, tokenEnteteDebut, strlen(tokenEnteteDebut)) == 0)
                  {
                     strcpy(line, "<header>");
                     linePtr = line + strlen(line);
                     addCharToString = FAUX, delLastFromString = FAUX, getNextCharFromFile = FAUX;
                     done = VRAI;
                  }
                  else
                  {
                     memset(startP, 0x0, LINEPTRMAX-startP);
                     inputChar = ' ';
                     addCharToString = FAUX, delLastFromString = VRAI, getNextCharFromFile = VRAI;
                  }
               }
            }
            else
            {
               addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = VRAI;
            }
         break;
         default:
            addCharToString = VRAI, delLastFromString = FAUX, getNextCharFromFile = VRAI;
         break;
      }

      AddToLine:
      if (delLastFromString) *(--linePtr) = 0;
      if (getNextCharFromFile) fichier.get(inputNext);
      if (linePtr < LINEPTRMAX)
      {
         if (addCharToString)
         {
            *(linePtr++) = inputChar;
         }
      }
      else
         overFlow = VRAI;
      inputLast = inputChar;
      inputChar = inputNext;
   }

   //IF EOF(fichier) AND (LEN(Line$) > 0) THEN Line$ = LEFT$(Line$, LEN(Line$) - 1)
   if (overFlow)
   {
      line[0] = inputLast, line[1] = 0;
   }
   else
   {
      while (linePtr != line && *(linePtr-1) == ' ') *(--linePtr) = 0;
   }
   *this = line;

   ERMsg msg = ERMsg::OK;
   if (fichier.eof())
      msg = ERMsg(ERMsg::ERREUR, "ERR_FIN_DE_FICHIER");
   else if (!fichier)
      msg = ERMsg(ERMsg::ERREUR, "ERR_ERREUR_LECTURE");

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return msg;
}

//*****************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERMsg PRStatement::readToChar(PRistream& fichier, const char stopChar, char* bufferP, int dimBuf)
{
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   char  inputChar;
   char  inputLast = ' ';
   char* limitP = bufferP + dimBuf - 1;

   fichier.get(inputChar);
   while (fichier && inputChar != stopChar)
   {
      if (inputChar == '\t' || inputChar == '\n')
         inputChar = ' ';
      if (bufferP != limitP && (inputChar != ' ' || inputLast != ' '))
         *bufferP++ = inputChar;
      inputLast = inputChar;
      fichier.get(inputChar);
   }
   *bufferP = 0x0;

   ERMsg msg = ERMsg::OK;
   if (fichier.eof())
      msg = ERMsg(ERMsg::ERREUR, "ERR_FIN_DE_FICHIER");
   else if (!fichier)
      msg = ERMsg(ERMsg::ERREUR, "ERR_ERREUR_LECTURE");

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return msg;
}

//*****************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERMsg PRStatement::readToString(PRistream& fichier, const char* string, char* bufferP, int dimBuf)
{
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   char  inputChar;
   char  inputLast = ' ';
   char* limitP = bufferP + dimBuf;

   Booleen done = FAUX;
   fichier.get(inputChar);
   while (! done && fichier)
   {
      while (fichier && inputChar != string[0])
      {
         if (inputChar == '\t' || inputChar == '\n')
            inputChar = ' ';
         if (bufferP != limitP && (inputChar != ' ' || inputLast != ' '))
            *bufferP++ = inputChar;
         inputLast = inputChar;
         fichier.get(inputChar);
      }

      done = VRAI;
      for (size_t i = 1; fichier && i < strlen(string); ++i)
      {
         if (inputChar == '\t' || inputChar == '\n')
            inputChar = ' ';
         if (bufferP != limitP && (inputChar != ' ' || inputLast != ' '))
            *bufferP++ = inputChar;
         inputLast = inputChar;
         fichier.get(inputChar);
         if (inputChar != string[i])
         {
            done = FAUX;
            break;
         }
      }
   }
   if (done && bufferP != limitP)
   {
      size_t n = strlen(string)-1;
      memset(bufferP-n, 0x0, n);
   }

   ERMsg msg = ERMsg::OK;
   if (fichier.eof())
      msg = ERMsg(ERMsg::ERREUR, "ERR_FIN_DE_FICHIER");
   else if (!fichier)
      msg = ERMsg(ERMsg::ERREUR, "ERR_ERREUR_LECTURE");

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return msg;
}

//*****************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERMsg PRStatement::readToString(PRistream& fichier,
                                const char* string0,
                                const char* string1,
                                char* bufferP, int dimBuf,
                                int&  indStr)
{
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   char  inputChar;
   char  inputLast = ' ';
   char* limitP = bufferP + dimBuf;
   const char* string[2] = {string0, string1};

   indStr = -1;

   Booleen done = FAUX;
   fichier.get(inputChar);
   while (! done && fichier)
   {
      while (fichier &&
             inputChar != string[0][0] &&
             inputChar != string[1][0])
      {
         if (inputChar == '\t' || inputChar == '\n')
            inputChar = ' ';
         if (bufferP != limitP && (inputChar != ' ' || inputLast != ' '))
            *bufferP++ = inputChar;
         inputLast = inputChar;
         fichier.get(inputChar);
      }

      indStr = (inputChar == string[0][0]) ? 0 : 1;
      done = VRAI;
      for (size_t i = 1; fichier && i < strlen(string[indStr]); ++i)
      {
         if (inputChar == '\t' || inputChar == '\n')
            inputChar = ' ';
         if (bufferP != limitP && (inputChar != ' ' || inputLast != ' '))
            *bufferP++ = inputChar;
         inputLast = inputChar;
         fichier.get(inputChar);
         if (inputChar != string[indStr][i])
         {
            done = FAUX;
            break;
         }
      }
   }
   if (done && bufferP != limitP)
   {
      size_t n = strlen(string[indStr])-1;
      memset(bufferP-n, 0x0, n);
   }
   else
   {
      indStr = -1;
   }

   ERMsg msg = ERMsg::OK;
   if (fichier.eof())
      msg = ERMsg(ERMsg::ERREUR, "ERR_FIN_DE_FICHIER");
   else if (!fichier)
      msg = ERMsg(ERMsg::ERREUR, "ERR_ERREUR_LECTURE");

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return msg;
}

//*****************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERMsg PRStatement::skipToChar(PRistream& fichier, const char stopChar)
{
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   char inputChar = 0x0;

   while (fichier && inputChar != stopChar)
      fichier.get(inputChar);

   ERMsg msg = ERMsg::OK;
   if (fichier.eof())
      msg = ERMsg(ERMsg::ERREUR, "ERR_FIN_DE_FICHIER");
   else if (!fichier)
      msg = ERMsg(ERMsg::ERREUR, "ERR_ERREUR_LECTURE");

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return msg;
}

//*****************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
ERMsg PRStatement::skipToString(PRistream& fichier, const char* string)
{
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   char inputChar = 0x0;

   Booleen done = FAUX;
   while (! done && fichier)
   {
      while (fichier && inputChar != string[0])
         fichier.get(inputChar);

      done = VRAI;
      for (size_t i = 1; fichier && i < strlen(string); ++i)
      {
         fichier.get(inputChar);
         if (inputChar != string[i])
         {
            done = FAUX;
            break;
         }
      }
   }

   ERMsg msg = ERMsg::OK;
   if (fichier.eof())
      msg = ERMsg(ERMsg::ERREUR, "ERR_FIN_DE_FICHIER");
   else if (!fichier)
      msg = ERMsg(ERMsg::ERREUR, "ERR_ERREUR_LECTURE");

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return msg;
}

//*****************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
PRStatement traduisAOEM (const PRStatement& copie)
{
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   static unsigned char tbl_ansi_to_oem[] =
     {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
      0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,
      0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,
      0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f,
      0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f,
      0x50,0x51,0x52,0x53,0x54,0x55,0x56,0x57,0x58,0x59,0x5a,0x5b,0x5c,0x5d,0x5e,0x5f,
      0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,0x6f,
      0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,0x7b,0x7c,0x7d,0x7e,0x7f,
      0x5f,0x5f,0x27,0x9f,0x22,0x2e,0xc5,0xce,0x5e,0x25,0x53,0x3c,0x4f,0x5f,0x5f,0x5f,
      0x5f,0x27,0x27,0x22,0x22,0x07,0x2d,0x2d,0x7e,0x54,0x73,0x3e,0x6f,0x5f,0x5f,0x59,
      0xff,0xad,0xbd,0x9c,0xcf,0xbe,0xdd,0xf5,0xf9,0xb8,0xa6,0xae,0xaa,0xf0,0xa9,0xee,
      0xf8,0xf1,0xfd,0xfc,0xef,0xe6,0xf4,0xfa,0xf7,0xfb,0xa7,0xaf,0xac,0xab,0xf3,0xa8,
      0xb7,0xb5,0xb6,0xc7,0x8e,0x8f,0x92,0x80,0xd4,0x90,0xd2,0xd3,0xde,0xd6,0xd7,0xd8,
      0xd1,0xa5,0xe3,0xe0,0xe2,0xe5,0x99,0x9e,0x9d,0xeb,0xe9,0xea,0x9a,0xed,0xe8,0xe1,
      0x85,0xa0,0x83,0xc6,0x84,0x86,0x91,0x87,0x8a,0x82,0x88,0x89,0x8d,0xa1,0x8c,0x8b,
      0xd0,0xa4,0x95,0xa2,0x93,0xe4,0x94,0xf6,0x9b,0x97,0xa3,0x96,0x81,0xec,0xe7,0x98};

   PRStatement tmp;

   const EntierN dim = copie.len();
   for (EntierN i = 0; i < dim; ++i)
   {
      unsigned char c = static_cast<unsigned char>(copie[i]);
      tmp += tbl_ansi_to_oem[c];
   }

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return tmp;
}

//*****************************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//*****************************************************************************
PRStatement traduisANomFichier (const PRStatement& copie)
{
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   PRStatement tmp = copie;
   tmp.delChar(' ');

   // ---  les opérateurs
   while (tmp.instr("operator!=") >= 0) tmp.subst("operator!=", "operator$ne$");
   while (tmp.instr("operator!")  >= 0) tmp.subst("operator!",  "operator$not$");
   while (tmp.instr("operator->*")>= 0) tmp.subst("operator->*","operator$drm$");
   while (tmp.instr("operator->") >= 0) tmp.subst("operator->", "operator$drf$");
   while (tmp.instr("operator--") >= 0) tmp.subst("operator--", "operator$dec$");
   while (tmp.instr("operator-=") >= 0) tmp.subst("operator-=", "operator$subasg$");
   while (tmp.instr("operator-")  >= 0) tmp.subst("operator-",  "operator$sub$");
   while (tmp.instr("operator%=") >= 0) tmp.subst("operator$=", "operator$modasg$");
   while (tmp.instr("operator%")  >= 0) tmp.subst("operator$",  "operator$mod$");
   while (tmp.instr("operator&=") >= 0) tmp.subst("operator&=", "operator$andasg$");
   while (tmp.instr("operator&")  >= 0) tmp.subst("operator&",  "operator$and$");
   while (tmp.instr("operator*=") >= 0) tmp.subst("operator*=", "operator$mulasg$");
   while (tmp.instr("operator*")  >= 0) tmp.subst("operator*",  "operator$mul$");
   while (tmp.instr("operator/=") >= 0) tmp.subst("operator/=", "operator$divasg$");
   while (tmp.instr("operator/")  >= 0) tmp.subst("operator/",  "operator$div$");
   while (tmp.instr("operator.*") >= 0) tmp.subst("operator.*", "operator$acm$");
   while (tmp.instr("operator." ) >= 0) tmp.subst("operator.",  "operator$acs$");
   while (tmp.instr("operator()") >= 0) tmp.subst("operator()", "operator$par$");
   while (tmp.instr("operator[]") >= 0) tmp.subst("operator[]", "operator$ind$");
   while (tmp.instr("operator::") >= 0) tmp.subst("operator::", "operator$scp$");
   while (tmp.instr("operator^=") >= 0) tmp.subst("operator^=", "operator$xorasg$");
   while (tmp.instr("operator^")  >= 0) tmp.subst("operator^",  "operator$xor$");
   while (tmp.instr("operator|=") >= 0) tmp.subst("operator|=", "operator$orasg$");
   while (tmp.instr("operator|")  >= 0) tmp.subst("operator|",  "operator$or$");
   while (tmp.instr("operator~")  >= 0) tmp.subst("operator~",  "operator$neg$");
   while (tmp.instr("operator++") >= 0) tmp.subst("operator++", "operator$inc$");
   while (tmp.instr("operator+=") >= 0) tmp.subst("operator+=", "operator$addasg$");
   while (tmp.instr("operator+")  >= 0) tmp.subst("operator+",  "operator$add$");
   while (tmp.instr("operator<<=")>= 0) tmp.subst("operator<<=","operator$shlasg$");
   while (tmp.instr("operator<<") >= 0) tmp.subst("operator<<", "operator$ins$");
   while (tmp.instr("operator<=") >= 0) tmp.subst("operator<=", "operator$le$");
   while (tmp.instr("operator<")  >= 0) tmp.subst("operator<",  "operator$lt$");
   while (tmp.instr("operator>>=")>= 0) tmp.subst("operator>>=","operator$shrasg$");
   while (tmp.instr("operator>>") >= 0) tmp.subst("operator>>", "operator$xtr$");
   while (tmp.instr("operator>=") >= 0) tmp.subst("operator>=", "operator$ge$");
   while (tmp.instr("operator>")  >= 0) tmp.subst("operator>",  "operator$gt$");
   while (tmp.instr("operator==") >= 0) tmp.subst("operator==", "operator$eq$");
   while (tmp.instr("operator=")  >= 0) tmp.subst("operator=",  "operator$asg$");

   // ---  le reste
   while (tmp.instr(":")  >= 0) tmp.subst(":",  "-");
   while (tmp.instr("*")  >= 0) tmp.subst("*",  "@");
   while (tmp.instr("<")  >= 0) tmp.subst("<",  "{");
   while (tmp.instr(">")  >= 0) tmp.subst(">",  "}");
   while (tmp.instr("/")  >= 0) tmp.subst("/",  "$sl$");
   while (tmp.instr("\\") >= 0) tmp.subst("\\", "$bs$");

   tmp.left(250);
   tmp += PRStatement(".htm");

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return tmp;
}
