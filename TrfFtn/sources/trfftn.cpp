//************************************************************************
// --- Copyright (c) 2006
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS / ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette
// --- permission apparaissent dans toutes les copies ainsi que dans la
// --- documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//******************************************************************************
// $Id$
// Fichier: trfcpp.cpp
//******************************************************************************

#include "sytypes.h"
#include "ermsg.h"

#include "prifstr.h"
#include "prfictmp.h"
#include "prstat.h"
#include "wbcpyrht.h"

#include "DirectoryIterator.h"
#include "tpvecsmp.h"

#include <fstream>
#include <iostream>
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

// ---  Redéfinition de std::min local à cause de MSVC win64
template<typename Tp> inline const Tp& std_min(const Tp& l, const Tp& r)
{
   return (l < r) ? l : r;
}

#include <cstdio>
#include <cstring>
#include <iterator>
#include <map>
#include <set>
#include <stack>
#include <time.h>

#if defined(__WIN32__)
#  define STRICMP _stricmp
#  define STRDUP  _strdup
#else
#  include <strings.h>
#  define STRICMP strcasecmp
#  define STRDUP  strdup
#endif

char VERSION_PARSER[] = "TrfFtn v2.0.0";

// ---  Structure de pair pour contenir les mots à chercher
struct Token
{
   enum Type
   {
      NORMAL,
      LINKWEBPPFUNC,    // Function dans Webpp
      LINKFICSRC,       // Fichier source
      LINKFUNC,         // Fonction dans un fichier src
      KEYWORD,          // Mot clef du Fortran
      FUNC_BGN,         // Mot clef du Fortran; début de fonction-sub
      FUNC_END,         // Mot clef du Fortran; fin de fonction-sub
      INCLUDE,          // Mot clef du Fortran; include
      INTRINSIC,        // Fonction intrinsic du Fortran
      USER
   };
   ConstCarP firstP;
   ConstCarP secondP;
   Type      type;
   Token(ConstCarP p1P = 0, Type t = NORMAL)
      : firstP(p1P), secondP(p1P), type(t) {}
   Token(ConstCarP p1P, ConstCarP p2P, Type t = NORMAL)
      : firstP(p1P), secondP(p2P), type(t) {}
};
inline Entier compareToken (const Token& p1, const Token& p2)
{
   using namespace std;
   return STRICMP(p1.firstP, p2.firstP);
}

struct PairCarP
{
   ConstCarP firstP;
   ConstCarP secondP;
   PairCarP(ConstCarP p1P = 0, ConstCarP p2P = 0)
      : firstP(p1P), secondP(p2P) {}
};
inline Entier comparePairCarP (const PairCarP& p1, const PairCarP& p2)
{
   using namespace std;
   return strcmp(p1.firstP, p2.firstP);
}

struct Contexte
{
   typedef std::vector<char> Buffer;
   PRStatement       nomOu;
   std::ofstream    ficOu;
   PRStatement       nomIn;
   Buffer::iterator  cI;    // courant
   Buffer::iterator  fI;    // eob
   Entier            ligne;
   PRStatement       fncActu;
   bool              inDeclaration;
   bool              inInclude;
   bool              singleQuotesOn;
   bool              ftnCommentOn;
   bool              ftnDebugOn;
   bool              preprocOn;

   Contexte() : nomOu(), ficOu(), nomIn(), cI(), fI(), ligne(-1), fncActu(),
   inDeclaration(false), inInclude(false), singleQuotesOn(false), ftnDebugOn(false),
   preprocOn(false){}
};

// ---  Variables globales
Entier tailleMin = 1024;
TPVecteurSimple<Token> lstTokens(256);
PRStatement repHTML;
PRStatement urlMain;

// ---  MultiMap pour les liens inverses
typedef std::map<PRStatement, std::set<PRStatement> > Mmap;
Mmap mmap;

// ---  Stack pour les déclarations d'un fichier
typedef std::stack<PRStatement> Stack;
Stack declStack;

// ---  Couleurs par défaut
Car fontKeywordP[]   = "<span class=\"keyword\">";
Car fontIntrinsicP[] = "<span class=\"intrinsic\">";
Car fontCommentP[]   = "<span class=\"comment\">";
Car fontDebugP[]     = "<span class=\"debug\">";
Car fontPreprocP[]   = "<span class=\"preproc\">";
Car fontStringP[]    = "<span class=\"string\">";
Car fontUserP[]      = "<span class=\"user\">";
Car endFont[]        = "</span>";


//==========================================================================
//==========================================================================
//       Encode certains signes particuliers
//==========================================================================
//==========================================================================
PRStatement encodeHTM(const PRStatement& inpStr)
{

   PRStatement tmp;
   char car;

   const EntierN dim = inpStr.len();
   const Entier  LIMSIZE = QBString::MAXSIZE - 10;  // Pour ne pas deborder
   for (EntierN i = 0; i < dim && tmp.len() < LIMSIZE; ++i)
   {
      car = inpStr[i];
      switch (car)
      {
         case '&' : tmp += "&amp;";  break;
         case '<' : tmp += "&lt;";   break;
         case '>' : tmp += "&gt;";   break;
         default  : tmp += car;
     }
   }

#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   return tmp;
}

//==========================================================================
//==========================================================================
//       Retourne le prochain car non blanc
//==========================================================================
//==========================================================================
int prochainCharNonBlanc(std::vector<char>::iterator cI,
                         std::vector<char>::iterator fI)
{
   while(cI != fI && (*cI == '\n' || *cI == '\t' || *cI == ' '))
   {
      ++cI;
   }

   return *cI;
}

//==========================================================================
//==========================================================================
//       Ecris l'entête des fichier html
//==========================================================================
//==========================================================================
void ecrisEnteteHTML(std::ostream& os, const PRStatement& t = "")
{
   os << WBCopyright();

   os << "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional\" \"http://www.w3.org/TR/html4/loose.dtd\">" << endl;
   os << "<html>" << endl;
   os << "<head>" << endl;
   os << "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" >" << endl;
   os << "<link rel=\"stylesheet\" type=\"text/css\" href=\"TrfFtn.css\" />" << endl;
   os << "<title>" << t << "</title>" << endl;
   os << "</head>" << endl;
   os << "<body>" << endl;
   os << "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" summary=\"Main table\">" << endl;

   os << "<tr><td>" << endl;     // Main table

   os << "<div class=\"menu\">" << endl;
   os << "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" summary=\"WebFtn main menu\">" << endl;
   os << "<tr>" << endl;
   os << "<td><img src=\"WebFtn.jpg\" alt=\"WebFtn banner\"></td>" << endl;
   os << "<td align=\"center\">" << endl;
   os << "<a class=\"mnu_entry\" href=\"" << urlMain << "\">Home</a>|" << endl;
   os << "<a class=\"mnu_entry\" href=\"WebFtn_Co.htm\">Components</a>|" << endl;
   os << "<a class=\"mnu_entry\" href=\"WebFtn_Al.htm\">Functions</a>|" << endl;
   os << "<a class=\"mnu_entry\" href=\"WebFtn_Fi.htm\">Files</a>" << endl;
   os << "</td>" << endl;
   os << "</table>" << endl;
   os << "</div>" << endl;
   
   os << "</td></tr>" << endl;   // Main table
}

//==========================================================================
//==========================================================================
//       Ecris le pied des fichier html
//==========================================================================
//==========================================================================
void ecrisPiedHTML(std::ostream& os)
{
   os << "<tr><td>" << endl;     // Main table

   os << "<div class=\"menu\">" << endl;
   os << "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" summary=\"WebFtn main menu\">" << endl;
   os << "<tr>" << endl;
   os << "<td><img src=\"WebFtn.jpg\" alt=\"WebFtn banner\"></td>" << endl;
   os << "<td align=\"center\">" << endl;
   os << "<a class=\"mnu_entry\" href=\"" << urlMain << "\">Home</a>|" << endl;
   os << "<a class=\"mnu_entry\" href=\"WebFtn_Co.htm\">Components</a>|" << endl;
   os << "<a class=\"mnu_entry\" href=\"WebFtn_Al.htm\">Functions</a>|" << endl;
   os << "<a class=\"mnu_entry\" href=\"WebFtn_Fi.htm\">Files</a>" << endl;
   os << "</td>" << endl;
   os << "</table>" << endl;
   os << "</div>" << endl;

   os << "</td></tr>" << endl;   // Main table

   time_t now;
   time(&now);
   char* s = asctime( localtime(&now) );
   s[strlen(s)-1] = 0;

   os << "<tr><td>" << endl;     // Main table

   os << "<hr size=\"1\">" << endl;
   os << "<small>" << endl;
   os << "<div>Generated on " << s << "</div>" << endl;
   os << "<div>By " << VERSION_PARSER << "</div>" << endl;
   os << "<div>Copyright &copy; 2006  Institut National de la Recherche Scientifique" << "</div>" <<endl;
   os << "</small>" << endl;

   os << "</td></tr>" << endl;   // Main table
   os << "</table>" << endl;     // Main table

   while (! declStack.empty())
   {
      os << "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" << declStack.top() << ".js\"></script>" << endl;
      declStack.pop();
   }
   os << "<script language=\"JavaScript\" type=\"text/javascript\" src=\"menu.js\"></script>" << endl;
   os << "<script language=\"JavaScript\" type=\"text/javascript\" src=\"wz_tooltip.js\"></script>" << endl;

   os << "</body>" << endl;
   os << "</html>" << endl;
}

//==========================================================================
//==========================================================================
//       Ecris les fichiers des appelants
//==========================================================================
//==========================================================================
void ecrisLstAppelants()
{
   for (Mmap::const_iterator mI = mmap.begin(); mI != mmap.end(); ++mI)
   {
      std::ofstream os(ConstCarP((*mI).first + ".js"));

      os << "function " << (*mI).first << "()" << endl;
      os << "{" << endl;
      os << "   var t = '';" << endl;

      typedef Mmap::value_type::second_type::const_iterator SetIter;
      for (SetIter sI = (*mI).second.begin(); sI != (*mI).second.end(); ++sI)
      {
         if ((*sI) == "") continue;

         PRStatement l = mid((*sI), "@");
         PRStatement s = left((*sI), "@");
         PRStatement f = left(l, "#");
         PRStatement n = mid(mid(l, "#"), 1);
         f = left(f, ".");
         Entier i = f.rinstr("_");
         f = left(f,i) + '.' + mid(f,i+1);
         os << "   t += '";
         os << "<a href=\"" << repHTML << l << "\">" << encodeHTM(s) << "</a> in ";
         os << "<a href=\"" << repHTML << l << "\">" << encodeHTM(f) << "(" << n << ")";
         os << "\\n';" << endl;
      }

      os << "   return menu_display(t);" << endl;
      os << "}" << endl;
   }
}

//==========================================================================
//==========================================================================
//       Ecris la liste alfa des noms de fonction
//==========================================================================
//==========================================================================
void ecrisLstAl()
{
   std::ofstream os("WebFtn_Al.htm");

   ecrisEnteteHTML(os, L"WebFtn - Alphabetical list of functions");

   // ---  Menu avec alphabet
   os << "<tr><td>" << endl;     // Main table
   os << "<a name=\"Top\"></a>" << endl;
   os << "<div class=\"alpha\">" << endl;
   char actu = 0x0;
   int  nentries = 0;
   for (Mmap::const_iterator mI = mmap.begin(); mI != mmap.end(); ++mI)
   {
      PRStatement f = (*mI).first;
      if (f[0] != actu)
      {
         if (actu != 0x0) os << " | ";
         actu = f[0];
         ++nentries;
         os << "<a class=\"alpha_entry\" href=\"#Entry_" << int(actu) << "\">" << actu << "</a>" << endl;
      }
   }
   os << "</div>" << endl;
   os << "</td></tr>" << endl;   // Main table

   // ---  Liste des fonctions
   os << "<tr><td>" << endl;     // Main table
   const int col_size = int( double(mmap.size()+nentries) / 4.0 + 0.5);
   int icol = 0;
   actu = 0x0;
   os << "<div class=\"main\">" << endl;
   Mmap::const_iterator mI = mmap.begin();
   for (unsigned int ctr = 0; ctr < mmap.size()+nentries; ++ctr)
   {
      if (ctr % col_size == 0)
      {
         switch (icol)
         {
            case 0:
               os << "<div class=\"col_l\">" << endl;
               os << "<div class=\"col_l\">" << endl;
               os << "<table summary=\"Function list = column 1\">" << endl;
               break;
            case 1:
               os << "</table>" << endl;
               os << "</div>" << endl;
               os << "<div class=\"col_r\">" << endl;
               os << "<table summary=\"Function list = column 2\">" << endl;
               break;
            case 2:
               os << "</table>" << endl;
               os << "</div>" << endl;
               os << "</div>" << endl;
               os << "<div class=\"col_r\">" << endl;
               os << "<div class=\"col_l\">" << endl;
               os << "<table summary=\"Function list = column 3\">" << endl;
               break;
            case 3:
               os << "</table>" << endl;
               os << "</div>" << endl;
               os << "<div class=\"col_r\">" << endl;
               os << "<table summary=\"Function list = column 4\">" << endl;
               break;
         }
         ++icol;
      }

      PRStatement f = (*mI).first;
      if (f[0] != actu)
      {
         actu = f[0];
         os << "<tr><th>"
            << "<span class=\"thl\"><a name=\"Entry_" << int(actu) << "\">" << actu << "</a></span>"
            << "<span class=\"thr\"><a href=\"#Top\">Top</a></span>"
            << "</th></tr>" << endl;
      }
      else
      {
         EntierN ind;
         bool trouve = false;
         if (f.len() >= tailleMin)
         {
            if (OK == lstTokens.trouveB(ind, Token(f), compareToken))
               trouve = (0 == compareToken(Token(f), lstTokens[ind]));
         }
         if (trouve)
         {
            PRStatement t = lstTokens[ind].secondP;
            os << "<tr><td><a href=\"" << repHTML << t << "\">" << encodeHTM(f) << "</a></td></tr>" << endl;
         }
         ++mI;
      }
   }
   if (icol > 0) os << "</table>" << endl << "</div>" << endl;
   os << "</div>" << endl;
   os << "</div>" << endl;
   os << "</td></tr>" << endl;   // Main table

   // ---  Menu avec alphabet
   os << "<tr><td>" << endl;     // Main table
   os << "<div class=\"menu\">" << endl;
   actu = 0x0;
   for (Mmap::const_iterator mI = mmap.begin(); mI != mmap.end(); ++mI)
   {
      PRStatement f = (*mI).first;
      if (f[0] != actu)
      {
         if (actu != 0x0) os << " | ";
         actu = f[0];
         ++nentries;
         os << "<a class=\"mnu_entry\" href=\"#Entry_" << int(actu) << "\">" << actu << "</a>" << endl;
      }
   }
   os << "</div>" << endl;
   os << "</td></tr>" << endl;   // Main table

   ecrisPiedHTML(os);
}

//==========================================================================
//==========================================================================
//       Ecris la liste des fichiers
//==========================================================================
//==========================================================================
void ecrisLstFi()
{
   std::ofstream os("WebFtn_Fi.htm");

   ecrisEnteteHTML(os, "WebFtn - Alphabetical list of files");

   // ---  Menu
   os << "<tr><td>" << endl;     // Main table
   os << "<a name=\"Top\"></a>" << endl;
   os << "<div class=\"menu\">"<< endl;
   char actu = 0x0;
   int  nmenu = 0;
   int  nfic  = 0;
   for (EntierN ind = 0; ind < lstTokens.dimension(); ++ind)
   {
      if (lstTokens[ind].type != Token::LINKFICSRC) continue;

      PRStatement f = lstTokens[ind].firstP;
      if (f[0] != actu)
      {
         if (actu != 0x0) os << " | ";
         actu = f[0];
         ++nmenu;
         os << "<a class=\"mnu_entry\" href=\"#Entry_" << int(actu) << "\">" << actu << "</a>" << endl;
      }
      ++nfic;
   }
   os << "</div>" << endl;
   os << "</td></tr>" << endl;   // Main table

   // ---  Liste
   const int col_size = int( double(nfic+nmenu) / 4.0 + 0.5);
   int ind  = -1;
   int icol = 0;
   actu = 0x0;
   os << "<tr><td>" << endl;     // Main table
   os << "<div class=\"main\">" << endl;
   for (int ctr = 0; ctr < (nfic+nmenu); ++ctr)
   {
      if (ctr % col_size == 0)
      {
         switch (icol)
         {
            case 0:
               os << "<div class=\"col_l\">" << endl;
               os << "<div class=\"col_l\">" << endl;
               os << "<table summary=\"Function list = column 1\">" << endl;
               break;
            case 1:
               os << "</table>" << endl;
               os << "</div>" << endl;
               os << "<div class=\"col_r\">" << endl;
               os << "<table summary=\"Function list = column 2\">" << endl;
               break;
            case 2:
               os << "</table>" << endl;
               os << "</div>" << endl;
               os << "</div>" << endl;
               os << "<div class=\"col_r\">" << endl;
               os << "<div class=\"col_l\">" << endl;
               os << "<table summary=\"Function list = column 3\">" << endl;
               break;
            case 3:
               os << "</table>" << endl;
               os << "</div>" << endl;
               os << "<div class=\"col_r\">" << endl;
               os << "<table summary=\"Function list = column 4\">" << endl;
               break;
         }
         ++icol;
      }

//      cerr << "Avant:" << lstTokens[ind+1].firstP << "  " << (ind+1) << " " << lstTokens.dimension() << endl;
      while (lstTokens[++ind].type != Token::LINKFICSRC) ; // cerr << ind << " " << lstTokens[ind].firstP << endl;
//      cerr << "Apres:" << lstTokens[ind].firstP << "  " << (ind) << " " << lstTokens.dimension() << endl;
      PRStatement f = lstTokens[ind].firstP;
      if (f[0] != actu)
      {
         actu = f[0];
         os << "<tr><th>"
            << "<span class=\"thl\"><a name=\"Entry_" << int(actu) << "\">" << actu << "</a></span>"
            << "<span class=\"thr\"><a href=\"#Top\">Top</a></span>"
            << "</th></tr>" << endl;
         --ind;
      }
      else
      {
         PRStatement l = PRStatement(lstTokens[ind].secondP);
         l.subst(".", "_");
         l += ".htm";
         os << "<tr><td><a href=\"" << repHTML << l << "\">" << encodeHTM(f) << "</a></td></tr>" << endl;
      }
   }
   if (icol > 0) os << "</table>" << endl << "</div>" << endl;
   os << "</div>" << endl;
   os << "</div>" << endl;
   os << "<tr><td>" << endl;     // Main table


   // ---  Menu
   os << "<tr><td>" << endl;     // Main table
   os << "<div class=\"menu\">" << endl;
   actu = 0x0;
   for (EntierN ind = 0; ind < lstTokens.dimension(); ++ind)
   {
      if (lstTokens[ind].type != Token::LINKFICSRC) continue;

      PRStatement f = lstTokens[ind].firstP;
      if (f[0] != actu)
      {
         if (actu != 0x0) os << " | ";
         actu = f[0];
         ++nmenu;
         os << "<a class=\"mnu_entry\" href=\"#Entry_" << int(actu) << "\">" << actu << "</a>" << endl;
      }
      ++nfic;
   }
   os << "</div>" << endl;
   os << "</td></tr>" << endl;   // Main table

   ecrisPiedHTML(os);
}

//==========================================================================
//==========================================================================
//       Ecris la liste arborescente des composantes
//==========================================================================
//==========================================================================
void ecrisLstCo()
{
   std::ofstream os("WebFtn_Co.htm");

   ecrisEnteteHTML(os, "WebFtn - Components list");

   os << "<tr><td>" << endl;     // Main table
   os << "<!-- tree -->" << endl;
   os << "<a href=\"#\" onClick=\"expandTree(\'tree1\'); return false;\"><img src=\"plus.gif\" alt=\"Expand all\"></a>" << endl;
   os << "<a href=\"#\" onClick=\"collapseTree(\'tree1\'); return false;\"><img src=\"minus.gif\" alt=\"Collapse all\"></a>" << endl;
   os << endl;
   os << "<ul class=\"mktree\" id=\"tree1\">" << endl;

   // ---  Arbre 
   PRStatement grpActu;
   PRStatement clsActu;
   for (Mmap::const_iterator mI = mmap.begin(); mI != mmap.end(); ++mI)
   {
      PRStatement f = (*mI).first;
      PRStatement grp = left(f, "_");
      PRStatement cls = left(mid(f, "_"), "_");
      if (grp != "" && grp != grpActu && right(f, 4) == "_000")
      {
         if (grpActu != "")
         {
            os << "\t\t\t</ul>" << endl;  // Classe
            os << "\t\t</li>" << endl;    // Classe
            os << "\t\t</ul>" << endl;    // Groupe
            os << "\t</li>" << endl;      // Groupe
         }
         grpActu = grp;
         clsActu = cls;
         os << "\t<li class=\"liClosed\">"   << grp << endl;
         os << "\t\t<ul>" << endl;
         os << "\t\t<li class=\"liClosed\">" << cls << endl;
         os << "\t\t\t<ul>" << endl;
      }
      else if (cls != "" && cls != clsActu && right(f, 4) == "_000")
      {
         os << "\t\t\t</ul>" << endl;
         os << "\t\t</li>" << endl;
         clsActu = cls;
         os << "\t\t<li class=\"liClosed\">" << cls << endl;
         os << "\t\t\t<ul>" << endl;
      }
      if (grp != grpActu) continue;
      if (cls != clsActu) continue;

      EntierN ind;
      bool trouve = false;
      if (f.len() >= tailleMin)
      {
         if (OK == lstTokens.trouveB(ind, Token(f), compareToken))
            trouve = (0 == compareToken(Token(f), lstTokens[ind]));
      }
      if (trouve)
      {
         PRStatement t = lstTokens[ind].secondP;
         os << "\t\t\t<li><a href=\"" << repHTML << t << "\">" << encodeHTM(f) << "</a></li>" << endl;
      }
   }
   os << "\t\t\t</ul>" << endl;  // Classe
   os << "\t\t</li>" << endl;    // Classe
   os << "\t\t</ul>" << endl;    // Groupe
   os << "\t</li>" << endl;      // Groupe

   os << "</ul>" << endl;        // Arbre

   declStack.push("mktree");
   os << "</td></tr>" << endl;   // Main table
   
   ecrisPiedHTML(os);
}

//==========================================================================
//==========================================================================
//       Ajoute les mots clefs à la liste
//==========================================================================
//==========================================================================
void ajouteMotsClefs()
{

   lstTokens.ajoute(Token(STRDUP("block"), Token::FUNC_BGN));
   lstTokens.ajoute(Token(STRDUP("function"), Token::FUNC_BGN));
   lstTokens.ajoute(Token(STRDUP("program"), Token::FUNC_BGN));
   lstTokens.ajoute(Token(STRDUP("subroutine"), Token::FUNC_BGN));

   lstTokens.ajoute(Token(STRDUP("endblock"), Token::FUNC_END));
   lstTokens.ajoute(Token(STRDUP("return"), Token::FUNC_END));

   lstTokens.ajoute(Token(STRDUP("include"), Token::INCLUDE));

   lstTokens.ajoute(Token(STRDUP("allocatable"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("allocate"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("assign"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("backspace"), Token::KEYWORD));
//   lstTokens.ajoute(Token(STRDUP("block"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("call"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("case"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("character"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("close"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("common"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("complex"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("contains"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("continue"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("cycle"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("data"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("deallocate"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("dimension"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("do"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("double *precision"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("elemental"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("else"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("elseif"), Token::KEYWORD));
//   lstTokens.ajoute(Token(STRDUP("endblock"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("endblockdata"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("enddo"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("endfile"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("endif"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("endselect"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("endtype"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("endwhere"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("end"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("endfile"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("entry"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("equivalence"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("exit"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("external"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("forall"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("format"), Token::KEYWORD));
//   lstTokens.ajoute(Token(STRDUP("function"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("goto"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("if"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("implicit"), Token::KEYWORD));
//   lstTokens.ajoute(Token(STRDUP("include"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("inquire"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("integer"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("intent"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("intrinsic"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("kind"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("logical"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("namelist"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("none"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("nullify"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("only"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("open"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("optional"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("parameter"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("pause"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("pointer"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("print"), Token::KEYWORD));
//   lstTokens.ajoute(Token(STRDUP("program"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("pure"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("read"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("real"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("recursive"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("result"), Token::KEYWORD));
//   lstTokens.ajoute(Token(STRDUP("return"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("rewind"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("save"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("select *case"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("sequence"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("stop"), Token::KEYWORD));
//   lstTokens.ajoute(Token(STRDUP("subroutine"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("target"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("then"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("to"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("type"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("use"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("where"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("while"), Token::KEYWORD));
   lstTokens.ajoute(Token(STRDUP("write"), Token::KEYWORD));

   lstTokens.ajoute(Token(STRDUP("abs"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("acos"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("adjustl"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("adjustr"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("advance"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("aimag"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("all"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("allocated"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("alog10"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("amax0"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("amax1"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("amin0"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("amin1"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("amod"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("anint"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("any"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("asin"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("associated,"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("atan"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("atan2"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("bit_size"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("blank"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("btest"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("cabs"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("ccos"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("ceiling"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("cexp"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("char"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("clog"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("cmplx"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("conjg"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("cos"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("cosh"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("cshift"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("csin"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("csqrt"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dabs"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dacos"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dasin"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("datan"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("datan2"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("date_and_time"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dble"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dcos"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dcosh"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("ddim"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dexp"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dim"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dint"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dlog"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dlog10"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dmax0"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dmax1"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dmin0"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dmin1"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dmod"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dot_product"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dprod"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dsign"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dsin"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dsqrt"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dtan"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("dtanh"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("eoshift"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("epsilon"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("exp"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("exponent"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("float"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("floor"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("fmt"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("fraction"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("huge"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("iachar"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("iand"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("ichar"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("idim"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("idint"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("ieor"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("ifix"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("index"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("int"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("ior"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("ishft"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("ishftc"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("isign"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("lbound"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("len"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("len_trim"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("lge"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("lgt"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("lle"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("llt"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("log"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("log10"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("matmul"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("max"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("max0"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("max1"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("maxexponent"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("maxloc"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("maxval"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("merge"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("min"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("min0"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("min1"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("minexponent"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("minloc"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("minval"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("mod"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("modulo"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("mvbits"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("nearest"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("nint"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("not"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("pack"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("precision"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("present"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("product"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("random_number"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("random_seed"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("range"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("real"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("repeat"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("reshape"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("rrspacing"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("scale"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("scan"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("selected_int_kind"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("selected_real_kind"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("set_exponent"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("shape"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("sign"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("sin"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("size"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("sngl"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("spacing"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("spread"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("sqrt"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("sum"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("system_clock"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("tan"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("tanh"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("tiny"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("transfer"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("transpose"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("trim"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("ubound"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("unpack"), Token::INTRINSIC));
   lstTokens.ajoute(Token(STRDUP("verify"), Token::INTRINSIC));

   tailleMin = std_min(tailleMin, 2L);
}


//==========================================================================
//==========================================================================
//       Function qui lis le fichier de configuration
//==========================================================================
//==========================================================================
void lisFichierConf(const PRStatement& nomIn)
{

   if (nomIn == "")
      return;

   char ligne[1024];

   std::ifstream fichierIn;

   fichierIn.open(nomIn);
   if (! fichierIn) return;

   while (fichierIn && !fichierIn.eof())
   {
      fichierIn.getline(ligne, 1024);
      if (lcase(ligne) == PRStatement("[types]"))
         break;
   }
   while (fichierIn && !fichierIn.eof())
   {
      fichierIn.getline(ligne, 1024);
      PRStatement var = lcase(ltrim(rtrim(left(ligne, "="))));
      PRStatement val = ltrim(rtrim(mid (ligne, "=")));
      if (var != "type") continue;
      if (val == "") continue;

      lstTokens.ajoute(Token(val.dupAsCarP(), Token::USER));
   }
   fichierIn.close();
}

//==========================================================================
//==========================================================================
//       Lis liste de types
//==========================================================================
//==========================================================================
void lisFichierTypes(const PRStatement& nomFichier)
{
   // ---  Lis la liste liens
   if (nomFichier == "")
      return;

   PRifstream fichier(nomFichier);
   if (! fichier) return;

   bool done = false;
   PRStatement statement;
   PRStatement nomReference;
   PRStatement nomLien;
   while (! done)
   {
      if (! statement.readLine(fichier)) break;
      if (statement == "") continue;

      char code = statement[0];
      statement.mid(2);
      if (code == 'f')
      {
         nomReference = left(statement, "(");
         if (nomReference != "")
         {
            nomLien = traduisANomFichier(nomReference);
            lstTokens.ajoute(Token(nomReference.dupAsCarP(),
                                   nomLien.dupAsCarP(),
                                   Token::LINKWEBPPFUNC));
            tailleMin = std_min(tailleMin, nomReference.len());
         }
      }
   }

}

//==========================================================================
//==========================================================================
//       Traduis les liens et autres mots clefs
//==========================================================================
//==========================================================================
PRStatement traduisLiens(Contexte& cntx,
                         const PRStatement& token,
                         bool onlyLink = false)
{

   if (cntx.inDeclaration && token != "")
   {
      cntx.fncActu = token;
      cntx.inDeclaration = false;
   }

   EntierN ind;
   bool trouve = false;
   if (token.len() >= tailleMin)
   {
      if (OK == lstTokens.trouveB(ind, Token(token), compareToken))
          trouve = (0 == compareToken(Token(token), lstTokens[ind]));
   }

   PRStatement lineOut;
   if (trouve)
   {
      switch (lstTokens[ind].type)
      {
         case Token::LINKWEBPPFUNC:
         {
            if (prochainCharNonBlanc(cntx.cI, cntx.fI) == '(')
            {
               lineOut = "<a href=\""
                       + repHTML + PRStatement(lstTokens[ind].secondP)
                       + "\">"
                       + encodeHTM(token) + "</a>";
            }
            else
            {
               lineOut = encodeHTM(token);
            }
            break;
         }
         case Token::LINKFICSRC:
         {
            PRStatement fichierHTML = PRStatement(lstTokens[ind].firstP);
            fichierHTML.subst(".", "_");
            fichierHTML += ".htm";
            lineOut = "<a href=\""
                       + repHTML + fichierHTML
                       + "\">"
                       + encodeHTM(token) + "</a>";
            break;
         }
         case Token::LINKFUNC:
         {
            if (! cntx.ftnCommentOn && prochainCharNonBlanc(cntx.cI, cntx.fI) == '(')
            {
               bool estDeclaration = (cntx.fncActu == lstTokens[ind].firstP);
               if (estDeclaration) // Déclaration de la fonction
               {
                  PRStatement rht = lstTokens[ind].firstP;
                  mmap[rht].insert("");   // Fait exister la fonction
                  declStack.push(rht);

                  lineOut = PRStatement("<a class=\"mnu\" ")
                          +    "href=\"javascript:void(0);\" "
                          +    "onmouseover=\"this.T_STICKY=true;this.T_OFFSETY=12;return escape(" + rht + "())\""
                          + ">" 
                          + encodeHTM(token) + "</a>";
               }
               else                 // Appel de la fonction
               {
                  char numero[8];
                  std::sprintf(numero, "%li", cntx.ligne);
                  PRStatement rht = lstTokens[ind].firstP;
                  PRStatement lft = cntx.fncActu + "@" + cntx.nomOu + "#L" + numero;
                  mmap[rht].insert(lft);

                  lineOut = "<a href=\""
                          + repHTML + PRStatement(lstTokens[ind].secondP)
                          + "\">"
                          + encodeHTM(token) + "</a>";
               }
            }
            else                 // Autre ref au symbole
            {
               lineOut = "<a href=\""
                        + repHTML + PRStatement(lstTokens[ind].secondP)
                        + "\">"
                        + encodeHTM(token) + "</a>";
            }
            break;
         }
         case Token::KEYWORD:
         {
            if (! onlyLink)
               lineOut = fontKeywordP + encodeHTM(token) + endFont;
            else
               lineOut = encodeHTM(token);
            break;
         }
         case Token::FUNC_BGN:
         {
            cntx.inDeclaration = true;
            if (! onlyLink)
            {
               lineOut = fontKeywordP + encodeHTM(token) + endFont;
            }
            else
               lineOut = encodeHTM(token);
            break;
         }
         case Token::FUNC_END:
         {
            cntx.inDeclaration = false;
            cntx.fncActu = ""; 
            if (! onlyLink)
            {
               lineOut = fontKeywordP + encodeHTM(token) + endFont;
            }
            else
               lineOut = encodeHTM(token);
            break;
         }
         case Token::INCLUDE:
         {
            cntx.inInclude = true;
            if (! onlyLink)
            {
               lineOut = fontKeywordP + encodeHTM(token) + endFont;
            }
            else
               lineOut = encodeHTM(token);
            break;
         }
         case Token::USER:
         {
            if (! onlyLink)
               lineOut = fontUserP + encodeHTM(token) + endFont;
            else
               lineOut = encodeHTM(token);
            break;
         }
         default:
         {
            lineOut = encodeHTM(token);
            break;
         }
      }
   }
   else
   {
      lineOut = encodeHTM(token);
   }

   return lineOut;
}


//==========================================================================
//==========================================================================
//       Fonction qui scane le code
//==========================================================================
//==========================================================================
void scanFtn(Contexte& cntx)
{
   PRStatement chunk;
   PRStatement lineIn;
   PRStatement lineOut;
   char numero[64];
   char c[2] = "";
   char lastChar = '\n';
   bool writeLine      = false;

   // ---  Écris l'entête du fichier HTML
   ecrisEnteteHTML(cntx.ficOu, "WebFtn - " + cntx.nomIn);

   // ---  Boucle sur le fichier
   cntx.ficOu << "<tr><td>" << endl;     // Main table
   cntx.ficOu << "<pre>" << endl;
   while (cntx.cI != cntx.fI)
   {
      c[0] = *(cntx.cI);   // peek()

      switch (c[0])
      {
         case (39):  // '
            if (cntx.ftnCommentOn)
            {
               chunk += c;
            }
            else if (cntx.ftnDebugOn)
            {
               chunk += c;
            }
            else if (cntx.preprocOn)
            {
               lineOut += traduisLiens(cntx, chunk, true);
               lineOut += encodeHTM(c);
               chunk = "";
               cntx.singleQuotesOn = !cntx.singleQuotesOn;
            }
            else if (cntx.inInclude)
            {
               if (cntx.singleQuotesOn)
               {
                  lineOut += traduisLiens(cntx, chunk, true);
                  lineOut += encodeHTM(c);
                  chunk = "";
                  cntx.singleQuotesOn = false;
                  cntx.inInclude = false;
               }
               else
               {
                  lineOut += traduisLiens(cntx, chunk);
                  lineOut += encodeHTM(c);
                  chunk = "";
                  cntx.singleQuotesOn = true;
               }
            }
            else if (cntx.singleQuotesOn)
            {
               lineOut += encodeHTM(chunk+c) + endFont;
               chunk = "";
               cntx.singleQuotesOn = false;
            }
            else
            {
               lineOut += traduisLiens(cntx, chunk);
               lineOut += fontStringP;
               chunk = c;
               cntx.singleQuotesOn = true;
            }
            break;
         case (33):  // !
            if (cntx.singleQuotesOn)
            {
               chunk += c;
            }
            else
            {
               if (! cntx.ftnCommentOn)
               {
                  if (cntx.preprocOn)
                  {
                     lineOut += traduisLiens(cntx, chunk, false) + endFont;
                     cntx.preprocOn = false;
                     chunk = "";
                  }
                  else if (cntx.ftnDebugOn)
                  {
                     lineOut += traduisLiens(cntx, chunk, false) + endFont;
                     cntx.ftnDebugOn = false;
                     chunk = "";
                  }
                  lineOut += fontCommentP;
                  chunk += c;
                  cntx.ftnCommentOn = true;
               }
               else if (cntx.preprocOn)
               {
                  chunk += c;
               }
               else
               {
                  lineOut += traduisLiens(cntx, chunk);
                  chunk = c;
               }
            }
            break;
         case '\n':
            if (cntx.singleQuotesOn)
            {
               lineOut += traduisLiens(cntx, chunk, true) + endFont;
               cntx.preprocOn    = false;
            }
            else if (cntx.ftnCommentOn)
            {
               lineOut += traduisLiens(cntx, chunk, true) + endFont;
               cntx.ftnCommentOn = false;
               cntx.ftnDebugOn   = false;
               cntx.preprocOn    = false;
            }
            else if (cntx.ftnDebugOn)
            {
               lineOut += traduisLiens(cntx, chunk, true) + endFont;
               cntx.ftnCommentOn = false;
               cntx.ftnDebugOn   = false;
               cntx.preprocOn    = false;
            }
            else if (cntx.preprocOn)
            {
               lineOut += traduisLiens(cntx, chunk, true) + endFont;
               cntx.ftnCommentOn = false;
               cntx.ftnDebugOn   = false;
               cntx.preprocOn    = false;
            }
            else
            {
               lineOut += traduisLiens(cntx, chunk);
               cntx.preprocOn    = false;
            }
            chunk = "";
            writeLine = true;
            break;
         case 'c':
         case 'C':
            if (lastChar == '\n')
            {
               lineOut += fontCommentP;
               chunk += c;
               cntx.ftnCommentOn = true;
            }
            else
            {
               chunk += c;
            }
            break;
         case 'd':
         case 'D':
            if (lastChar == '\n')
            {
               lineOut += fontDebugP;
               chunk += c;
               cntx.ftnDebugOn = true;
            }
            else
            {
               chunk += c;
            }
            break;
         case ' ':
         case '\t':
         case ',':
         case '*':
         case '=':
         case '(':
         case ')':
            if (cntx.ftnCommentOn)
            {
               lineOut+= traduisLiens(cntx, chunk, true);
               lineOut+= encodeHTM(c);
               chunk = "";
            }
            else if (cntx.ftnDebugOn)
            {
               lineOut+= traduisLiens(cntx, chunk, true);
               lineOut+= encodeHTM(c);
               chunk = "";
            }
            else if (cntx.preprocOn)
            {
               lineOut+= traduisLiens(cntx, chunk, true);
               lineOut+= encodeHTM(c);
               chunk = "";
            }
            else
            {
               lineOut+= traduisLiens(cntx, chunk);
               lineOut+= encodeHTM(c);
               chunk = "";
            }
            break;
         default:
            chunk += c;
            break;
      }

      if (writeLine)
      {
         lineOut += encodeHTM(chunk);
         std::sprintf(numero, "<a name=\"L%li\" id=\"L%li\">%4li</a>  ", cntx.ligne, cntx.ligne, cntx.ligne);
         cntx.ficOu << numero << lineOut << endl;
         if (cntx.preprocOn)
            lineOut = fontPreprocP;
         else
            lineOut= "";
         lineIn = "";
         chunk  = "";
         ++cntx.ligne;
         writeLine = false;
      }

      if (cntx.cI != cntx.fI)
      {
         lastChar = *(cntx.cI);
         ++cntx.cI;
      }
   }
   cntx.ficOu << "</pre>" << endl;
   cntx.ficOu << "</td></tr>" << endl;   // Main table

   // ---  Ecris le pied
   ecrisPiedHTML(cntx.ficOu);
}

//==========================================================================
//==========================================================================
//       Scan le fichier pour ajouter les fonctions
//==========================================================================
//==========================================================================
void ajouteFonctions(const PRStatement& nomIn, const PRStatement& nomOu)
{
   PRifstream fichierIn(nomIn);

   // ---  Parse le fichier pour extraire les fonctions
   char buf[1024];
   int  noLigne = 0;
   while (true)
   {
      fichierIn.getline(&buf[0], 1023);
      if (! fichierIn) break;
      ++noLigne;

      PRStatement stat = buf;
      if (stat == "") continue;
      if (stat[0] != ' ') continue;
      if (stat[5] != ' ') continue;

      bool trouve = false;
      stat.trim();
      if (lcase(left(stat, 8)) == "program ")
      {
         stat.mid(8);
         trouve = true;
      }
      else if (lcase(left(stat, 9)) == "function ")
      {
         stat.mid(9);
         trouve = true;
      }
      else if (lcase(left(stat, 11)) == "subroutine ")
      {
         stat.mid(11);
         trouve = true;
      }

      if (trouve)
      {
         stat.trim();
         stat.left("(");
         PRStatement nom = stat.trim();

         char numero[8];
         std::sprintf(numero, "%u", noLigne);
         PRStatement lnk = nomOu + "#L" + numero;

         lstTokens.ajoute(Token(nom.dupAsCarP(),
                                lnk.dupAsCarP(),
                                Token::LINKFUNC));
      }
   }
   fichierIn.close();

}

//==========================================================================
//==========================================================================
//       Traduis le fichier en format Dos
//==========================================================================
//==========================================================================
void unix2Dos(const PRStatement& nomOu, const PRStatement& nomIn)
{
   std::FILE* inp;
   std::FILE* out;

	if ((inp = std::fopen (nomIn, "rb")) == (std::FILE *) 0)
		return;
	if ((out = std::fopen (nomOu, "wb")) == (std::FILE *) 0)
	{
		std::fclose(inp);
		return;
	}

   int ch;
	while ((ch = getc(inp)) != EOF)
   {
      switch (ch)
      {
         case '\015':
         break;
         case '\012':
            putc('\015', out);
            putc('\012', out);
         break;
         default:
            putc(ch, out);
      }
   }

   std::fclose(inp);
   std::fclose(out);
   return;
}

//==========================================================================
//==========================================================================
//       Ajoute un groupe de fichiers avec wildcards comme token connus
//==========================================================================
//==========================================================================
void ajouteGroupeFichiers(const PRStatement& pathComplet)
{
   // ---  Cherche le répertoire dans le nom
   QBString path;
   QBString fic;
   if (instr(pathComplet, "\\") >= 0)
   {
      fic = mid (pathComplet, pathComplet.rinstr("\\")+1);
      path =left(pathComplet, pathComplet.rinstr("\\"));
   }
   else if (instr(pathComplet, "/") >= 0)
   {
      fic = mid (pathComplet, pathComplet.rinstr("/")+1);
      path =left(pathComplet, pathComplet.rinstr("/"));
   }
   else
   {
      fic = pathComplet;
      path = "";
   }

   // ---  Parse chaque fichier
   DirectoryIterator iter = DirectoryIterator(ConstCarP(path), ConstCarP(fic));
   DirectoryIterator iend = DirectoryIterator();
   for ( ; iter != iend; ++iter)
   {
      PRStatement pathIn = iter->path().c_str();
      PRStatement nomIn  = iter->path().filename().c_str();

      lstTokens.ajoute(Token(nomIn.dupAsCarP(), Token::LINKFICSRC));

      // ---  Génère le nom du fichier de sortie
      PRStatement nomOu = nomIn;
      if (nomOu == "") nomOu = pathIn;
      nomOu.subst(".", "_");
      nomOu += PRStatement(".htm");

      ajouteFonctions(pathIn, nomOu);
   }

   return;
}

//==========================================================================
//==========================================================================
//       Traite un groupe de fichiers avec wildcards
//==========================================================================
//==========================================================================
void traiteGroupeFichiers(const PRStatement& pathComplet)
{
   // ---  Cherche le repertoire dans le nom
   QBString path;
   QBString fic;
   if (instr(pathComplet, "\\") >= 0)
   {
      fic = mid (pathComplet, pathComplet.rinstr("\\")+1);
      path =left(pathComplet, pathComplet.rinstr("\\"));
   }
   else if (instr(pathComplet, "/") >= 0)
   {
      fic = mid (pathComplet, pathComplet.rinstr("/")+1);
      path =left(pathComplet, pathComplet.rinstr("/"));
   }
   else
   {
      fic = pathComplet;
      path = "";
   }

   // ---  Parse chaque fichier
   DirectoryIterator iter = DirectoryIterator(ConstCarP(path), ConstCarP(fic));
   DirectoryIterator iend = DirectoryIterator();
   if (iter == iend)
   {
      cout << "Erreur: fichier " << pathComplet << " introuvable" << endl;
      cerr << "Erreur: fichier " << pathComplet << " introuvable" << endl;
   }
   else
   {
      for ( ; iter != iend; ++iter)
      {
         PRStatement pathIn = iter->path().c_str();
         PRStatement nomIn  = iter->path().filename().c_str();
         cerr << "Traite : " << pathIn << endl;

         // ---  Unix2DOS
         PRNomFichierTemporaire ficUnix2Dos;
         PRStatement nomTm = ficUnix2Dos;
         unix2Dos(ficUnix2Dos, pathIn);

         // ---  Lis le fichier en mémoire
         std::ifstream fichierIn(nomTm, std::ios::in | std::ios::ate);
         std::streampos sz = fichierIn.tellg();
         fichierIn.seekg(0, std::ios::beg);
         Contexte::Buffer bufIn(sz);
         fichierIn.read(&bufIn[0], sz);
         fichierIn.close();

         // ---  Génère le nom du fichier de sortie
         PRStatement nomOu = nomIn;
         nomOu.subst(".", "_");
         nomOu += PRStatement(".htm");

         // ---  Scanne
         Contexte contexte;
         contexte.nomIn = nomIn;
         contexte.nomOu = nomOu;
         contexte.cI = bufIn.begin();
         contexte.fI = bufIn.end();
         contexte.ligne = 1;
         contexte.ficOu.open(nomOu);
         scanFtn(contexte);
         contexte.ficOu.close();
      }
   }

   return;
}

//==========================================================================
//==========================================================================
//       main
//==========================================================================
//==========================================================================
int main (int argc, char* argv[])
{
#ifdef CONTROLE_MEMOIRE
   ERR__init(argv[0]);
//   MEM__niveauTest = MEM__NEW_TOUS | MEM__DELETE_TOUS;
   MEM__memoireEtat();
#endif // ifdef CONTROLE_MEMOIRE

   ERMsg msg = ERMsg::OK;

   PRStatement lstFileName;
   PRStatement cnfFileName;

   // ---  Interpret command line arguments
   for (int i = 1; i < argc && msg; ++i)
   {
      if (argv[i][0] != '-') continue;

      switch (argv[i][1])
      {
         case 'c':
         case 'C':
            if (argv[i][2] == '=' && argv[i][3] != 0x0)
               cnfFileName = &argv[i][3];
            else
               msg = ERMsg(ERMsg::ERREUR, "ERR_PARAMETRE_ATTENDU");
            break;
         case 'i':
         case 'I':
            if (argv[i][2] == '=' && argv[i][3] != 0x0)
               lisFichierTypes(&argv[i][3]);
            else
               msg = ERMsg(ERMsg::ERREUR, "ERR_PARAMETRE_ATTENDU");
            break;
         case 'p':
         case 'P':
            if (argv[i][2] == '=' && argv[i][3] != 0x0)
            {
            	urlMain = &argv[i][3];
            }
            else
               msg = ERMsg(ERMsg::ERREUR, "ERR_PARAMETRE_ATTENDU");
            break;
         case 'r':
         case 'R':
            if (argv[i][2] == '=' && argv[i][3] != 0x0)
            {
            	repHTML = &argv[i][3];
               repHTML.subst("\\", "/");
               repHTML = "/" + repHTML + "/";
               repHTML.subst("//", "/");
            }
            else
               msg = ERMsg(ERMsg::ERREUR, "ERR_PARAMETRE_ATTENDU");
            break;
         default :
            msg = ERMsg(ERMsg::ERREUR, "ERR_OPTION_INCONNUE");
            break;
      }
   }
   if (argc < 2 || ! msg)
   {
      cerr << endl;
      cerr << WBCopyright();
      cerr << endl;

      cerr << traduisAOEM("Usage : TrfFtn [Options] fichiers") << endl;
      cerr << endl;
      cerr << traduisAOEM("fichiers:  noms de fichiers C++.") << endl;
      cerr << traduisAOEM("           Wild cards acceptes.") << endl;
      cerr << endl;
      cerr << traduisAOEM("OPTION          DESCRIPTION") << endl;
      cerr << traduisAOEM("---------------------------") << endl;
      cerr << traduisAOEM(" -c=nomFichier       Fichier de configuration") << endl;
      cerr << traduisAOEM(" -h                  Affiche ce message") << endl;
      cerr << traduisAOEM(" -i=nomFichier       Importe les types du fichier") << endl;
      cerr << traduisAOEM(" -p=pagePrincipale   URL de la page principale") << endl;
      cerr << traduisAOEM(" -r=repertoire       Répertoire HTML des fichiers") << endl;
      cerr << endl;
      cerr << traduisAOEM("EXEMPLE:  TrfFtn -i=foo.lst fp*.for fp*.fi ..\\sources\\fp*.ftn") << endl;
      cerr << endl;
      return -1;
   }

   // ---  Initialise la liste des mots clefs
   ajouteMotsClefs();

   // ---  Lis le fichier de configuration
   if (cnfFileName != "")
   {
      lisFichierConf(cnfFileName);
   }

   // ---  Ajoute les noms des fichiers comme token
   for (int i = 1; i < argc; ++i)
   {
      if (argv[i][0] == '-') continue;

      PRStatement nom = argv[i];
      if (nom[0] == '@')      // Response file
      {
         nom.mid(1);
         std::ifstream is(nom);
         while (is && !is.eof())
         {
            is >> nom;
            nom.trim();
            if (nom != "" && nom[0] != '#')
               ajouteGroupeFichiers(nom);
         }
      }
      else
      {
         ajouteGroupeFichiers(nom);
      }
   }
   if (lstTokens.dimension() > 0)
      lstTokens.trie(compareToken);

   // ---  Traite tous les fichiers
   for (int i = 1; i < argc; ++i)
   {
      if (argv[i][0] == '-') continue;

      PRStatement nom = argv[i];
      if (nom[0] == '@')      // Response file
      {
         nom.mid(1);
         std::ifstream is(nom);
         while (is && !is.eof())
         {
            is >> nom;
            nom.trim();
            if (nom != "" && nom[0] != '#')
               traiteGroupeFichiers(nom);
         }
      }
      else
      {
         traiteGroupeFichiers(nom);
      }
   }

   ecrisLstAppelants();
   ecrisLstAl();
   ecrisLstFi();
   ecrisLstCo();

   cerr << "Done" << endl;

   mmap.erase(mmap.begin(), mmap.end());


#ifdef CONTROLE_MEMOIRE
   MEM__memoireControle();
   ERR__vide();
#endif // ifdef CONTROLE_MEMOIRE
   return 0;
}
