//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: tpvector.hpp
// Classe:  TPVecteur<T>
//************************************************************************

#ifndef TPVECTOR_HPP_DEJA_INCLU
#define TPVECTOR_HPP_DEJA_INCLU

#include <string.h>

template <typename T> const Entier TPVecteur<T>::DIM_BLOC = 16;
template <typename T> const Entier TPVecteur<T>::NBR_BLOC = 16;

//**************************************************************
// Description: Constructeur par défaut.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
TPVecteur<T>::TPVecteur (const EntierN dim)
{
#ifdef MODE_DEBUG
   PRECONDITION (dim >= 2);
#endif   // ifdef MODE_DEBUG

   // ---  Cherche l'exposant (2^exposant >= dim)
   EntierN ind = 1;
   exposant = 0;
   while (ind < dim)
   {
      ind = ind << 1;
      exposant ++;
   }
   ASSERTION (exposant > 0);

   // ---  Dimension d'un bloc : puissance de deux
   dimBloc = 0x1 << exposant;
   masque  = dimBloc - 1;

   // ---  Crée le vecteur de blocs et le premier bloc
   nbrBlocs = NBR_BLOC;
   blocsV   = new TP[nbrBlocs];
   blocsV[0]= new T[dimBloc];

   // ---  Ajuste les indices
   indBloc =  0;
   indVect = -1;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}  // template <typename T> TPVecteur<T>::TPVecteur (const EntierN dim)


//**************************************************************
// Description: Copie Constructeur.
//
// Entrée:
//    const TPVecteur& t      : Vecteur à copier
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
TPVecteur<T>::TPVecteur (const TPVecteur& t)
{
#ifdef MODE_DEBUG
   t.invariant("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Copie les attributs
   exposant = t.exposant;
   masque   = t.masque;
   dimBloc  = t.dimBloc;
   nbrBlocs = t.nbrBlocs;
   indBloc  = t.indBloc;
   indVect  = t.indVect;

   // ---  Dimensionne le vecteur de blocs
   blocsV = new TP[nbrBlocs];

   // ---  Crée les blocs
   for (Entier i = 0; i <= indBloc; i++)
      blocsV[i]= new T [dimBloc];

   // ---  Copie
   (*this) = t;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}  // template <typename T> TPVecteur<T>::TPVecteur (const TPVecteur& t)


//**************************************************************
// Description:
//    Destructeur. Désalloue les blocs puis le vecteur de blocs
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
TPVecteur<T>::~TPVecteur ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Détruis tous les blocs sauf le premier
   for (Entier i = 0; i <= indBloc; i++)
      delete[] blocsV[i];
   indBloc = 0;

   // ---  Détruis le vecteur de blocs
   delete[] blocsV;
   blocsV = NUL;

}  // template <typename T> TPVecteur<T>::~TPVecteur ()


//**************************************************************
// Description:
//    Ajoute un élément à la fin du vecteur. Au besoin, le vecteur est allongé.
//
// Entrée:
//    const T& item  : Elément à ajouter au vecteur
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
inline Entier TPVecteur<T>::ajoute (const T& item)
{
#ifdef MODE_DEBUG
   EntierN dimPre = dimension();
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Ajuste l'indice
   indVect ++;
   if (indVect == dimBloc)
   {
      indVect = 0;
      ajouteBloc();
   }

   // ---  Ajoute
   blocsV[indBloc][indVect] = item;

#ifdef MODE_DEBUG
   EntierN dimPst = dimension();
   POSTCONDITION ((dimPst-dimPre) == 1);
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return OK;
}  // template <typename T> inline Entier TPVecteur<T>::ajoute (const T& item)


//**************************************************************
// Description:
//    Méthode protégée; Ajoute un bloc à la liste des blocs.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
Entier TPVecteur<T>::ajouteBloc ()
{
#ifdef MODE_DEBUG
   EntierN indBlocPre = indBloc;
   PRECONDITION (indVect == 0);
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Contrôle le vecteur de blocs
   indBloc ++;
   if (indBloc == nbrBlocs)
   {
      nbrBlocs = nbrBlocs + NBR_BLOC;
      TP* tempV = new TP[nbrBlocs];
      memcpy (tempV, blocsV, indBloc*sizeof(TP));
      delete[] blocsV;
      blocsV = tempV;
   }

   // ---  Ajoute le bloc
   blocsV[indBloc] = new T[dimBloc];

#ifdef MODE_DEBUG
   POSTCONDITION ((indBloc-indBlocPre) == 1);
   POSTCONDITION (blocsV[indBloc] != NUL);
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return OK;
}  // template <typename T> Entier TPVecteur<T>::ajouteBloc ()


//**************************************************************
// Description:
//    Retourne la dimension du vecteur.
//
// Entrée:
//
// Sortie:
//    EntierN& dim
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
inline EntierN TPVecteur<T>::dimension () const
{
#ifdef MODE_DEBUG
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   EntierN dim = indBloc*dimBloc + indVect + 1;

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return dim;
}  // template <typename T> inline EntierN TPVecteur<T>::dimension


//**************************************************************
// Description:
//    Enlève l'élément à l'indice donné. Le vecteur est compacté.
//
// Entrée:
//    const EntierN ind      : Indice à enlever
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
Entier TPVecteur<T>::enleve (const EntierN ind)
{
#ifdef MODE_DEBUG
   PRECONDITION (ind < dimension());
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Entier i, indMax;

   // ---  Recopie les éléments
   indMax = dimension() - 1;
   for (i = ind; i < indMax; i++)
      (*this)[i] = (*this)[i+1];

   // ---  Décrémente l'indice
   indVect--;
   if (indVect < 0)
   {
      if (indBloc != 0) indVect = dimBloc - 1;
      enleveBloc();
   }

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return OK;
}  // template <typename T> Entier TPVecteur<T>::enleve (const EntierN i)


//**************************************************************
// Description:
//    Méthode protégée; Enlève un bloc à la liste des blocs.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
Entier TPVecteur<T>::enleveBloc ()
{
#ifdef MODE_DEBUG
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (indBloc > 0)
   {
      delete[] blocsV[indBloc];
      blocsV[indBloc] = NUL;
      indBloc --;
   }

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return OK;
}  // template <typename T> Entier TPVecteur<T>::enleveBloc ()


//**************************************************************
// Description:
//    Insère l'élément donné à l'indice demandé. Au besoin,
//    le vecteur est allongé.
//
// Entrée:
//    const T& item  : élément à ajouter au vecteur
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
Entier TPVecteur<T>::insere (const EntierN ind,
                             const T& item)
{
#ifdef MODE_DEBUG
   PRECONDITION (ind <= dimension());
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Entier i, indMax;

   // ---  Ajuste les indices
   indVect ++;
   if (indVect == dimBloc)
   {
      indVect = 0;
      ajouteBloc();
   }

   // ---  Déplace vers le haut
   indMax = dimension() - 1;
   for (i = indMax; i > ind; i--)
      (*this)[i] = (*this)[i-1];

   // ---  Remplace l'élément à l'indice demandé
   (*this)[ind] = item;

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return OK;
}  // template <typename T> Entier TPVecteur<T>::insere (const EntierN, const T&)


//**************************************************************
// Description:
//    Echange les éléments des deux indices.
//
// Entrée:
//    const EntierN i      : Premier indice
//    const EntierN j      : Deuxième indice
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
Entier TPVecteur<T>::echange (const EntierN i,
                              const EntierN j)
{
#ifdef MODE_DEBUG
   PRECONDITION (i < dimension());
   PRECONDITION (j < dimension());
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   T tmp = (*this)[i];
   (*this)[i] = (*this)[j];
   (*this)[j] = tmp;

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return OK;
}  // template <typename T> Entier TPVecteur<T>::echange


//**************************************************************
// Description:
//    Trie le vecteur depuis l'indice indInf jusqu'à l'indice indSup.
//    L'utilisateur doit fournir la fonction pour comparer les éléments.
//    Cette fonction de prototype:
//       Entier (*compElem)(const T& t1, const T& t2)
//    doit retourner 1 si t1 > t2, 0 si t1 == t2 et -1 si t1 < t2.
//
// Entrée:
//    Entier (*compElem)(const T& t1, const T& t2) : Fonction de comparaison
//    const EntierN indInf    : Indice inférieur pour le trie
//    const EntierN indSup    : Indice supérieur pour le trie
//
// Sortie:
//
// Notes:
//    Cette méthode est adaptée de "Numerical Recipes in C", vesion 2.5,
//    W.H. Press, B.P. Flannery, S.A. Teukolsky, W.T. Vetterling.
//    Cambridge University Press. (INRS-Eau 12-2003429)
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
Entier TPVecteur<T>::trie(Entier (*compElem)(const T&, const T&),
                                             const EntierN indInf,
                                             const EntierN indSup)
{
#ifdef MODE_DEBUG
   PRECONDITION (indInf <= indSup);
   PRECONDITION (indSup == ENTIER_MAX || indSup < dimension());
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   const Entier M = 7;
   const Entier NSTACK = 100;

   Entier err = OK;
   Entier i, j, k;
   Entier indGauche;
   Entier indDroite;
   Entier jstack=0;
   Entier istack[NSTACK];
   T elemTmp;

   // ---  Initialise les indices
   indGauche = indInf;
   if (indSup == ENTIER_MAX)
      indDroite = dimension() - 1;
   else
      indDroite = indSup;

   // --- Trie
   for (;;)
   {
      // ---  Trie par insertion si c'est un petit bloc
      if (indDroite-indGauche < M)
      {
         for (j = indGauche+1; j <= indDroite; j++)
         {
            elemTmp = (*this)[j];
            for (i = j-1; i >= indGauche; i--)
            {
               if (compElem(elemTmp, (*this)[i]) > 0) break;
               (*this)[i+1] = (*this)[i];
            }
            (*this)[i+1] = elemTmp;
         }
         if (jstack == 0) break;
         indDroite = istack[--jstack];
         indGauche = istack[--jstack];
      }
      // ---  Continue Quick Sort
      else
      {
         k = (indGauche+indDroite) >> 1;
         echange(k, indGauche+1);
         if (compElem((*this)[indGauche+1], (*this)[indDroite]) > 0) echange(indGauche+1, indDroite);
         if (compElem((*this)[indGauche],   (*this)[indDroite]) > 0) echange(indGauche,   indDroite);
         if (compElem((*this)[indGauche+1], (*this)[indGauche]) > 0) echange(indGauche+1, indGauche);
         i = indGauche + 1;
         j = indDroite;
         elemTmp = (*this)[indGauche];
         for (;;)
         {
            while (compElem((*this)[++i], elemTmp) < 0) { ;}
            while (compElem((*this)[--j], elemTmp) > 0) { ;}
            if (j < i) break;
            echange(i, j);
         }
         echange(j, indGauche);

         if (jstack >= NSTACK) {err = !OK; break;}
         if (indDroite-i+1 >= j-indGauche)
         {
            istack[jstack++] = i;
            istack[jstack++] = indDroite;
            indDroite = j-1;
         }
         else
         {
            istack[jstack++] = indGauche;
            istack[jstack++] = j-1;
            indGauche = i;
         }
      }
   }

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return err;
}  // template <typename T> inline Entier TPVecteur<T>::trie


//**************************************************************
// Description:
//    Recherche l'indice de l'élément supérieur ou égal à celui
//    demandé. Cette méthode par bisection n'est valable que sur un
//    vecteur trié. Sur un vecteur non trié, il faut utiliser la méthode
//    trouveL.
//
// Entrée:
//    const T& item           : Elément à rechercher
//    Entier (*compElem)(const T& t1, const T& t2) : Fonction de comparaison
//    const EntierN indInf    : Indice inférieur pour la recherche
//    const EntierN indSup    : indice supérieur pour la recherche
//
// Sortie:
//    EntierN& ind   : Indice de l'élément
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
Entier TPVecteur<T>::trouveB (EntierN& ind,
                              const T& item,
                              Entier (*compElem)(const T&, const T&),
                              const EntierN indInf,
                              const EntierN indSup) const
{
#ifdef MODE_DEBUG
   PRECONDITION (indInf <= indSup);
   PRECONDITION (indSup == ENTIER_MAX || indSup < dimension());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Entier err = OK;
   Entier cmp;
   Entier indGauche, indDroite, indMed;

   if (dimension() == 0) return !OK;

   // ---  Initialise les indices
   indGauche = indInf;
   if (indSup == ENTIER_MAX)
      indDroite = dimension() - 1;
   else
      indDroite = indSup;

   // ---  Elimine les cas triviaux
   if (compElem(item, (*this)[indGauche]) < 0) return !OK;
   if (compElem(item, (*this)[indDroite]) > 0) return !OK;

   // ---  Cherche par bisection
   while (indDroite-indGauche > 0)
   {
      indMed = (indGauche+indDroite+1) >> 1;
      cmp = compElem (item, (*this)[indMed]);
      if (cmp < 0)
         indDroite = indMed - 1;
      else if (cmp > 0)
         indGauche = indMed;
      else
         indGauche = indDroite = indMed;
   }
   ind = indGauche;
   ASSERTION (compElem((*this)[ind], item) <= 0);

#ifdef MODE_DEBUG
   POSTCONDITION (err != OK || ind >= indInf);
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return err;
}  // template <typename T> TPVecteur<T>::trouveB


//**************************************************************
// Description:
//    Recherche l'indice de l'élément demandé. Cette méthode est
//    inefficace, mais c'est la seule utilisable sur un vecteur non trié.
//    Sur un vecteur trié, il est recommandé d'utiliser la méthode trouveB.
//
// Entrée:
//    const T& item           : Elément à rechercher
//    Entier (*compElem)(const T& t1, const T& t2) : Fonction de comparaison
//    const EntierN indInf    : Indice inférieur pour la recherche
//    const EntierN indSup    : indice supérieur pour la recherche
//
// Sortie:
//    EntierN& ind   : Indice de l'élément
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
Entier TPVecteur<T>::trouveL (EntierN& ind,
                              const T& item,
                              Entier (*compElem)(const T&, const T&),
                              const EntierN indInf,
                              const EntierN indSup) const
{
#ifdef MODE_DEBUG
   PRECONDITION (indInf <= indSup);
   PRECONDITION (indSup == ENTIER_MAX || indSup < dimension());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Entier  err = !OK;
   EntierN i, indGauche, indDroite;

   // ---  Initialise les indices
   indGauche = indInf;
   if (indSup == ENTIER_MAX)
      indDroite = dimension() - 1;
   else
      indDroite = indSup;

   // ---  Cherche linéaire
   for (i = indGauche; i <= indDroite; i++)
   {
      if (compElem(item, (*this)[i]) == 0)
      {
         err = OK;
         break;
      }
   }
   if (err == OK) ind = i;

#ifdef MODE_DEBUG
   POSTCONDITION (err != OK || (ind >=indGauche && ind <= indDroite));
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return err;
}  // template <typename T> TPVecteur<T>::trouveL


//**************************************************************
// Description:
//    Vide le vecteur. Désalloue les blocs mais conserve le vecteur de blocs
//    à sa dimension.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
Entier TPVecteur<T>::vide ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Détruis tous les blocs sauf le premier
   for (Entier i = 1; i <= indBloc; i++)
      delete[] blocsV[i];

   // ---  Initialise les indices
   indBloc =  0;
   indVect = -1;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return OK;
}  // template <typename T> TPVecteur<T>::vide ()


//**************************************************************
// Description: operator []. Retourne l'élément à l'indice demandé.
//
// Entrée:
//    const Entier ind
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
inline T& TPVecteur<T>::operator [] (const EntierN ind) const
{
#ifdef MODE_DEBUG
   PRECONDITION (ind < dimension());
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Entier i = ind >> exposant;
   ASSERTION (i < nbrBlocs);
   Entier j = ind & masque;
   ASSERTION ((i <  (nbrBlocs-1) && (j <  dimBloc)) ||
               (i == (nbrBlocs-1) && (j <= indVect)));

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return blocsV[i][j];
}  // template <typename T> inline T& TPVecteur<T>::operator []


//**************************************************************
// Description: operator =; Copie le vecteur passé en paramètre sur le
//    vecteur courant.
//
// Entrée:
//    const TPVecteur t : TPVecteur à copier
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
TPVecteur<T>& TPVecteur<T>::operator = (const TPVecteur& t)
{
#ifdef MODE_DEBUG
   PRECONDITION (&t != NUL);
   t.invariant("PRECONDITION");
   INVARIANTS ("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   EntierN i, j, dimDest, dimSource;
   dimDest  =   dimension();
   dimSource= t.dimension();

   // ---  Si les dimensions sont différentes, ajuste
   if (dimDest != dimSource)
   {
      // ---  Détruis les éléments actuels
      vide();

      // ---  Ajuste le vecteur de blocs
      if (t.nbrBlocs > nbrBlocs)
      {
         delete[] blocsV;
         blocsV = new TP[t.nbrBlocs];
      }

      // ---  Copie les attributs
      exposant = t.exposant;
      masque   = t.masque;
      dimBloc  = t.dimBloc;
      nbrBlocs = t.nbrBlocs;
      indBloc  = t.indBloc;
      indVect  = t.indVect;

      // ---  Crée les blocs - le premier existe déjà
      for (i = 1; i <= indBloc; i++)
         blocsV[i]= new T [dimBloc];
   }

   // ---  Copie les éléments
   for (i = 0; i < indBloc; i++)
     for (j = 0; j < dimBloc; j++)
         blocsV[i][j] = t.blocsV[i][j];
   for (j = 0; j <= indVect; j++)
         blocsV[indBloc][j] = t.blocsV[indBloc][j];

#ifdef MODE_DEBUG
   INVARIANTS ("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return (*this);
}  // template <typename T> inline TPVecteur<T>& TPVecteur<T>::operator =


#endif   // TPVECTOR_HPP_DEJA_INCLU
