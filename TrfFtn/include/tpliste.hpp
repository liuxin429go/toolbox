//************************************************************************
// $Header$
// $Date$
//************************************************************************
//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Nom du fichier: tpliste.hpp
// Classe:  TPListe
//************************************************************************
#ifndef TPLISTE_HPP_DEJA_INCLU
#define TPLISTE_HPP_DEJA_INCLU

//**************************************************************
// Description: Constructeur par défaut.
//
// Entrée:
//
// Sortie:
//
// Notes:
//    La précondition est pour s'assurer que le type de la liste
//    est bien un pointeur. Ce n'est pas très sévère comme test, et
//    il génère des Warning de compilation si le type est correct.
//    A défaut de mieux, il est laissé tel quel.
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
TPListe<T>::TPListe (const EntierN dim)
   : TPVecteur<T> (dim)
{
#ifdef MODE_DEBUG
   PRECONDITION (sizeof(T) == sizeof(VoidP));
#endif   // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}  // template <typename T> TPListe<T>::TPListe (const EntierN dim)


//**************************************************************
// Description: Copie Constructeur.
//
// Entrée:
//    const TPListe& t      : Vecteur à copier
//
// Sortie:
//
// Notes:
//    La précondition est pour s'assurer que le type de la liste
//    est bien un pointeur. Ce n'est pas très sévère comme test, et
//    il génère des Warning de compilation si le type est correct.
//    A défaut de mieux, il est laissé tel quel.
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
TPListe<T>::TPListe (const TPListe& t)
   : TPVecteur<T> (t)
{
#ifdef MODE_DEBUG
   PRECONDITION (sizeof(T) == sizeof(VoidP));
#endif   // ifdef MODE_DEBUG

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
}  // template <typename T> TPListe<T>::TPListe (const TPListe& t)


//**************************************************************
// Description:
//    Destructeur. Désalloue les blocs puis le vecteur de blocs
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
TPListe<T>::~TPListe ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

}  // template <typename T> TPListe<T>::~TPListe ()


//**************************************************************
// Description:
//    Efface l'éléments(delete sur le pointeur) et enlève l'indice de la liste.
//
// Entrée:
//    const EntierN ind    : Indice de l'élément à effacer
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
inline Entier TPListe<T>::efface (const EntierN ind)
{
#ifdef MODE_DEBUG
   PRECONDITION (ind < this->dimension());
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Efface l'élément
   delete (*this)[ind];

   // ---  Enlève l'indice
   this->enleve(ind);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return OK;
}  // template <typename T> TPListe<T>::efface (const EntierN)


//**************************************************************
// Description:
//    Efface tous les éléments (delete sur les pointeurs) et vide la liste.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 27-10-94         par Yves Secretan
//**************************************************************
template <typename T> 
Entier TPListe<T>::effaceTout ()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   // ---  Efface tous les élément
   for (EntierN i = 0; i < this->dimension(); i++)
      delete (*this)[i];

   // ---  Vide la liste
   this->vide();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return OK;
}  // template <typename T> TPListe<T>::effaceTout ()

#endif   // define TPLISTE_HPP_DEJA_INCLU
