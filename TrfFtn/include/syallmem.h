//************************************************************************
// $Header$
// $Date$
// $Author$
// $Locker$
//************************************************************************
//************************************************************************
// --- Copyright (c) 2000 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette 
// --- permission apparaissent dans toutes les copies ainsi que dans la 
// --- documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: syallmem.h
//
// Classe:  SYAllocateurMemoire
//
// Sommaire:   Pool d'allocation de mémoire.
//
// Description:
//    La classe <code>SYAllocateurMemoire</code> représente
//
// Invariants:
//
// Attributs:
//    TPVecteur<TTData>  pool;      
//    TPListe  <TTDataP> pile;
//
// Notes:
//
//************************************************************************
// 27-10-94  Yves Secretan
// 20-08-99  Yves Secretan  Version Web++ 2.0b
//************************************************************************
#ifndef SYALLMEM_H_DEJA_INCLU
#define SYALLMEM_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

#include "tpliste.h"
#include "tpvector.h"

template <class TTData, EntierN dim = 16>
class SYAllocateurMemoire
{
public:
   typedef TTData* TTDataP;

           SYAllocateurMemoire  ();
           ~SYAllocateurMemoire ();

   TTDataP demandePtr           ();
   void    relachePtr           (const TTDataP);

protected:
   static const EntierN DIM_BLOC;

   TPVecteur<TTData>  pool;
   TPListe  <TTDataP> pile;

   void invariant (ConstCarP) const;
};

//**************************************************************
// Sommaire: Contrôle les invariants de la classe
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contrôle les invariants
//    de la classe.
//
// Entrée:
//    ConstCarP conditionP    //  "PRECONDITON" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
template <class TTData, EntierN dim>
inline void
SYAllocateurMemoire<TTData, dim>::invariant(ConstCarP /*conditionP*/) const
{
//   INVARIANT (VRAI, conditionP);
}
#else
template <class TTData, EntierN dim>
inline void
SYAllocateurMemoire<TTData, dim>::invariant(ConstCarP) const
{
}
#endif //   #ifdef MODE_DEBUG

#include "syallmem.hpp"

#endif   // SYALLMEM_H_DEJA_INCLU

