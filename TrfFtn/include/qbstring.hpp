//************************************************************************
// --- Copyright (c) 2000
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette
// --- permission apparaissent dans toutes les copies ainsi que dans la
// --- documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: qbstring.hpp
// Classe : QBString
//************************************************************************
// 20-08-99  Yves Secretan  Version Web++ 2.0b
//************************************************************************
#ifndef QBSTRING_HPP_DEJA_INCLU
#define QBSTRING_HPP_DEJA_INCLU

#include <iostream>
#include <cstring>
#include <cctype>

//**************************************************************
// Sommaire: Duplifie la chaîne.
//
// Description:
//    La méthode publique <code>dupAsCarP()</code> retourne une
//    nouvelle chaîne copie de l'objet. La chaîne est allouée par
//    appel à <code>new char[]</code> et doit être désallouée par
//    l'utilisateur avec un appel à <code>delete[]</code>
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline CarP QBString::dupAsCarP() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   CarP cP = new Car[std::strlen(chaineP)+1];
   strcpy(cP, chaineP);

   return cP;
}  // QBString::dupAsCarP


//**************************************************************
// Sommaire: Retourne la longueur de la chaîne.
//
// Description:
//    La méthode publique <code>len()</code> retourne la longeur
//    de la chaîne.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline Entier QBString::len() const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return static_cast<Entier>( strlen(chaineP) );
}  // QBString::instr


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline Entier QBString::instr(ConstCarP cP) const
{
#ifdef MODE_DEBUG
   PRECONDITION(cP  != 0);
   PRECONDITION(*cP != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   ConstCarP posP = strstr(chaineP, cP);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return (posP == NULL) ? -1 : static_cast<Entier>(posP-chaineP);
}  // QBString::instr

inline Entier QBString::instr(const QBString& copie) const
{
   return instr(copie.chaineP);
}  // QBString::instr

inline Entier instr(const QBString& copie, const QBString& s)
{
   return copie.instr(s);
}  // QBString::instr

inline Entier instr(const QBString& copie, ConstCarP cP)
{
   return copie.instr(cP);
}  // QBString::instr


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline Entier QBString::rinstr(const QBString& copie) const
{
   return rinstr(copie.chaineP);
}  // QBString::rinstr

inline Entier rinstr(const QBString& copie, const QBString& s)
{
   return copie.rinstr(s);
}  // QBString::rinstr

inline Entier rinstr(const QBString& copie, ConstCarP cP)
{
   return copie.rinstr(cP);
}  // QBString::rinstr


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::erase(ConstCarP cP)
{
#ifdef MODE_DEBUG
   PRECONDITION(cP != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   if (strlen(cP) == 0) return *this;

   CarP posDebP = strstr(chaineP, cP);
   if (posDebP != NULL)
   {
      CarP posFinP = posDebP + strlen(cP);
      *posDebP = 0x0;
      strcat (chaineP, posFinP);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::erase

inline const QBString& QBString::erase(const QBString& copie)
{
   return erase(copie.chaineP);
}  // QBString::erase


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::lcase()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Car *modifP = chaineP;
   
   while (*modifP != '\0')
   {
      if (isupper(*modifP))
         *modifP = tolower(*modifP);
      ++modifP;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::lcase

inline const QBString& QBString::ucase()
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   Car *modifP = chaineP;
   
   while (*modifP != '\0')
   {
      if (islower(*modifP))
         *modifP = toupper(*modifP);
      ++modifP;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::ucase

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::left(const Entier posi)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

	EntierN pos = (posi < 0) ? 0 : static_cast<Entier>(posi);
   if (pos < std::strlen(chaineP))
      chaineP[pos] = 0x0;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::left

inline const QBString& QBString::left(ConstCarP cP)
{
#ifdef MODE_DEBUG
   PRECONDITION(cP  != 0);
   PRECONDITION(*cP != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   char* posiP = strstr(chaineP, cP);
   if (posiP != 0)
      *posiP = 0x0;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::left

inline const QBString& QBString::left(const QBString& copie)
{
   return left(copie.chaineP);
}  // QBString::left


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::right(const Entier taille)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;
   if (taille < static_cast<Entier>(strlen(chaineP)))
      strcpy(chaineP, chaineP+strlen(chaineP)-taille);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::right

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::mid(const Entier posi)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   if (posi >= 0 && posi < static_cast<Entier>(strlen(chaineP)))
      strcpy(chaineP, chaineP+posi);
   else
      memset(chaineP, 0x0, MAXSIZE-1);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::mid

inline const QBString& QBString::mid(ConstCarP cP)
{
#ifdef MODE_DEBUG
   PRECONDITION(cP  != 0);
   PRECONDITION(*cP != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   char *posiP = strstr(chaineP, cP);
   if (posiP != NUL)
      strcpy(chaineP, posiP+strlen(cP));
   else
      memset(chaineP, 0x0, MAXSIZE-1);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::mid

inline const QBString& QBString::mid(const QBString& s)
{
   return mid(s.chaineP);
}  // QBString::mid


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::mid(const Entier pos, const Entier len)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   if (pos < static_cast<Entier>(strlen(chaineP)))
   {
      strncpy(chaineP, chaineP+pos, len);
      chaineP[len] = 0x0;
   }
   else
   {
      memset(chaineP, 0x0, MAXSIZE-1);
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString:mid


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::bloc(const QBString& s1, const QBString& s2)
{
   return bloc(s1.chaineP, s2.chaineP);
}  // QBString::bloc


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::rbloc(ConstCarP c1P, ConstCarP c2P)
{
#ifdef MODE_DEBUG
   PRECONDITION(c1P  != 0);
   PRECONDITION(*c1P != 0);
   PRECONDITION(c2P  != 0);
   PRECONDITION(*c2P != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   QBString s1 = c1P;
   QBString s2 = c2P;

   invert();
   bloc(s2.invert(), s1.invert());
   invert();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::rbloc

inline const QBString& QBString::rbloc(const QBString& s1, const QBString& s2)
{
   return rbloc(s1.chaineP, s2.chaineP);
}  // QBString::rbloc

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::subst(const QBString& s1, const QBString& s2)
{
   return subst(s1.chaineP, s2.chaineP);
}  // QBString::subst


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::token(const QBString& s)
{
   return token(s.chaineP);
}  // QBString::token


//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::rtoken(ConstCarP c1P)
{
#ifdef MODE_DEBUG
   PRECONDITION(c1P  != 0);
   PRECONDITION(*c1P != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   QBString s1 = c1P;

   invert();
   token(s1.invert());
   invert();

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::rtoken

inline const QBString& QBString::rtoken(const QBString& s1)
{
   return rtoken(s1.chaineP);
}  // QBString::rtoken

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::operator = (const QBString& copie)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   memset(chaineP, 0x0, MAXSIZE-1);
   if (copie.chaineP != NUL)
      strcpy (chaineP, copie.chaineP);
   chaineP[MAXSIZE-1] = Car(0xAB);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::operator =

inline const QBString& QBString::operator = (ConstCarP cP)
{
#ifdef MODE_DEBUG
   PRECONDITION(cP != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   memset(chaineP, 0x0, MAXSIZE-1);
   if (cP != NUL)
      strcpy (chaineP, cP);
   chaineP[MAXSIZE-1] = Car(0xAB);

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return *this;
}  // QBString::operator =

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline const QBString& QBString::operator += (ConstCarP cP)
{
#ifdef MODE_DEBUG
   PRECONDITION(cP  != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   size_t lThis  = strlen(chaineP);
   size_t lCopie = strlen(cP);
   size_t lMax   = ((MAXSIZE-lThis-2) < lCopie) ? (MAXSIZE-lThis-2) : lCopie;
   strncpy(chaineP+lThis, cP, lMax);
   chaineP[lMax+lThis] = 0x0;

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return (*this);
}  // QBString::operator +=

inline const QBString& QBString::operator += (const QBString& copie)
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return (*this)+=copie.chaineP;
}  // QBString::operator +=

inline const QBString& QBString::operator += (Car c)
{
#ifdef MODE_DEBUG
   PRECONDITION(c != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;

   size_t len = strlen(chaineP);
   if (len+2 < MAXSIZE)
   {
      chaineP[len  ] = c;
      chaineP[len+1] = 0x0;
   }

#ifdef MODE_DEBUG
   INVARIANTS("POSTCONDITION");
#endif   // ifdef MODE_DEBUG
   return (*this);
}  // QBString::operator +=

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline Booleen QBString::operator == (const QBString& copie) const
{
#ifdef MODE_DEBUG
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return (strcmp(chaineP, copie.chaineP) == 0);
}  // QBString::operator ==

inline Booleen QBString::operator == (ConstCarP cP) const
{
#ifdef MODE_DEBUG
   PRECONDITION(cP  != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return (strcmp(chaineP, cP) == 0);
}  // QBString::operator ==

inline Booleen QBString::operator == (CarP cP) const
{
#ifdef MODE_DEBUG
   PRECONDITION(cP  != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   return (strcmp(chaineP, cP) == 0);
}  // QBString::operator ==

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline Booleen QBString::operator < (ConstCarP cP) const
{
#ifdef MODE_DEBUG
   PRECONDITION(cP  != 0);
   INVARIANTS("PRECONDITION");
#endif   // ifdef MODE_DEBUG

   using namespace std;
   return (strcmp(chaineP, cP) < 0);
}  // QBString::operator ==

//**************************************************************
// Sommaire:
//
// Description:
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
inline std::istream& operator >> (std::istream& is, QBString& copie)
{
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG
   is.getline(copie.chaineP, QBString::MAXSIZE);
   return is;
}  // QBString::operator >>

inline std::ostream& operator << (std::ostream& os, const QBString& copie)
{
#ifdef MODE_DEBUG
#endif   // ifdef MODE_DEBUG

   return (os << copie.chaineP);
}  // QBString::operator <<

#endif // QBSTRING_HPP_DEJA_INCLU

