//*****************************************************************************
// $Header$
// $Date$
// $Author$
// $Locker$
//*****************************************************************************
//************************************************************************
// --- Copyright (c) 2000 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette 
// --- permission apparaissent dans toutes les copies ainsi que dans la 
// --- documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************
//************************************************************************
// Fichier: tpobjdef.h
//
// Classe:  TPObjetDefere
//
// Description:
//    La classe template TPObjetDefere est une functor qui représente un
//    objet dont la construction est déférée à la première utilisation.
//    Elle est composée d'un pointeur à l'objet, et l'accès se fait à
//    l'aide de l'opérateur(). <p>
//    Une utilisation possible est de remplacer les objets statiques par
//    des TPObjetDefere. En déférant la construction de l'objet jusqu'à
//    qu'il soit utilisé, on évite les problèmes d'ordre d'initialisation
//    des variables statiques.
//
// Attributs:
//    TTObjet*    Pointeur à l'objet
//
// Notes:
//
//************************************************************************
// 15-07-96  Yves Secretan
// 20-08-99  Yves Secretan    Version Web++ 2.0b
//************************************************************************
#ifndef TPOBJDEF_H_DEJA_INCLU
#define TPOBJDEF_H_DEJA_INCLU

#include "sytypes.h"

template <class TTObjet>
class TPObjetDefere
{
public:
   TPObjetDefere() : objetP(NUL), detruis(FAUX) {}
   ~TPObjetDefere()
   {
      delete objetP;
      objetP = NUL;
      detruis = VRAI;
   }

   TTObjet* operator () ()
   {
      if (objetP == NULL && ! detruis)
         objetP = new TTObjet;
      return objetP;
   }

private:
   TPObjetDefere(const TPObjetDefere&);
   TPObjetDefere& operator = (const TPObjetDefere&);

   TTObjet* objetP;
   Booleen  detruis;
};

#endif   // TPOBJDEF_H_DEJA_INCLU

