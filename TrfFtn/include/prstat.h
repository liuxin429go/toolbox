//************************************************************************
// --- Copyright (c) 2000 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS R╔SERV╔S
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette 
// --- permission apparaissent dans toutes les copies ainsi que dans la 
// --- documentation. 
// --- L'INRS ne prétend en aucune faþon que ce logiciel convient Ó un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************
//*****************************************************************************
// Fichier:     prstat.h
//
// Classe:      PRStatement
//
// Sommaire:
//
// Description:
//
// Attributs:
//
// Notes:
//*****************************************************************************
// 24-08-93  Yves Secretan  Version initiale
// 25-09-97  Yves Roy       Préparation pour une version simple pour étudiants.
// 20-08-99  Yves Secretan  Version Web++ 2.0b
//*****************************************************************************
#ifndef PRSTAT_H_DEJA_INCLU
#define PRSTAT_H_DEJA_INCLU

#include "sytypes.h"
#include "ermsg.h"

#include "qbstring.h"

DECLARE_CLASS(PRStatement);
DECLARE_CLASS(PRistream);

class PR_EXPORT_MODIF PRStatement : public QBString
{
public:
           PRStatement   ()                     : QBString()   {}
           PRStatement   (ConstCarP cP)         : QBString(cP) {}
           PRStatement   (const wchar_t* cP)    : QBString(cP) {}
           PRStatement   (const QBString& c)    : QBString(c)  {}
           PRStatement   (const PRStatement& c) : QBString(c)  {}
           ~PRStatement  () {}

   static void asgTokenEnteteDebut(const PRStatement&);
   static void asgTokenEnteteFin  (const PRStatement&);

   Booleen estClass      () const;
   Booleen estStruct     () const;
   Booleen estUnion      () const;
   Booleen estEnum       () const;
   Booleen estAttribut   () const;
   Booleen estMethode    () const;
   void    getTypeName   (PRStatement&, PRStatement&) const;

   ERMsg   readHeaderLine(PRistream&);
   ERMsg   readLine      (PRistream&);
   ERMsg   readStatement (PRistream&);

protected:
   void    invariant     (ConstCarP) const;

private:
   static ConstCarP tokenEnteteDebut;
   static ConstCarP tokenEnteteFin;

   static Booleen contiensUniquement  (const char*, const char*);

   static ERMsg   readToChar    (PRistream&, const char , char*, int);
   static ERMsg   readToString  (PRistream&, const char*, char*, int);
   static ERMsg   readToString  (PRistream&, const char*, const char*, char*, int, int&);
   static ERMsg   skipToChar    (PRistream&, const char);
   static ERMsg   skipToString  (PRistream&, const char*);
};

PRStatement traduisAOEM       (const PRStatement&);
PRStatement traduisANomFichier(const PRStatement&);

//**************************************************************
// Sommaire: Contr¶le les invariants de la classe.
//
// Description:
//    La méthode protégée <code>invariant(...)</code> contr¶le les
//    invariants de la classe. Si un invariant est violé, le
//    traitement est stoppé.
//
// Entrée:
//    ConstCarP conditionP :  "PRECONDITION" ou "POSTCONDITION"
//
// Sortie:
//
// Notes:
//**************************************************************
inline void PRStatement::invariant(ConstCarP conditionP) const
{
   QBString::invariant(conditionP);
}

#endif // PRSTAT_H_DEJA_INCLU

