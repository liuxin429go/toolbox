//************************************************************************
// --- Copyright (c) 1992-1998
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: tpvecsmp.h
//
// Classe:  TPVecteurSimple
//
// Description:
//    Classe template de vecteurs. Les éléments du vecteur sont stockés
//    par blocs. Cette classe optimise la clase TPVecteur pour les
//    objets qu'il est possible de copier par copie de mémoire.
//
// Attributs:
//
// Fonctions:
//
// Notes:
//
//************************************************************************
// 27-10-94  Yves Secretan      Version originale
// 28-10-98  Yves Secretan      Remplace #include "ererreur"
//************************************************************************
#ifndef TPVECSMP_H_DEJA_INCLU
#define TPVECSMP_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

#include "tpvector.h"

template <typename T> class TPVecteurSimple : public TPVecteur <T>
{
protected:
   inline void  invariant (ConstCarP) const;

public:
   typedef TPVecteur<T>       TCParent;
   typedef TPVecteurSimple<T> TCSelf;
   
          TPVecteurSimple (const EntierN = TCParent::DIM_BLOC);
          TPVecteurSimple (const TPVecteurSimple&);
          TPVecteurSimple (T*, const EntierN, const EntierN = TCParent::DIM_BLOC);
          ~TPVecteurSimple();

          TPVecteurSimple& operator = (const TPVecteurSimple&);
};

//**************************************************************
// Description: Invariants de classe.
//
// Entrée:
//
// Sortie:
//
// Notes:
//
//**************************************************************
#ifdef MODE_DEBUG
template <typename T>
inline void TPVecteurSimple<T>::invariant(ConstCarP conditionP) const
{
   TPVecteur<T>::invariant(conditionP);
}
#else
template <typename T>
inline void TPVecteurSimple<T>::invariant(ConstCarP) const
{
}
#endif

#include "tpvecsmp.hpp"

#endif   // TPVECSMP_H_DEJA_INCLU



