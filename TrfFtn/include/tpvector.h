//************************************************************************
// --- Copyright (c) 1992-1998 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que 
// --- commerciales sont autorisées sans frais pour autant que la présente 
// --- notice de copyright ainsi que cette permission apparaissent dans 
// --- toutes les copies ainsi que dans la documentation. 
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************

//************************************************************************
// Fichier: tpvector.h
//
// Classe:  TPVecteur
//
// Description:
//    Classe template de vecteurs. Les éléments du vecteur sont stockés
//    par blocs.
//
// Parents:
//
// Type: template
//
// Invariants:
//
// Attributs:
//
// Méthodes:
//
// Fonctions:
//
// Notes:
//
//************************************************************************
// 27-10-94  Yves Secretan      Version originale
// 15-09-95  Yves Roy           Enlever le mot réservé inline dans la déclaration
// 28-10-98  Yves Secretan      Remplace #include "ererreur"
//************************************************************************
#ifndef TPVECTOR_H_DEJA_INCLU
#define TPVECTOR_H_DEJA_INCLU

#include "sytypes.h"
#include "erexcept.h"

#include <climits>
#define ENTIER_MAX LONG_MAX

template <typename T> class TPVecteur
{
protected:
   typedef T* TP;

   static const Entier DIM_BLOC;
   static const Entier NBR_BLOC;

   Entier indVect;
   Entier indBloc;
   Entier dimBloc;
   Entier nbrBlocs;
   Entier masque;
   Entier exposant;
   TP*    blocsV;

   Entier ajouteBloc();
   Entier enleveBloc();

   void invariant (ConstCarP) const;

public:
                 TPVecteur  (const EntierN = DIM_BLOC);
                 TPVecteur  (const TPVecteur&);
                 ~TPVecteur ();

   Entier        ajoute   (const T&);
   Entier        enleve   (const EntierN);
   Entier        insere   (const EntierN, const T&);
   Entier        echange  (const EntierN, const EntierN);
   EntierN       dimension() const;
   Entier        trouveB  (EntierN&, const T&,
                           Entier (*) (const T&, const T&), const EntierN = 0,
                           const EntierN = ENTIER_MAX) const;
   Entier        trouveL  (EntierN&, const T&,
                           Entier (*) (const T&, const T&), const EntierN = 0,
                           const EntierN = ENTIER_MAX) const;
   Entier        trie     (Entier (*) (const T&, const T&), const EntierN = 0,
                           const EntierN = ENTIER_MAX);
   Entier        vide     ();

   T&            operator [] (const EntierN) const;
   TPVecteur&    operator << (const T& t) {ajoute(t); return (*this);}
   TPVecteur&    operator =  (const TPVecteur&);
};

//**************************************************************
// Description: Invariants de classe.
//
// Entrée:
//    ConstCarP conditionP
//
// Sortie:
//
// Notes:
//
//**************************************************************
// Créé le 24 août 1993         par Yves Secretan
//**************************************************************
#ifdef MODE_DEBUG
template <typename T> inline void TPVecteur<T>::invariant(ConstCarP conditionP) const
{
   INVARIANT (indVect >= 0 || (indVect == -1 && indBloc == 0), conditionP);
   INVARIANT (indVect < dimBloc, conditionP);
   INVARIANT (indBloc >= 0, conditionP);
   INVARIANT (indBloc < nbrBlocs, conditionP);
   INVARIANT (exposant > 0, conditionP);
   INVARIANT (dimBloc == (0x1 << exposant), conditionP);
   INVARIANT (masque == (dimBloc-1), conditionP);
   INVARIANT (blocsV != NUL, conditionP);
   INVARIANT (blocsV[0] != NUL, conditionP);
}
#else
template <typename T> inline void TPVecteur<T>::invariant(ConstCarP) const
{
}
#endif   // MODE_DEBUG

#include "tpvector.hpp"

#endif   // TPVECTOR_H_DEJA_INCLU

