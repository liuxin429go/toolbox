//************************************************************************
// --- Copyright (c) 2000 
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS R╔SERV╔S
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, sont autorisées sans frais
// --- pour autant que la présente notice de copyright ainsi que cette 
// --- permission apparaissent dans toutes les copies ainsi que dans la 
// --- documentation. 
// --- L'INRS ne prétend en aucune faþon que ce logiciel convient Ó un 
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie 
// --- implicite ou explicite.
//************************************************************************
//*****************************************************************************
// Fichier:     prifstr.h
//
// Classe:      PRistream
//
// Sommaire:
//
// Description:
//
// Attributs:
//
// Notes:
//*****************************************************************************
// 24-08-93  Yves Secretan  Version initiale
// 25-09-97  Yves Roy       Préparation pour une version simple pour étudiants.
// 14-03-98  Yves Secretan  const dans PRifstream
//                          Ajout de tellg()
// 20-08-99  Yves Secretan  Version Web++ 2.0b
//*****************************************************************************
#ifndef PRIFSTR_H_DEJA_INCLU
#define PRIFSTR_H_DEJA_INCLU

#include "sytypes.h"

#include "prstat.h"

#include <iostream>
#include <cstring>

DECLARE_CLASS(PRistream);

class PR_EXPORT_MODIF PRistream : protected std::istream
{
public:
           PRistream(std::streambuf* b) : std::istream(b), noLignes(0), commentaireC(false) {}
   virtual ~PRistream() {}

   int                      eof     ()               {return std::istream::eof();}
   int                      get     ()               {char c; get(c); return c;}
   PRistream&               get     (char& c)        {std::istream::get(c); if (c == '\n') ++noLignes; return *this;}
   PRistream&               getline (char* cP, int l){std::istream::getline(cP, l); if (static_cast<int>(strlen(cP)) < (l-1)) ++noLignes; return *this;}
   int                      peek    ()               {return std::istream::peek();}
   std::istream&   putback (char  c)        {std::istream::putback(c); if (c == '\n') --noLignes; return *this;}
   std::streampos  tellg   ()               {return std::istream::tellg();}

   bool     operator !    () const {return std::istream::operator!();}
   explicit operator bool () const {return std::istream::operator bool();}

   bool               estEnCommentaireC() const {return commentaireC; }
   void               asgEnCommentaireC(bool c) { commentaireC = c; }

   void               asgNoLigne       (Entier n, const PRStatement& f="") {if (f=="" || f==ficNoLignes) noLignes=n;}
   Entier             reqNoLigne       () const                            {return noLignes;}
   const PRStatement& reqFichierNoLigne() const                            {return ficNoLignes;}

protected:
   PRStatement ficNoLignes;
   Entier      noLignes;
   bool        commentaireC;

private:
           PRistream();
};


#include <fstream>
DECLARE_CLASS(PRifstream);

class PR_EXPORT_MODIF PRifstream : public std::fstream, public PRistream
{
private:
   typedef std::fstream TTFStream;

public:
        PRifstream    (const PRStatement& c) : TTFStream(ConstCarP(c), std::ios_base::in), PRistream(rdbuf()) {}
        ~PRifstream   () {}

   PRifstream&  get     (char& c)               {PRistream::get(c); return *this;}
   PRifstream&  getline (char* cP, int l)       {PRistream::getline(cP, l); return *this;}
   void         open    (const PRStatement& c)  {noLignes=0; ficNoLignes=c; TTFStream::open(ConstCarP(c), std::ios_base::in); }

   bool     operator !    () const {return PRistream::operator!();}
   explicit operator bool () const {return PRistream::operator bool();}

private:
        PRifstream    ();
};

#endif // PRIFSTR_H_DEJA_INCLU
