//************************************************************************
// --- Copyright (c) 1992-2003
// --- Institut National de la Recherche Scientifique (INRS)
// --- TOUS DROITS RÉSERVÉS
// --- ALL RIGHTS RESERVED
// ---
// --- Ce logiciel est couvert par les lois de copyright. L'utilisation,
// --- la copie ou la modification de ce logiciel sous toutes ses formes,
// --- que ce soit code source ou code compilé, à des fins autres que
// --- commerciales sont autorisées sans frais pour autant que la présente
// --- notice de copyright ainsi que cette permission apparaissent dans
// --- toutes les copies ainsi que dans la documentation.
// --- L'INRS ne prétend en aucune façon que ce logiciel convient à un
// --- emploi quelconque. Celui-ci est distribué sans aucune garantie
// --- implicite ou explicite.
//************************************************************************
//*****************************************************************************
// Fichier:  syconfig.h
//
// Classe:
//
// Sommaire: Fichier de configuration
//
// Description:
//    Le fichier de configuration contient tous les define de configuration
//    qui permettent de piloter la compilation en fonction des compilateurs
//    et des plateformes.
//
// Attributs:
//
// Notes:
//
//*****************************************************************************
// 05-08-1998  Yves Secretan      Version initiale
// 01-09-1998  Yves Roy           Ajout notice de copyright
// 28-04-1999  Yves Secretan      Nouveau define de garde contre les compilations multiples
//                                Introduit std
// 30-04-1999  Yves Secretan      Introduit INRS_SANS_NOUVEAUX_ENTETES et std
// 04-05-1999  Yves Secretan      Introduit INRS_SANS_SPECIALISATION_PARTIELLE_DES_CLASSES_TEMPLATE
// 19-05-1999  Yves Secretan      Introduit INRS_SANS_STL_MULTIPLIES_AVEC_STL_TIMES
// 26-01-2000  Yves Secretan      Support partiel Watcom
// 27-05-2003  Dominique Richard  Port multi-compilateur
// 10-12-2003  Yves Secretan      Ajout VisualC++ 7.1
// 18-02-2004  Yves Secretan      Ajout de macros
// 22-06-2004  Yves Secretan      Modification VisualC++ 7.1
//*****************************************************************************
#ifndef SYCONFIG_H_DEJA_INCLU
#define SYCONFIG_H_DEJA_INCLU

//**********************************************************************
//**********************************************************************
//
//                   Section d'initialisation
//
//**********************************************************************
//**********************************************************************

// define si std::pair n'a pas de constructeur par défaut
#undef INRS_STD_PAIR_SANS_CONSTRUCTEUR_PAR_DEFAUT

// define si les conteneurs STL n'ont pas le paramètre Allocator
#undef INRS_STD_CONTENEUR_SANS_PARAMETRE_ALLOCATOR

// define si O_TEXT n'est pas défini
#undef INRS_SANS_O_TEXT

// define si O_BINARY n'est pas défini
#undef INRS_SANS_O_BINARY

// define si inclus io.h avec fcntl.h
#undef INRS_DOIT_INCLURE_IO_H_AVEC_FCNTL_H

// define si bitset n'est pas defini
#undef INRS_SANS_BITSET

// define si on n'a pas share.h
#undef INRS_SANS_SHARE_H

// define si on n'a pas process.h
#undef INRS_SANS_PROCESS_H

// define si on n'a pas strlwr et strupr
#undef INRS_SANS_STRLWR_STRUPR

// define si xdr_quadruple n'est pas defini
#undef INRS_SANS_XDR_QUADRUPLE

// define si on est dans un environnement unix ne supportant pas
// le lock de fichier avec fcntl() sous NFS
#undef INRS_BUG_UNIX_LOCK_FCNTL_NFS

// define si on ne peut faire de using namespace std
#undef INRS_SANS_USING_NAMESPACE_STD

// define si la spécialisation partielle de classes template
// comportant des paramètres par défaut n'est pas supportée
#undef INRS_SANS_SPECIALISATION_PARTIELLE_DES_CLASSES_TEMPLATE

// define si le functor STL multiplies se nomme times
#undef INRS_SANS_STL_MULTIPLIES_AVEC_STL_TIMES

// declaration throw pour les new et delete
#undef INRS_NEW_THROW
#undef INRS_DEL_THROW

// define si les classes template avec des fonction friend template
// n'ont pas besoin de <> 
#undef INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM

// ---  Bugs

// define si la méthode erase(size_type, size_type) s'appelle remove
#undef INRS_BUG_STRING_REMOVE_REMPLACE_ERASE

// define si ne supporte pas le passage de paramètres par référence d'enum const
#undef INRS_BUG_CONST_ENUM_PAR_REFERENCE_DANS_TEMPLATE

// define si ne supporte pas != sur des enum --> !(a == b) pour remplacer
#undef INRS_BUG_NE_SUR_ENUM

// define si le strstream n'est pas synchronisé pour la lecture
//    Sur certaines implantations, le pointeur de lecture n'est pas
//    synchronisé. Un simple tellg() sur le buffer reset ce pointeur.
#undef INRS_BUG_STRSTREAM_SYNCHRONISE_LECTURE

// define s'il y a un problème avec les méthodes virtuelles dans les
// constructeur de classes faisant partie d'une hiérarchie avec héritage
// multiple.
#undef INRS_BUG_CONSTRUCTEUR_ET_METHODE_VIRTUELLE_HERITAGE_MULTIPLE

//**********************************************************************
//**********************************************************************
//
//                   Section des compilateurs
//
//**********************************************************************
//**********************************************************************

//----------------------------------------------------------------------
//
// ---  GNU
//
//----------------------------------------------------------------------
#undef INRS_GCC
#if defined(__GNUC__)

#  define INRS_GCC 1

//#  if (__GNUC__ < 3 || __GNUC == 3)
//#     error GNU < 3.3 - Version non supportee

//#  elif (__GNUC__ == 3 && (__GNUC_MINOR__ <= 3))
#     define INRS_SANS_O_TEXT
#     define INRS_SANS_O_BINARY
#     define INRS_NEW_THROW throw(std::bad_alloc)
#     define INRS_DEL_THROW throw()

//#  else
//#     error GNU > 3.3 - Version non supportee
//#  endif

#endif   // defined (__GNUC__)

//----------------------------------------------------------------------
//
// ---  aCC (Compilateur natif HP)
//
//----------------------------------------------------------------------
#undef INRS_ACC
#if defined(__HP_aCC)
#  define INRS_ACC 1
#  define INRS_SANS_XDR_QUADRUPLE
// La version courante de roguewave ne supporte pas le namespace std...
//aCC2 #define std
//aCC2 #undef MODE_NAMESPACE
#endif // __HP_aCC

//----------------------------------------------------------------------
//
// ---  KCC
//
//----------------------------------------------------------------------
#undef INRS_KCC
#if defined(__KCC)
#  define INRS_KCC 1
#  define INRS_SANS_O_TEXT
#  define INRS_SANS_O_BINARY
#endif   // defined (__KCC)

//----------------------------------------------------------------------
//
// ---  ICC
//
//----------------------------------------------------------------------
#undef INRS_ICC
#if defined(__ICC)
#  define INRS_ICC 1
#  define INRS_BUG_CONSTRUCTEUR_ET_METHODE_VIRTUELLE_HERITAGE_MULTIPLE
#endif // defined (__ICC)

//----------------------------------------------------------------------
//
// ---  SGI Mips PRO
// existe-t-il une meilleur facon de trouver le compilateur?
//----------------------------------------------------------------------
#undef INRS_SGI
#if defined (__sgi)

#  define INRS_SGI 1

#  if (_COMPILER_VERSION < 730)
#     error sgi < 7.30 - Version non supportee

#  elif (_COMPILER_VERSION == 730)
#     define INRS_BUG_CONSTRUCTEUR_ET_METHODE_VIRTUELLE_HERITAGE_MULTIPLE

#  else
#     error sgi > 7.30 - Version non supportee

#  endif

#endif

//----------------------------------------------------------------------
//
// ---  Microsoft Visual C++
//
//----------------------------------------------------------------------
#undef INRS_MSVC
#if defined(_MSC_VER)

#  define INRS_MSVC 1

#  if (_MSC_VER < 1200)
#     error Visual C++ < 12.0 - Version non supportee

#  elif (_MSC_VER <= 1200)
#     define INRS_DOIT_INCLURE_IO_H_AVEC_FCNTL_H
#     define INRS_SANS_XDR_QUADRUPLE
#     define INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM

#  elif (_MSC_VER <= 1300)
#     define INRS_DOIT_INCLURE_IO_H_AVEC_FCNTL_H
#     define INRS_SANS_XDR_QUADRUPLE
#     define INRS_CLASSE_TEMPLATE_AVEC_FONCTION_FRIEND_TEMPLATE_SANS_PARAM
#     define INRS_NEW_THROW throw(...)
#     define INRS_DEL_THROW throw()

#  elif (_MSC_VER <= 1916)
#     define INRS_DOIT_INCLURE_IO_H_AVEC_FCNTL_H
#     define INRS_SANS_XDR_QUADRUPLE
#     define INRS_NEW_THROW throw(...)
#     define INRS_DEL_THROW throw()
#     ifndef _CRT_SECURE_NO_WARNINGS
#        define _CRT_SECURE_NO_WARNINGS
#     endif

#  else
#     error Visual C++ > 19.16 - Version non supportee
#  endif

#endif   // defined(_MSC_VER)


//----------------------------------------------------------------------
//
// ---  Intel C++
//
//----------------------------------------------------------------------
#undef INRS_INTEL
#if defined(__INTEL_COMPILER)

#  define INRS_INTEL 1

#endif


//----------------------------------------------------------------------
//
// ---  Sun
//
//----------------------------------------------------------------------
#undef INRS_SUN
#if defined (__SUNPRO_C) || defined (__SUNPRO_CC)
#  define INRS_SUN 1

// Le define est necessaire avec la lib  std de Rogue-Wave
//#  define INRS_SANS_SPECIALISATION_PARTIELLE_DES_CLASSES_TEMPLATE

#endif


//**********************************************************************
//**********************************************************************
//
//                     Section des plateformes
//
//**********************************************************************
//**********************************************************************

//----------------------------------------------------------------------
//
// ---  WIN32
//
//----------------------------------------------------------------------
#if defined(__NT__) || defined(_WIN32) || defined(WIN32)
#  if !defined( __WIN32__)
#     define __WIN32__
#  endif
#endif

// ---  Watcom ne definit pas __WIN32__
#if defined(__NT__) && !defined(__WIN32__)
#  define __WIN32__
#endif

//----------------------------------------------------------------------
//
// ---  SUN
//
//----------------------------------------------------------------------
#if defined(__sun)
#  define INRS_SANS_STRLWR_STRUPR
#  define INRS_SANS_PROCESS_H
#  define INRS_SANS_SHARE_H
#endif

//----------------------------------------------------------------------
//
// ---  SGI
//
//----------------------------------------------------------------------
#if defined(__sgi)
#  define INRS_SANS_STRLWR_STRUPR
#  define INRS_SANS_PROCESS_H
#  define INRS_SANS_XDR_QUADRUPLE
#  define INRS_SANS_SHARE_H
#endif


//----------------------------------------------------------------------
//
// ---  Linux
//
//----------------------------------------------------------------------
#if defined(linux) || defined(__linux)
#  define INRS_SANS_STRLWR_STRUPR
#  define INRS_SANS_PROCESS_H
#  define INRS_SANS_XDR_QUADRUPLE
#  define INRS_SANS_SHARE_H
#  define INRS_BUG_UNIX_LOCK_FCNTL_NFS
#endif


//----------------------------------------------------------------------
//
// ---  CYGWIN
//
//----------------------------------------------------------------------
#if defined(__CYGWIN__)
#  define INRS_SANS_STRLWR_STRUPR
#  define INRS_SANS_PROCESS_H
#  define INRS_SANS_XDR_QUADRUPLE
#  define INRS_SANS_SHARE_H
#endif

//----------------------------------------------------------------------
//
// ---  HPUX
//
//----------------------------------------------------------------------
#if defined(hpux) || defined (__hpux)
#  define INRS_SANS_STRLWR_STRUPR
#  define INRS_SANS_PROCESS_H
#  define INRS_SANS_SHARE_H
#endif

//----------------------------------------------------------------------
//
// ---  AIX
//
//----------------------------------------------------------------------
#if defined(_AIX)
#  define INRS_SANS_STRLWR_STRUPR
#  define INRS_SANS_PROCESS_H
#  define INRS_SANS_XDR_QUADRUPLE
#  define INRS_SANS_SHARE_H
#endif

//**********************************************************************
//**********************************************************************
//
//              Section de configuration générique pour les DLL
//
//**********************************************************************
//**********************************************************************
#define MODULE_ACTIF 2

#if defined(__WIN32__) && defined(MODE_DYNAMIC)
#  define DLL_EXPORT __declspec(dllexport)
#  if defined(MODE_DYNAMIC_NO_DLL_IMPORT)
#     define DLL_IMPORT
#  else
#     define DLL_IMPORT __declspec(dllimport)
#  endif
#else
#  define DLL_IMPORT
#  define DLL_EXPORT
#endif

#define DLL_HLPR_1      DLL_IMPORT
#define DLL_HLPR_2      DLL_EXPORT
#define DLL_HELPER(mdl) DLL_HLPR_ ## mdl
#define DLL_IMPEXP(mdl) DLL_HELPER(mdl)

#endif   // SYCONFIG_H_DEJA_INCLU
