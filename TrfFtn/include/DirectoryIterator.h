//************************************************************************
// --- Copyright (c) INRS 2012
// --- Institut National de la Recherche Scientifique (INRS)
// ---
// --- Distributed under the GNU Lesser General Public License, Version 3.0.
// --- See accompanying file LICENSE.txt.
//************************************************************************

//************************************************************************
// Fichier: $Id$
//
// Sommaire:
//
// Description:
//
// Notes:
//************************************************************************
#ifndef DIRECTORYITERATOR_H_DEJA_INCLU
#define DIRECTORYITERATOR_H_DEJA_INCLU

#include "sytypes.h"

#define BOOST_FILESYSTEM_VERSION  3
#define BOOST_ALL_NO_LIB          1
#include <boost/functional/hash.hpp>
#include <boost/filesystem.hpp>
#if defined(__WIN32__)
#  include <Shlwapi.h>
#else
#  include <fnmatch.h>
#endif

class DirectoryIterator : public boost::filesystem::directory_iterator
{
public:
      typedef DirectoryIterator                      TTSelf;
      typedef boost::filesystem::directory_iterator  TTParent;
      typedef boost::filesystem::path                TTPath;

      DirectoryIterator() :
         TTParent()
      {}
      
      DirectoryIterator(const boost::filesystem::path& p, const std::string& m) :
         TTParent(p)
      {
#if defined(__WIN32__)
         _snwprintf(match, size_t(256), L"%hs", m.c_str());
#else
         match = m;
#endif
         while ((*this) != TTSelf() && !isMatch(*(*this)))
            TTParent::operator++();
      }

      bool operator== (const DirectoryIterator& rhs) const
      {
         return (static_cast<const TTParent&>(*this) == static_cast<const TTParent&>(rhs));
      }
      bool operator!= (const DirectoryIterator& rhs) const
      {
         return ! (*this == rhs);
      }
      
      DirectoryIterator& operator++ ()
      {
         if ((*this) == TTSelf()) return (*this);
         do
         {
            TTParent::operator++();
         }
         while ((*this) != TTSelf() && !isMatch(*(*this)));
         return (*this);
      }
      
private:
#if defined(__WIN32__)
   wchar_t match[256];
#else
   std::string match;
#endif
   
   bool isMatch(const TTPath& p) const
   {
      if (p.filename() == match) return true;
#if defined(__WIN32__)
      return (PathMatchSpecW(p.c_str(), match) == TRUE);
#else
      return (fnmatch(match.c_str(), p.c_str(), 0) == 0);
#endif
   }
};

#endif   // ifndef DIRECTORYITERATOR_H_DEJA_INCLU

/*
#include <iostream>
int main(int argc, char* argv[])
{
   if (argc > 1)
   {
      bool istop = true;
      while (istop)
         Sleep(2*1000);
   }

   DirectoryIterator it("e:\\dev", "*.bat");
   DirectoryIterator ie;
   for (; it != ie; ++it)
      std::cout << it->path() << std::endl;

   return 0;
}
*/
